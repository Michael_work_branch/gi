<?php
defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;
$user = JFactory::getUser();
$db = JFactory::getDbo();
$query = $db->getQuery(true);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=1200">
    <jdoc:include type="head" />
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35741200 = new Ya.Metrika({id:35741200, webvisor:true, clickmap:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/35741200" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick-theme.css"/> 
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/jquery-ui.theme.min.css">
    <!--<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>--> 
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.maskedinput.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/media/js/productplugin.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.validate.min.js"></script> 
</head>
<body>
    <div id="preloader">
        <div id="global">
            <div id="top" class="mask">
                <div class="plane"></div>
            </div>
            <div id="middle" class="mask">
                <div class="plane"></div>
            </div>
            <div id="bottom" class="mask">
                <div class="plane"></div>
            </div>
            <p><i>LOADING...</i></p>
        </div>
    </div>
    <a href="#popUpCall" id="fixedBtn" class="popUpCall animated" data-name-form="Request a call" onclick="yaCounter35741200.reachGoal('cell_buttonclick_salon'); return true;">
        <div class="fixedBtnWrap animated"></div>
        <div>
            <i class="fa fa-phone"></i>
        </div>
    </a>

    <!----------------------------------------HEADER---------------------------------->
    <header>
        <!--(готово)--------------------------------------PARALLAX-------------------------------->
        <!-- <div class="parallaxWrapper"> -->
        <span class="parallaxFixed parFixed1"></span>
        <span class="parallaxFixed parFixed2"></span>
        <span class="parallaxFixed parFixed3"></span>
        <span class="parallaxFixed parFixed4"></span>
        <span class="parallaxFixed parFixed5"></span>
        <span class="parallaxFixed parFixed6"></span>
        <span class="parallaxFixed parFixed7"></span>
        <span class="parallaxFixed parFixed8"></span>
        <span class="parallaxFixed parFixed9"></span>
        <span class="parallaxFixed parFixed10"></span>
        <span class="parallaxFixed parFixed11"></span>
        <span class="parallaxFixed parFixed12"></span>
        <span class="parallaxFixed parFixed13"></span>
        <span class="parallaxFixed parFixed14"></span>
        <span class="parallaxFixed parFixed15"></span>
        <span class="parallaxFixed parFixed16"></span>
        <span class="parallaxFixed parFixed17"></span>
        <span class="parallaxFixed parFixed18"></span>
        <span class="parallaxFixed parFixed19"></span>
        <span class="parallaxFixed parFixed20"></span>
        <span class="parallaxFixed parFixed21"></span>
        <!-- </div> -->
        <div class="headerUp animated">
            <div>
                <div class="headerUpFirst">
                    <jdoc:include type="modules" name="top_phone" />
                </div>
                <div class="headerUpSecond">
                    <?php if ($user->guest): ?>
                        <div class="btnHeader">
                            <button class="openpopupreg"><i class="fa fa-pencil"></i> Sign In</button>
                        </div>
                    <?php else: ?>
                        <div class="btnHeader">
                            <a href="<?= JRoute::_('index.php?option=com_users&view=profile', false) ?>"><i class="fa fa-key"></i> Personal Area</a>
                        </div>
                        <div class="btnHeader">
                            <form class="ajaxStop" action="<?= JRoute::_(htmlspecialchars(JUri::getInstance()->toString())) ?>" method="post">
                                <input type="hidden" name="option" value="com_users" />
                                <input type="hidden" name="task" value="user.logout" />
                                <input type="hidden" name="return" value="./" />
                                <?php echo JHtml::_('form.token'); ?>
                                <button class="exitProfile">
                                    <i class="fa fa-sign-out"></i> Go out
                                </button>
                            </form>
                        </div>
                    <?php endif; ?>
                    <a href="#">Ru</a>
                    <!-- <a href="#">En</a> -->
                </div>
            </div>
        </div>
        <div class="headerMiddle animated">
            <div>
                <div class="headerMiddleFirst">
                    <div class="headerMiddleImgWrap">
                        <a href="./">
                            <img src="images/logo/logoLeft.png">
                            <img src="images/logo/logoRight.png">
                        </a>                            
                    </div>
                    <a href="#" class="menuTable">
                        <i class="fa fa-bars animated"></i>
                        <p>Menu</p>
                    </a>
                </div>
                <div class="headerMiddleSecond">
                    <div class="headerFormWrap">
                        <span class="firstBorder formSpan"></span>
                        <span class="secondBorder formSpan"></span>
                        <span class="thirdBorder formSpan"></span>
                        <span class="fourthBorder formSpan"></span>
                        <span class="fifthBorder formSpan"></span>
                        <form method="POST" class="subscribeForm" data-target="shares_form_salon">
                            <p class="forNameForm">Subscribe to shares</p>
                            <div>
                                <i class="fa fa-percent"></i>
                                <p>Subscribe to shares</p>
                            </div>
                            <div class="inFormInput">
                                <input type="text" name="email" placeholder="Ваш email" onclick="yaCounter35741200.reachGoal('input_shares_salon'); return true;" class="inputAnim usermail">
                                <button class="send headerSend">
                                    <i class="fa fa-envelope"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <a href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&lang=ru', false); ?>" class="basket">
                        <jdoc:include type="modules" name="my_cart" />
                    </a>
                </div>
            </div>
            <div class="headerMiddleinner">
                <div class="content-container">
                    <?php if ($this->countModules('mainmenu')) : ?>
                        <jdoc:include type="modules" name="mainmenu" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="headerDown">
            <div>
                <div class="hWrap">
                    <h1>Cosmeceutical</h1>
                    <h1>high-end GI BEAUTY</h1>
                </div>
                <h3>for beauty salons and clinics</h3>
                <div class="headerDownWrapP">
                    <p>Perfectly balanced<br>composition</p>
                    <p>•</p>
                    <p>Innovative<br>formula</p>
                    <p>•</p>
                    <p>Packaging, reliably protects<br>from bacterial contamination</p>
                </div>
            </div>
        </div>
    </header>
    <!--(готово)------------------------------------------SECTION-1-------------------------------->
    <section class="section1">
        <div>
            <h3>Quality and efficiency at an affordable price!</h3>
            <a class="btnStyle popUpCall" href="#popUpCall" data-name-form="Order shipment* today" onclick="yaCounter35741200.reachGoal('zakaz_buttonclick_salon'); return true;">Order shipment* today</a>
            <p>*five units of each denomination</p>
            <h3>GI BEAUTY - the choice of professionals!</h3>
        </div>
        <div class="animationH2">
            <h2>Means of active steps at a reasonable wholesale price!</h2>
            <span></span>
            <span></span>
        </div>
    </section>
    <!--------------------------------------------SECTION-2-------------------------------->
    <section class="section2">
        <div class="sliderSec2-1 sliderSec2First noprice">
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section2/img1Slider1Sal.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    buy in one click
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section2/img2Slider1Sal.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    buy in one click
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section2/img3Slider1Sal.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    buy in one click
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section2/img4Slider1Sal.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    buy in one click
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section2/img4Slider1Sal.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    buy in one click
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section2/img4Slider1Sal.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    buy in one click
                </div>
            </a>
        </div>
    </section>
    <!--------------------------------------------SECTION-3-------------------------------->
    <section class="section3 calc">
        <form>
            <div>
                <h3>calculator</h3>
                <div class="sec11WrapTop">
                    <div class="checkBtnWrap" >
                        <div class="radioWrap" id="sex">
                            <div class="radioStyle">
                                <input type="radio" name="gender" value="ledy">
                                <div>
                                    <img src="images/section3/lips.png">
                                </div>
                            </div>
                            <div class="radioStyle">
                                <input type="radio" name="gender" value="man">
                                <div>
                                    <img src="images/section3/mustacheSal.png">
                                </div>
                            </div>
                        </div>
                        <label>What's your gender</label>
                    </div>
                    <div class="borderForm"></div>
                    <div class="checkBtnWrap" id="age">
                        <div class="slider-range"></div>
                        <input type="hidden" name="age" id="ageInner">
                        <label>Your age</label>
                    </div>
                    <div class="borderForm"></div>
                    <div class="checkBtnWrap" id="typemerch">
                        <select>
                            <option>Cream</option>
                            <option>mask</option>
                            <option>means for washing</option>
                            <option>mesopreparations</option>
                        </select>
                        <label>Choose vehicle type</label>
                    </div>
                </div>
                <div class="sec11WrapBottom animated">
                    <div class="borderFormGor"></div>
                    <div class="radioWrapBot" id="typeskin">
                        <div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>Dry</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>Normal</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>Oily</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>combined</p>
                                </div>
                            </div>

                        </div>
                        <label>Skin condition</label>
                    </div>
                    <div class="borderForm"></div>
                    <div class="radioWrapBot" d="typeaction">
                        <div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>Humidification</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>Purification</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>Skin nutrition</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>comprehensive care</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>anti-age</p>
                                </div>
                            </div>
                        </div>
                        <label>What would you like to receive?</label>
                    </div>
                    <button class="btnStyle">pick up</button>
                </div>
                <div class="absoluteBottom">
                    <h3>the effect is guaranteed!</h3>
                    <img src="images/section3/arrow.png">
                </div>
            </div>

        </form>
    </section>
    <!--------------------------------------------SECTION-4-------------------------------->
    <section class="section4">
        <div>
            <div class="sliderSec10 cosm">
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img1Sec10.png">
                    </div>
                    <p>Regenerating Serum for women 50 ml GI BEAUTY</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img2Sec10.png">
                    </div>
                    <p>A set of masks for the face GI BEAUTY, 8pcs</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img1Sec10.png">
                    </div>
                    <p>Regenerating Serum for women 50 ml GI BEAUTY</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img2Sec10.png">
                    </div>
                    <p>A set of masks for the face GI BEAUTY, 8pcs</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img1Sec10.png">
                    </div>
                    <p>Regenerating Serum for women 50 ml GI BEAUTY</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img2Sec10.png">
                    </div>
                    <p>A set of masks for the face GI BEAUTY, 8pcs</p>
                </div>
            </div>
            <div class="afterCos">
                <div>
                    <?
                    $path_male = $_SERVER['DOCUMENT_ROOT'] . '/images/faces/male/';
                    $path_female = $_SERVER['DOCUMENT_ROOT'] . '/images/faces/female/';
                    $photo_faces = array();
                    if (file_exists($path_female)) {
                        $photo_female = scandir($path_female);
                        foreach ($photo_female as $value) {
                            if ($value != '.' and $value != '..')
                                $photo_faces[] = array('data' => 'female', 'path' => 'images/faces/female/' . $value);
                        }
                    }

                    if (file_exists($path_male)) {
                        $photo_male = scandir($path_male);
                        foreach ($photo_male as $value) {
                            if ($value != '.' and $value != '..')
                                $photo_faces[] = array('data' => 'male', 'path' => 'images/faces/male/' . $value);
                        }
                    }
                    //первая картинка
                    $img = array_shift($photo_faces);
                    print '<img id="girl" data-face="' . $img['data'] . '" src="' . $img['path'] . '">';
                    unset($photo_faces[0]);
                    ?>
                    <div class="hide-foto" style="display: none" id="calcfoto">
                        <?
                        foreach ($photo_faces as $img) {
                            print '<img data-face="' . $img['data'] . '" src="' . $img['path'] . '">';
                        }
                        ?>
                    </div>
                </div>
                <div class="sec10H6Wrap">
                    <div>
                        <h6>after peeling</h6>
                    </div>
                    <div>
                        <h6>8 hours after the application of funds GI BEAUTY</h6>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--(надо проверить шаблон модуля)------------------------------------------SECTION-5-------------------------------->
    <section class="section5">
        <div class="animationH2">
            <h2>Features ideal formula is unique</h2>
            <span></span>
            <span></span>
        </div>
        <jdoc:include type="modules" name="patents"/>
    </section>
    <!--(оставляем статичным)------------------------------------------SECTION-6-------------------------------->
    <section class="section6">
        <div>
            <div class="columnStyle">
                <img src="images/section6/img1Sec6Sal.png" class="sec6Anim1-1 animated">
                <h3 class="sec6Anim1-2 animated">Rich<br>balanced composition.<br>Like him just do not have!</h3>
                <div class="sec6Anim1-3 animated">
                    <i class="fa fa-plus-circle"></i>
                    <p>4 microcell (Na, K, Ca, Mg), 18 amino acids</p>
                </div>
                <div class="sec6Anim1-4 animated">
                    <i class="fa fa-plus-circle"></i>
                    <p>9 basic essential vitamins, glucose, iron, 9 innovative components that do not have other cosmetics</p>
                </div>
                <div class="sec6Anim1-5 animated">
                    <i class="fa fa-plus-circle"></i>
                    <p>Growth factors and cytokines (IL6, IL11, TGF-beta; KGF; EGF; bFGF), biologically active substances</p>
                </div>
            </div>
            <div class="borderSec6"></div>
            <div class="columnStyle">
                <img src="images/section6/img2Sec6Sal.png" class="sec6Anim1-1 animated">
                <h3 class="sec6Anim1-2 animated">The use in the manufacture<br>foreign components<br>High Quality</h3>
                <p class="sec6Anim2-3 animated">the same that is used in Europe famous cosmetic house</p>
                <img src="images/section6/img3Sec6Sal.png" class="sec6Anim2-4 animated">
                <h3 class="sec6Anim2-5 animated">The absence of fat<br>components in</h3>
                <p class="sec6Anim2-6 animated">does not clog pores, it prevents unpleasant shine, allowing the skin to breathe</p>
            </div>
            <div class="borderSec6"></div>
            <div class="columnStyle">
                <img src="images/section6/img4Sec6Sal.png" class="sec6Anim1-1 animated">
                <h3 class="sec6Anim1-2 animated">The rapid penetration of<br>into the skin - quadruple<br>faster analogues</h3>
                <p class="sec6Anim3-3 animated">the size of the active ingredients is less than 150,000 Daltons</p>
                <img src="images/section6/img5Sec6Sal.png" class="sec6Anim3-4 animated">
                <h3 class="sec6Anim3-5 animated">Double exposure</h3>
                <div class="sec6Anim3-6 animated">
                    <i class="fa fa-plus-circle"></i>
                    <p>epidermis- on hydration and nutrition</p>
                </div>
                <div class="sec6Anim3-7 animated">
                    <i class="fa fa-plus-circle"></i>
                    <p>in the dermis - anti-age action</p>
                </div>
                <div class="sec6Anim3-8 animated">
                    <i class="fa fa-plus-circle"></i>
                    <p>the immune system of the skin - increased protective properties and anti-inflammatory effect</p>
                </div>
            </div>
        </div>
    </section>
    <!--------------------------------------------SECTION-7-------------------------------->
    <section class="section7">
        <div class="leftSec7 sliderSec7">
            <div class="sec7ContentWrap">
                <div class="imgWrapSec7">
                    <img src="images/section7/img1Sec7.png">
                </div>
                <div class="contentLeft animated">
                    <div class="arrowSec7">
                        <img src="images/section7/arrowLeft.png">
                    </div>
                    <h3>Mask GI BEAUTY</h3>
                    <div class="sec7PWrap">
                        <i class="fa fa-plus-circle"></i>
                        <p>Indicator osmolarity (plasma) mask GI BEAUTY - up to 320 mmol / L (normal - 285)</p>
                    </div>
                    <div class="sec7PWrap">
                        <i class="fa fa-plus-circle"></i>
                        <p>As part of - Na, K, Ca, Mg, Fe, 18 amino acids, 9 essential vitamins, glucose, IL6, IL11, TGF-beta; KGF; EGF; bFGF</p>
                    </div>
                    <div class="sec7PWrap">
                        <i class="fa fa-plus-circle"></i>
                        <p>The presence of substances that promote the regeneration - growth factors and cytokines, marine collagen, the molecular film Lipomoist 2013, Unitamuron H-22</p>
                    </div>
                </div>
                <a href="#">Learn the details of the composition of masks</a>
            </div>
        </div>
        <div class="rightSec7 sliderSec7">
            <div class="sec7ContentWrap">
                <div  class="contentLeft animated">
                    <div class="arrowSec7">
                        <img src="images/section7/arrowRight.png">
                    </div>
                    <h3>serum GI BEAUTY<br>for women</h3>
                    <p>As a part - active substances:</p> 
                    <ul>
                        <li>Antarcticine<i class="fa fa-check-circle"></i></li>
                        <li>Juveleven<i class="fa fa-check-circle"></i></li>
                        <li>Unirepair T-43<i class="fa fa-check-circle"></i></li>
                        <li>Safester A-75<i class="fa fa-check-circle"></i></li>
                        <li>Unilucent HR-14<i class="fa fa-check-circle"></i></li>
                        <li>Lipomoist 2036<i class="fa fa-check-circle"></i></li>
                        <li>Trylagen<i class="fa fa-check-circle"></i></li>
                        <li>Hyanify<i class="fa fa-check-circle"></i></li>
                        <li>D-Panthenyltriacetate<i class="fa fa-check-circle"></i></li>
                        <li>Inoveol EGCG<i class="fa fa-check-circle"></i></li>
                    </ul>
                </div>
                <div class="imgWrapSec7">
                    <img src="images/section7/img2Sec7.png">
                </div>
                <a href="#">To know more about serum</a>
            </div>
        </div>
        <div class="doubleArrow">
            <i class="fa fa-arrows-h"></i>
        </div>
    </section>
    <!--------------------------------------------SECTION-8-------------------------------->
    <section class="section8">
        <form method="POST" data-target="question_form_salon">
            <h3>Have questions?</h3>
            <p class="forNameForm">Have questions?</p>
            <div class="inputWrapSec8">
                <div class="inputStyle">
                    <input type="text" name="name">
                    <span></span>
                    <label>Notify Your name*</label>
                </div>
                <div class="inputStyle">
                    <input type="text" name="phone" class="userphone">
                    <span></span>
                    <label>Your phone number*</label>
                </div>
            </div>
            <button class="btnStyle send">Contact!</button>
            <h4>Our manager will advise you free of charge</h4>
            <p>*Your personal data will not under any circumstances be transferred to third parties</p>
        </form>
    </section>
    <!--(готово)------------------------------------------SECTION-9-------------------------------->
    <section class="section9">
        <div class="animationH2">
            <h2>Unique for the cosmetics industry!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <div class="sec9PWrap">
                <img src="images/section9/world.png">
                <div>
                    <h4>Only products GI BEAUTY line is made in the laboratory of international standard GMP</h4>
                    <p>a set of international standards and regulations for the manufacture of pharmaceutical products</p>
                </div>
            </div>
            <div class="sliderSec9">
                <jdoc:include type="modules" name="sertificates"/>
            </div>
        </div>
    </section>
    <!--(готово)------------------------------------------SECTION-10-------------------------------->
    <section class="section10">
        <div>
            <jdoc:include type="modules" name="video_1"/>
        </div>
    </section>
    <!--(готово)------------------------------------------SECTION-11-------------------------------->
    <section class="section11">
        <div class="animationH2">
            <h2>Information for cosmetologists your clinic or salon:</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <h4>Details on the application of GI BEAUTY</h4>
            <jdoc:include type="modules" name="secret"/>
        </div>
    </section>
    <!--------------------------------------------SECTION-12-------------------------------------------->
    <section class="section12">
        <form method="POST" data-target="obraz_form_salon"> 
            <h3>Ensure quality - really<br>We send probes free!</h3>
            <p class="forNameForm">Ensure quality - really free We send probes!</p>
            <div class="sec12Wrap">
                <div class="inputStyle">
                    <input type="text" name="company">
                    <span></span>
                    <label>Name of your company</label>
                </div>
                <div class="inputStyle">
                    <input type="text" name="name">
                    <span></span>
                    <label>Notify Your name </label>
                </div>
                <div class="inputStyle">
                    <input type="text" name="phone" class="userphone">
                    <span></span>
                    <label>Your phone number</label>
                </div>
            </div>
            <button class="btnStyle send">Order coupons</button>
            <p>*Your personal data will not under any circumstances be transferred to third parties</p>
        </form>
    </section>
    <!--(готово)----------------------------SECTION-13-------------------------------->
    <section class="section13">
        <div class="animationH2">
            <h2>Creator line GI BEAUTY - Russian company «НПО ЛИОМАТРИКС»</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <div class="sec13Anim1 animated">
                <i class="fa fa-plus-circle"></i>
                <p>It is using modern advances in regenerative medicine and cellular technologies</p>
            </div>
            <div class="sec13Anim2 animated">
                <i class="fa fa-plus-circle"></i>
                <p>It takes into account all the features of the women's and men's skin</p>
            </div>
        </div>
        <div>
            <div>
                <a href="./"><img src="images/logo/logoSec13.png"></a>
                <h3>All Company<br>«НПО ЛИОМАТРИКС»</h3>
            </div>
        </div>
    </section>
    <!--(готово)----------------------------SECTION-14-------------------------------->
    <section class="section14">
        <div class="animationH2">
            <h2>Contact Information:</h2>
            <span></span>
            <span></span>
        </div>
        <div class="top-contacts-belt">
            <div>
                <i class="fa fa-map-marker"></i>
                <jdoc:include type="modules" name="bottom_adress"/>
            </div>
            <div>
                <i class="fa fa-phone"></i>
                <jdoc:include type="modules" name="bottom_phone"/>
            </div>
            <div>
                <i class="fa fa-envelope mailI"></i>
                <a href="mailto:<?= JFactory::getMailer()->From; ?>" onclick="yaCounter35741200.reachGoal('mail_salon'); return true;"><?= JFactory::getMailer()->From; ?></a>
            </div>
        </div>
        <div id="mapWrap"></div>
    </section>
    <!----------------------------------SECTION15-------------------------------------------->
    <section class="section15">
        <div>
            <h3>We guarantee to the effect GI BEAUTY!!</h3>
            <h3>Technologies of the future - today our!</h3>
            <?php
            $query->select('params')
                    ->from('#__extensions')
                    ->where('extension_id = 10066');
            $socials = $db->setQuery($query)->loadResult();
            $query->clear();
            $socials = json_decode($socials);
            ?>
            <div class="linksWrap">
                <a href="<?= $socials->fb ?>">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="<?= $socials->inst ?>">
                    <i class="fa fa-instagram"></i>
                </a>
                <a href="<?= $socials->tw ?>">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="<?= $socials->vk ?>">
                    <i class="fa fa-vk"></i>
                </a>
                <a href="<?= $socials->ok ?>">
                    <i class="fa fa-odnoklassniki"></i>
                </a>
                <a href="<?= $socials->pi ?>">
                    <i class="fa fa-pinterest-p"></i>
                </a>
                <a href="<?= $socials->in ?>">
                    <i class="fa fa-linkedin"></i>
                </a>
            </div>
        </div>
    </section>
    <!--(готово)--------------------------------FOOTER------------------------------------------------>
    <footer>
        <div class="footerUp">
            <div>

                <a href="./">
                    <img src="images/logo/logoFooter.png">
                </a>
                <jdoc:include type="modules" name="footer_menu" />
            </div>
        </div>
        <div class="footerDown">
            <div>
                <div>
                    <span>©</span>
                    <p>2015 ООО «НПО Лиоматрикс»</p>
                </div>
                <div>
                    <a href="http://tutmee.ru/">
                        <!--<p>Создание дизайна<br>и разработка сайтов<br>LTD Tutmee.ru</p>-->
                        <img src="images/logo/tutmee.png">
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <div class="popupoverlay"></div>
    <div class="autentification-popup" id="popupreg">        <!-- если в URL есть якорь #popupreg отлкрывать форму при загрузке страницы-->   
        <ul class="autentification-tab">
            <li><span class="fa fa-pencil"></span>check in</li>
            <li class="active"><span class="fa fa-key"></span>login</li>                           
        </ul>
        <table>
            <tr>
                <td>
                    <ul class="autentification-tab-item">
                        <li>
                            <!--                        <div class="error-form-place">
                                                        Ошибка формы !!!!
                                                    </div>-->
                            <form class="ajaxStop" action="<?php echo JRoute::_('index.php?option=com_users&task=gireg.register'); ?>" method="post">
                                <!--                                    <ul class="register-type">
                                                                        <li class="active">для физических лиц <div class="hide-radio"><input type="radio"  name="rtype"/></div></li>
                                                                        <li>для юридических лиц <div class="hide-radio"><input type="radio"  name="rtype"/></li>
                                                                    </ul>-->
                                <ul class="register-item">
                                    <li class="active">
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[name]" /><label>Your Name (required)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password1]" /><label>Password (required)</label></div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[email1]" /><label>Your email address (required)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password2]" /><label>Confirm Password (required)</label></div>
                                        </fieldset>
                                        <div class="capcha-check-container">
                                            <fieldset>
                                                <div class="capcha-block">
                                                    <div class="g-recaptcha" data-sitekey="6LdqdRgTAAAAAPYF1ypIO5UDbI1SaSqlNsvrPK5p"></div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="check-container">
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"> </label></div>
                                                        <span>Receive news by email service</span>
                                                    </div>
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"></label></div>
                                                        <span>I have read and agree to the terms and conditions <a href="#">Privacy policy</a></span>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <input type="hidden" name="option" value="com_users" />
                                        <input type="hidden" name="task" value="gireg.register" />
                                        <?php echo JHtml::_('form.token'); ?>
                                        <button>sign up</button>
                                    </li>
                                    <!--                                        <li>
                                    
                                                                                <fieldset>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Название организации или ИП (обязательно)</label></div>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Email (обязательно) </label></div>
                                                                                </fieldset>
                                                                                <fieldset>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Контактное лицо</label></div>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Номер телефона (обязательно)</label></div>
                                                                                </fieldset>
                                                                                <div class="password-block">
                                                                                    <fieldset><div class="form-item" data-hint=""><input type="password" /><label>Пароль(обязательно)</label></div></fieldset>
                                                                                    <fieldset><div class="form-item" data-hint=""><input type="password" /><label>Подтверждение пароля(обязательно)</label></div></fieldset>
                                                                                </div>
                                                                                <div class="capcha-check-container">
                                                                                    <fieldset>
                                                                                        <div class="capcha-block"><img src="images/capha.png" alt="" /></div>
                                                                                    </fieldset>
                                                                                    <fieldset>
                                                                                        <div class="check-container">
                                                                                            <div class="check-block">
                                                                                                <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"> </label></div>
                                                                                                <span>Получать на email новости сервиса</span>
                                                                                            </div>
                                                                                            <div class="check-block">
                                                                                                <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"></label></div>
                                                                                                <span>Я ознакомлен и согласен с условиями <a href="#">политики конфиденциальности</a></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </fieldset>
                                                                                </div>
                                                                                <button>зарегистрироваться</button>
                                    
                                                                            </li>-->
                                </ul>
                            </form>
                        </li>
                        <li class="active">
                        <jdoc:include type="modules" name="login" />
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        <div class="close-popup"></div>               
    </div>
    <div id="popUpCall">
        <form method="POST" data-target="cell_form_salon">
            <h2 class="nameInner">Request a call</h2>
            <p class="forNameForm nameInner">Request a call</p>
            <div>
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Your name</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone">
                    <span></span>
                    <label>Your phone number</label>
                </div>
            </div>
            <button class="btnStyle send">Send</button>
            <div class="closeFancy">
                <span></span>
                <span></span>
            </div>
        </form>
    </div>
    <div id="productPopUp">
        <form method="POST" data-target="byin_form_salon">
            <h2>Triple Action Face Mask GI BEAUTY</h2>
            <p class="forNameForm">Triple Action Face Mask GI BEAUTY</p>
            <div class="productPopUpInput">
                <div>
                    <img src="images/merch.png">
                </div>
                <ul>
                    <li><i class="fa fa-check-circle"></i><p>regeneration (restoring)</p></li>
                    <li><i class="fa fa-check-circle"></i><p>face hydration</p></li>
                    <li><i class="fa fa-check-circle"></i><p>anti-age affect</p></li>
                </ul>
            </div>
            <div>
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Enter your name</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone">
                    <span></span>
                    <label>Enter your phone</label>
                </div>
                <div class="inputStyle">
                    <input name="email" type="text" class="usermail">
                    <span></span>
                    <label>Enter your email</label>
                </div>
            </div>
            <button class="btnStyle">send</button>
            <div class="closeFancy">
                <span></span>
                <span></span>
            </div>   
        </form>
    </div>
    <div id="imagePopUp">
        <img src="#">
    </div>
    <div class="commonpopup" id="thanksPopUp">
        <div class="close-popup"></div>
        <div id="alert">
            <table>
                <tr>
                    <td>
                        <h2>Thank you for your application</h2>
                        <h3>all messages will be sent to the address <a href="mailto:<?= JFactory::getMailer()->From; ?>" onclick="yaCounter35741200.reachGoal('mail_salon'); return true;"><?= JFactory::getMailer()->From; ?></a></h3>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script src="https://www.youtube.com/iframe_api"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/script.js"></script> 
</body>
</html>
