$(document).ready(function(){

    //validation and form-send
    var form;
        form = function() {
          var fieldForm, idCurrentForm, sendAjaxAndResetForm;
          $.fn.valToJsObj = function() {
            var jsonFormData,
              _this = this;
            jsonFormData = {};
            this.each(function() {
              var elementFormData, formData, key, value, _i, _len, _results;
              formData = jQuery(_this).serializeArray();
              _results = [];
              for (_i = 0, _len = formData.length; _i < _len; _i++) {
                elementFormData = formData[_i];
                key = elementFormData.name;
                value = elementFormData.value;
                _results.push(jsonFormData[key] = value);
              }
              return _results;
            });
            return jsonFormData;
          };
          idCurrentForm = ''; // сама форма
          sendAjaxAndResetForm = function() {
            var jsonFormData;
            jsonFormData = $("#" + idCurrentForm).valToJsObj();
            // setTimeout(function(){
            //   $("#" + idCurrentForm).trigger("reset");
            // },300)
              
            return $.ajax({
              type: "POST",
              async: true,
              url: "form-handler.php",
              dataType: "json",
              data: {
                formData: jsonFormData
              }
            }).done(function(dataThanks) {
                // success:(function(arg) {
                // $('input[type=reset]').click();
           });
                
 
          };
          fieldForm = {
            rules: {
//              name: {
//                required: true,
//                minlength: 2,
//                maxlength: 70
//              },
              phone: {
                required: true,
                // minlength: 8
              },
              email: {
                required: true,
//                email: true
              },
//              comment: {
//                required: true,
//                minlength: 2,
//                maxlength: 200
//              }
            },
            messages: {
//              name: {
//                required: "",
//                minlength: "",
//                maxlength: ""
//              },
              phone: {
                required: "",
                minlength: ""
              },
              email: {
                required: "",
                email: ""
              },
//              comment: {
//                required: "",
//                minlength: "",
//                maxlength: ""
//              }
            },
            submitHandler: function(e) {
              e.preventDefault;
              idCurrentForm = this.currentForm.id;
              return sendAjaxAndResetForm();
            }
          };
          return $(".form-request").each(function() {
            return $(this).validate(fieldForm);
          });
        };
    form();
 
}); // end

  