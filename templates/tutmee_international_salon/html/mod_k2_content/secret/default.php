<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
if (count($items)):
    ?>
    <div class="sec11Wrap">
        <div class="sec11Anim1 animated">
            <img src="images/section11/img1Sec11.png">
            <a class="btnStyle" href="<?= strip_tags($items[0]->introtext) ?>">Скачайте инструкцию</a>
        </div>
        <div class="sec11Anim2 animated">
            <img src="images/section11/img2Sec11.png">
            <a class="btnStyle" href="<?= strip_tags($items[0]->fulltext) ?>">Посмотрите видеоурок</a>
        </div>
    </div>    
<? endif; ?>