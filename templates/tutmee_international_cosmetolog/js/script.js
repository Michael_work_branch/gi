jQuery(function ($) {
    $.validator.addMethod(
            'regexp',
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );

    $.validator.addClassRules({
        userphone: {
            required: true,
            minlength: 15,
            regexp: '[^_]+$'
        },
        usermail: {
            email: true
        }
    });
    $(window).load(function () {
        $('.calc form button').click(function () {
            calcMainPage.monitoring();
            return false;
        })
        function loadpage() {
            setTimeout(function () {
                var windowWidth = document.documentElement.clientWidth;
                if (windowWidth >= 1560) {
                    $('.sliderSec3').slick({
                        slidesToShow: 4,
                        slidesToScroll: 2,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 5000
                    });
//            $('.sliderSec2-1').slick({
//                slidesToShow:4,
//                slidesToScroll:2,
//                infinite:true,
//                arrows:true
//            })
                }
                if (windowWidth < 1560) {
                    $('.sliderSec3').slick({
                        slidesToShow: 3,
                        slidesToScroll: 2,
                        infinite: true,
                        autoplay: true,
                        autoplaySpeed: 5000
                    });
//            $('.sliderSec2-1').slick({
//                slidesToShow:3,
//                slidesToScroll:2,
//                infinite:true,
//                arrows:true
//            })  
                }
//        if (windowWidth>=1460){
//          $('.sliderSec10').slick({
//                slidesToShow:2,
//                slidesToScroll:2,
//                infinite:true,
//                arrows:true
//            })  
//        }
//        if (windowWidth<1460){
//          $('.sliderSec10').slick({
//                slidesToShow:1,
//                slidesToScroll:1,
//                infinite:true,
//                arrows:true
//            })  
//        }
                $('#preloader').addClass("animated fadeOut")
                setTimeout(function () {
                    $('#preloader').css("display", "none");
                    $('.girlAnimation').css('opacity', '1')
                    $('#fixedBtn').addClass('bounceInRight').css('opacity', '1');
                    $('.headerFormWrap span').eq(0).css('width', '11px');
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(1).css('height', '100%');
                    }, 100)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(2).css('width', '100%');
                    }, 400)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(3).css('height', '100%');
                    }, 1300)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(4).css('width', '277px');
                    }, 1600)
                    setTimeout(function () {
                        $('#fixedBtn').removeClass('bounceInRight')
                    }, 900)
                }, 500)
            }, 300)
        }
        loadpage();
        $(document).on('click', '.byin', function () {
            yaCounter35741200.reachGoal('byin_buttonclick_cosmetologi');
            return true;
        });
    });
    $('#fixedBtn').click(function () {
        $('#fixedBtn').addClass('rubberBand');
        setTimeout(function () {
            $('#fixedBtn').removeClass('rubberBand');
        }, 700)
    })

    /*--------------------------btn-animation------------------------------*/
    $('.menuTable').mouseover(function () {
        $('.fa-bars').addClass('jello');
        setTimeout(function () {
            $('.fa-bars').removeClass('jello');
        }, 700)
    })
    $('.basket').mouseover(function () {
        $('.fa-shopping-bag').addClass('jello');
        setTimeout(function () {
            $('.fa-shopping-bag').removeClass('jello');
        }, 700)
    })
    /*--------------------------INPUT ANIMATION-----------------------------*/
    $('.headerFormWrap input').hover(function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    }, function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    })
    var flag4 = true;
    $(window).scroll(function () {
        var heightWindow = document.documentElement.clientHeight / 2.5;

        if ($(this).scrollTop() >= $('.section1').offset().top - heightWindow) {
            $('.section1>div:nth-of-type(2) span').addClass('animatedSpan');
            // $('.videoWrap').addClass('fadeInUp').css('opacity','1')
        }
        if ($(this).scrollTop() >= $('.section2').offset().top - heightWindow) {
            $('.sec2Anim1').addClass('fadeInLeft').css('opacity', '1');
            $('.sec2Anim2').addClass('fadeInRight').css('opacity', '1');
            setTimeout(function () {
                $('.sec2Anim4').addClass('fadeInRight').css('opacity', '1');
                $('.sec2Anim3').addClass('fadeInLeft').css('opacity', '1');
            }, 300)
        }
        if ($(this).scrollTop() >= $('.section4').offset().top - heightWindow) {
            $('.section4>div:nth-of-type(1) span').addClass('animatedSpan');
            $('.sec4Anim1-1,.sec4Anim2-1,.sec4Anim3-1').addClass('flipInX').css('opacity', '1')
            setTimeout(function () {
                $('.sec4Anim1-2,.sec4Anim2-2,.sec4Anim3-2').addClass('flipInX').css('opacity', '1')
            }, 300)
            setTimeout(function () {
                $('.sec4Anim1-3,.sec4Anim2-3,.sec4Anim3-3').addClass('flipInX').css('opacity', '1')
            }, 600)
            setTimeout(function () {
                $('.sec4Anim1-4').addClass('flipInX').css('opacity', '1')
            }, 900);
        }
        if ($(this).scrollTop() >= $('.section5').offset().top - heightWindow) {
            $('.section5>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section11').offset().top - heightWindow) {
            $('.section11>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section14').offset().top - heightWindow) {
            $('.section14>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() > $('.section1').offset().top - 100) {
            if (flag4 == true) {
                $('.headerDown').css('margin-top', '153px');
                $('.headerUp').addClass('headerUpFixed fadeInDown');
                $('.headerMiddle').addClass('headerMiddleFixed fadeInDown');
                flag4 = false;
            }
        }
        if ($(this).scrollTop() < $('.section1').offset().top - 100) {
            if (flag4 == false) {
                $('.headerMiddle').removeClass('fadeInDown').addClass('fadeOutUp');
                $('.headerUp').removeClass('fadeInDown').addClass('fadeOutUp');
                setTimeout(function () {
                    $('.headerDown').css('margin-top', '0')
                    $('.headerUp').removeClass('headerUpFixed fadeOutUp').css('opacity', '1');
                    $('.headerMiddle').removeClass('headerMiddleFixed fadeOutUp').css('opacity', '1');
                }, 600)
                flag4 = true;
            }
        }
    })
    $('.slider-range').slider({
        range: "min",
        min: 18,
        max: 80,
        value: 35,
        slide: function (event, ui) {
            $("#ageInput").val(ui.value + " года");
            $(".ui-slider-range").attr('data-attribute', ui.value);
            $(this).closest('.checkBtnWrap>div').addClass('fixUiSlider');
        }
    })
    $("#ageInput").val($("#slider-range").slider("value") + ' года');
    $(".ui-slider-range").attr('data-attribute', '35');


    $('input[name="gender"]').click(function () {
        $('.sec11WrapBottom').addClass('bounceIn').css({'display': 'block', 'opacity': '1'})
    });

    (function () {
        var API = google.maps;
        var $container = $('#mapWrap');
        if ($container.length == 0)
            return;
        var mapPosition = new API.LatLng(55.776597, 37.542144);
        var markerPosition = new API.LatLng(55.776597, 37.542144);
        var map = new API.Map($container[0], {
            center: mapPosition,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            panControl: false,
            zoom: 14
        });
        var marker = new API.Marker({
            map: map, position: markerPosition, icon: 'images/marker.png'
        });
        var iw = new google.maps.InfoWindow({
            content: '<div style="text-align: left; height: 45px; font-size: 18px; position:relative; z-index:1;">г.Реутов<br>ул.Безымянная дом 0</div>'
        });
    })();
    $('.section5>div:nth-of-type(2)>div:nth-of-type(1)').click(function () {
        $(this).css({
            "transform": "scale(2)",
            "-o-transform": "scale(2)",
            "-moz-transform": "scale(2)",
            "-ms-transform": "scale(2)",
            "-webkit-transform": "scale(2)",
            "opacity": "0",
            "pointer-events": "none"
        })
        setTimeout(function () {
            $('.section5>div:nth-of-type(2)>div:nth-of-type(1)').css({'z-index': '40'});
        }, 500)
        $('.womenCat').css('opacity', '0');
        $('#forLady').addClass('activeGender');
        $('.catalogSec2').css({'opacity': '1', 'z-index': '50'});
        $('.sliderSec2First').css({'opacity': '1', 'z-index': '51'})
    });
    $('#forLady').click(function () {
        $('#forGentleman').removeClass('activeGender');
        $('#forLady').addClass('activeGender');
        $('.sliderSec2Second').css({'opacity': '0', 'z-index': '50'})
        $('.sliderSec2First').css({'opacity': '1', 'z-index': '51'})
    })
    $('#forGentleman').click(function () {
        $('#forLady').removeClass('activeGender')
        $('#forGentleman').addClass('activeGender')
        $('.sliderSec2First').css({'opacity': '0', 'z-index': '50'})
        $('.sliderSec2Second').css({'opacity': '1', 'z-index': '51'})
    });
    $('.zoomSertificate').fancybox({helpers: {overlay: {locked: false}}});
    var player, iframe;

// init player
    function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
            height: '360',
            width: '640',
            videoId: 'D0R0vNsPJCQ'
        });
    }
    $('.popUpCall').click(function () {
        $('#popUpCall').find('.nameInner').text($(this).attr('data-name-form'));
    })
    $('.popUpCall').fancybox({
        'showCloseButton': false,
        'closeBtn': false,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('.productPopUp').fancybox({
        'showCloseButton': false,
        'closeBtn': false,
        beforeShow: function () {
            var id = this.element;
            id = $(id).closest('.byin').attr('data-prod-id');
            for (var i = 0; i < prodArray.length; i++) {
                if (prodArray[i].virtuemart_product_id == id) {
                    $('#productPopUp').find('h2,.forNameForm').text(prodArray[i].product_name);
                    $('#productPopUp').find('.productPopUpInput').find('img').attr('src', prodArray[i].images[0].file_url);
                    $('#productPopUp').find('.productPopUpInput').find('ul').remove();
                    $('#productPopUp').find('.productPopUpInput').append(prodArray[i].descr);
                    break;
                }
            }
            $('.productPopUpInput ul li').each(function () {
                $(this).prepend('<i class="fa fa-check-circle-o"></i>')
            })
        },
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('.closeFancy').click(function () {
        $.fancybox.close();
    });
    $('.menuTable').click(function (e) {
        e.preventDefault();
        $('.headerMiddleinner').slideToggle(600);

    });
    $('section').click(function () {
        $('.headerMiddleinner').slideUp(600);
    })
    jQuery(function ($) {
        $('input[name="phone"]').mask("+7(999)999-99-99", {placeholder: '_'});
    });

    var formSend;
    formSend = function (formObject, namePopUp, target) {
        var fieldForm, sendAjaxAndResetForm, fieldArr, newFieldArr = [], taskMail = 'sendMail';
        fieldArr = formObject.find(':input,textarea,select').serializeArray();
        for (var c = 0; c < fieldArr.length; c++) {
            if (fieldArr[c].value.length > 0) {
                newFieldArr.push(fieldArr[c])
            }
        }
        if ($(formObject).hasClass('subscribeForm')) {
            taskMail = 'sendMailShares';
        }
        $.ajax({
            type: "POST",
            url: 'index.php?option=com_ajax&plugin=ajax&format=json',
            dataType: 'json',
            data: {
                formName: namePopUp,
                formData: newFieldArr,
                task: taskMail
            }
        }).done(function () {
            $.fancybox.close();
            $('#thanksPopUp').css('display', 'block')
            setTimeout(function () {
                $('#thanksPopUp').css('opacity', '1')
                $('form').trigger('reset');
            }, 300);
            setTimeout(function () {
                $('#thanksPopUp').css('opacity', '0')
            }, 1800)
            setTimeout(function () {
                $('#thanksPopUp').css('display', 'none')
            }, 2100);
            yaCounter35741200.reachGoal(target);
            return true;
        })
    }
    $(document).ready(function () {
        $('form:not(.ajaxStop)').each(function () {
            $(this).validate({
                errorPlacement: function (error, element) {
                    $('.error').next().addClass('errorVal');
                    $('.error').closest('.headerFormWrap').find('.formSpan').addClass('errorValHeader')
                    error.remove();
                },
                success: function () {
                    $('.valid').next().removeClass('errorVal');
                    $('.valid').closest('.headerFormWrap').find('.formSpan').removeClass('errorValHeader')
                },
                submitHandler: function (form) {

                    var object = $(form);
                    var namePopUp = $(object).find('.forNameForm').text();
                    var target = object.attr('data-target');
                    formSend(object, namePopUp, target);
                }

            });
        });
    });

    $('.autentification-tab>li').click(function () {
        var index = $(this).index();
        $('.autentification-tab>li').removeClass('active');
        $(this).addClass('active');
        $('.autentification-tab-item>li').removeClass('active').eq(index).addClass('active');
    })
    $('.register-type>li').click(function () {
        var index = $(this).index();
        $('.register-type>li').removeClass('active');
        $(this).addClass('active');
        $('.register-item>li').removeClass('active').eq(index).addClass('active');
    })

    $('.comment-button').click(function () {
        $(this).closest('.vk-comment').find('.comment-body').slideToggle(600);
    });
    $('.order-item .item-heading').click(function () {
        $('.order-item').find('.item-body').not($(this).closest('.order-item').find('.item-body')).slideUp(600);
        $(this).closest('.order-item').find('.item-body').slideToggle(600);
        $('.openorder').not($(this).find('.openorder')).addClass('fa-chevron-down').removeClass('fa-chevron-up')
        $(this).find('.openorder').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down')
    });

    $('.write-button').click(function () {
        $(this).closest('.form-item').find('input').removeAttr('readonly');
    });
    $('.lk-tab li').click(function () {
        var index = $(this).index();
        $('.lk-tab li').removeClass('active');
        $(this).addClass('active');
        $('.lk-tab-body li').removeClass('active').eq(index).addClass('active');
    });
    $('.prod-main-tab>li').click(function () {
        var index = $(this).index();
        $('.prod-main-tab>li').removeClass('active');
        $(this).addClass('active');
        $('.prod-main-tab-body>li').removeClass('active').eq(index).addClass('active');
    });
    $('.prod-inner-tab>li').click(function () {
        var index = $(this).index();
        $('.prod-inner-tab>li').removeClass('active');
        $(this).addClass('active');
        $('.prod-inner-tab-body>li').removeClass('active').eq(index).addClass('active');
    });

    function openCommonPopup(id) {
        $('.popupoverlay').fadeIn(400);
        $('#' + id).closest('.commonpopup').fadeIn(400);
    }

    $('.openpopupreg').click(function () {
        $('.popupoverlay,.autentification-popup').fadeIn(400);
    });

    $('.opencommonpopup').click(function () {
        var id = $(this).attr('data-id');
        openCommonPopup(id);
    });

    $('.close-popup,.popupoverlay').click(function () {
        $('.popupoverlay,.commonpopup,.autentification-popup').fadeOut(400);
    });
    $('.zoomSertificate').click(function () {
        var srcPath = ($(this).attr('data-path'))
        $('#imagePopUp').find('img').attr('src', srcPath);
    });
})


