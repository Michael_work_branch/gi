<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
if (count($items)):
    ?>
    <div class="sec6Wrap">
                <div>
                    <div>
                        <img src="images/section6/img1Sec6Cos.png">
                    </div>
                    <a class="btnStyle" href="<?=strip_tags($items[0]->introtext)?>">Скачайте инструкцию</a>
                </div>
                <div>
                    <div>
                        <img src="images/section6/img2Sec6Cos.png">
                    </div>
                    <a class="btnStyle" href="<?= strip_tags($items[0]->fulltext)?>">Посмотрите видеоурок</a>
                </div>
                <!-- <p class="orSec6">или</p>
                <div>
                    <div>
                        <img src="images/section6/img3Sec6Cos.png">
                    </div>
                    <button class="btnStyle">Посетите виртуальный<br>семинар</button>
                </div> -->
            </div>
<? endif; ?>