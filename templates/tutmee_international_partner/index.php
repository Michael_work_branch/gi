<?php
defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;
$user = JFactory::getUser();
$db = JFactory::getDbo();
$query = $db->getQuery(true);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=1200">
    <jdoc:include type="head" />
<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35741200 = new Ya.Metrika({id:35741200, webvisor:true, clickmap:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/35741200" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick-theme.css"/> 
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/jquery-ui.theme.min.css">
    <!--<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>-->
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/media/js/productplugin.js"></script> 
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.min.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.maskedinput.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.validate.min.js"></script>
</head>
<body>
    <div id="preloader">
        <div id="global">
            <div id="top" class="mask">
                <div class="plane"></div>
            </div>
            <div id="middle" class="mask">
                <div class="plane"></div>
            </div>
            <div id="bottom" class="mask">
                <div class="plane"></div>
            </div>
            <p><i>LOADING...</i></p>
        </div>
    </div>
    <a href="#popUpCall" id="fixedBtn" class="popUpCall animated" onclick="yaCounter35741200.reachGoal('cell_buttonclick_partner'); return true;">
        <div class="fixedBtnWrap animated"></div>
        <div>
            <i class="fa fa-phone"></i>
        </div>
    </a>

    <!--(готово)-----------------------------------------HEADER---------------------------------------------->
    <header>
        <!------------------------------------------PARALLAX----------------------------------------> 
        <div class="headerUp animated">
            <div>
                <div class="headerUpFirst">
                    <jdoc:include type="modules" name="top_phone" />
                </div>
                <div class="headerUpSecond">
                    <?php if ($user->guest): ?>
                        <div class="btnHeader">
                            <button class="openpopupreg"><i class="fa fa-pencil"></i>Sign In</button>
                        </div>
                    <?php else: ?>
                        <div class="btnHeader">
                            <a href="<?= JRoute::_('index.php?option=com_users&view=profile', false) ?>"><i class="fa fa-key"></i> Personal Area</a>
                        </div>
                        <div class="btnHeader">
                            <form class="ajaxStop" action="<?= JRoute::_(htmlspecialchars(JUri::getInstance()->toString())) ?>" method="post">
                                <input type="hidden" name="option" value="com_users" />
                                <input type="hidden" name="task" value="user.logout" />
                                <input type="hidden" name="return" value="./" />
                                <?php echo JHtml::_('form.token'); ?>
                                <button class="exitProfile">
                                    <i class="fa fa-sign-out"></i> Go out
                                </button>
                            </form>
                        </div>
                    <?php endif; ?>
                    <a href="#">Ru</a>
                    <!-- <a href="#">En</a> -->
                </div>
            </div>
        </div>
        <div class="headerMiddle animated">
            <div>
                <div class="headerMiddleFirst">
                    <div class="headerMiddleImgWrap">
                        <a href="./">
                            <img src="images/logo/logoLeft.png">
                            <img src="images/logo/logoRight.png">
                        </a>							
                    </div>
                    <a href="#" class="menuTable">
                        <i class="fa fa-bars animated"></i>
                        <p>Menu</p>
                    </a>
                </div>
                <div class="headerMiddleSecond">
                    <div class="headerFormWrap">
                        <span class="firstBorder formSpan"></span>
                        <span class="secondBorder formSpan"></span>
                        <span class="thirdBorder formSpan"></span>
                        <span class="fourthBorder formSpan"></span>
                        <span class="fifthBorder formSpan"></span>
                        <form class="subscribeForm" data-target="form_shares_partner">
                            <div>
                                <i class="fa fa-percent"></i>
                                <p>Subscribe to shares</p>
                            </div>
                            <div class="inFormInput">
                                <input type="text" name="email" placeholder="Your email" class="inputAnim usermail" onclick="yaCounter35741200.reachGoal('input_shares_partner'); return true;">
                                <button class="send headerSend">
                                    <i class="fa fa-envelope"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <a href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&lang=ru', false); ?>" class="basket">
                        <jdoc:include type="modules" name="my_cart" />
                    </a>
                </div>
            </div>
            <div class="headerMiddleinner">
                <div class="content-container">
                    <?php if ($this->countModules('mainmenu')) : ?>
                        <jdoc:include type="modules" name="mainmenu" />
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="headerDown">
            <canvas class="canvasAnimation"></canvas>
            <div>
                <img src="images/header/headerImagePart.png" class="girlAnimation">
                <div class="hWrap">
                    <h1>Creators</h1>
                    <h1>cosmeceutical line</h1>
                    <h1>premium GI BEAUTY</h1>
                </div>
                <h3>invite to cooperation</h3>
                <h4>The company «НПО ЛИОМАТРИКС» will be glad to new partners</h4>
                <p>distributors • sales agents • representatives</p>
            </div>
        </div>
    </header>
    <!--(готово)----------------------------SECTION-1-------------------------------->
    <section class="section1">
        <div>
            <h4>Profitable terms!<br>Goods effective action!</h4>
            <a href="#popUpCall" class="popUpCall animated btnStyle" data-name-form="Details manager «НПО ЛИОМАТРИКС»!" onclick="yaCounter35741200.reachGoal('podrobnosti_buttonclick_partner'); return true;">Details manager «НПО ЛИОМАТРИКС»!</a>
            <div class="sec1Clear"></div>
            <a href="<?= JRoute::_('images/pdf/present_partner.pdf') ?>" class="btnStyle" target="_blank">download presentation</a>
        </div>
        <div class="animationH2">
            <h2>Partners - unique conditions!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <div class="sec1WrapDiv">
                <div class="animated sec1Anim1">
                    <div>
                        <img src="images/section1/img2Sec1.png"> 
                    </div>
                    <p>Sale of exclusive cosmetics* installments or partial payment</p> 
                </div>
                <div class="animated sec1Anim2">
                    <div>
                        <img src="images/section1/img1Sec1.png">
                    </div>
                    <p>Lowest prices on the wholesale market</p>
                </div>
                <div class="animated sec1Anim3">
                    <div>
                        <img src="images/section1/img3Sec1.png"> 
                    </div>
                    <p>Reports of new arrivals</p>
                </div>
                <div class="animated sec1Anim3">
                    <div>
                        <img src="images/section1/img4Sec1.png"> 
                    </div>
                    <p>Possibility of remote training</p>
                </div>
            </div>
            <h6>*consignment - from 15 units of the same name</h6>
            <h4>Investment pays off quickly!<br>High Profit!</h4>
        </div>
    </section>
    <!--(нет договоров)----------------------------SECTION-2-------------------------------->
    <section class="section2">
        <div class="animationH2">
            <h2>You have decided to cooperate with us!</h2>
            <span></span>
            <span></span>
        </div>

        <div>
            <canvas class="canvasAnimation"></canvas>
            <div>
                <img src="images/section2/pdfIcon.png">
                <p>Download the partnership agreement</p>
            </div>
            <div class="sec2WrapDiv">
                <div class="animated sec2Anim1">
                    <div>
                        <img src="images/section2/img1Sec2Part.png">
                    </div>
                    <button class="btnStyle">for legal entities</button>
                </div>
                <div class="animated sec2Anim2">
                    <div>
                        <img src="images/section2/img2Sec2Part.png">
                    </div>
                    <button class="btnStyle">for individuals</button>
                </div>
            </div>
        </div> 
    </section>
    <!------------------------------SECTION-3-------------------------------->
    <section class="section3">
        <div class="animationH2">
            <h2>The undeniable advantages of production GI BEAUTY</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <jdoc:include type="modules" name="video_1"/>
            <jdoc:include type="modules" name="video_2"/>
        </div>
    </section>
    <!--(готово)------------------------------------------SECTION-4-------------------------------->
    <section class="section4">
        <div class="animationH2">
            <h2>Products for women and men</h2>
            <span></span>
            <span></span>
        </div>
    </section>
    <!--------------------------------------------SECTION-5-------------------------------->
    <section class="section5">
        <div class="sliderSec2-1 sliderSec2First partner">
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section5/img1Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    make an order
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section5/img2Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    make an order
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section5/img3Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    make an order
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section5/img4Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    make an order
                </div>
            </a>
            <a class="slidesSec2-1 productPopUp" href="#productPopUp">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section5/img4Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    make an order
                </div>
            </a>
        </div>
    </section>
    <!------------------------------SECTION-6-------------------------------->
    <section class="section6">
        <div>
            <p>Prices are acceptable!<br>For distributors, sales agents and representatives - wholesale!</p>
        </div>
        <div class="animationH2">
            <h2>Consultations provide free!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <canvas class="canvasAnimation"></canvas>
            <form method="POST" data-target="cell_form_partner">
                <p class="forNameForm">Consultations provide free!</p>
                <div>
                    <div class="inputStyle">
                        <input name="name" type="text">
                        <span></span>
                        <label>Your name</label>
                    </div>
                    <div class="inputStyle">
                        <input name="phone" type="text" class="userphone">
                        <span></span>
                        <label>Your phone number</label>
                    </div>
                </div>
                <button class="btnStyle send">make Up</button>
                <h6>Policy «НПО ЛИОМАТРИКС» - strict confidentiality of your personal data</h6>
            </form>
        </div>
    </section>
    <!------------------------------SECTION-7-------------------------------->
    <section class="section7">
        <h5>GI BEAUTY = steady demand!</h5>
        <div>
            <a class="animated sec7Anim1" href="<?= JRoute::_("index.php?option=com_content&view=featured&Itemid=143") ?>">
                <div class="sec7ImgWrap">
                    <img src="images/section7/img1Sec7Part.png">
                </div>
                <h5>at cosmeticians</h5>
            </a>
            <a class="animated sec7Anim2" href="<?= JRoute::_("index.php?option=com_content&view=featured&Itemid=144") ?>">
                <div class="sec7ImgWrap">
                    <img src="images/section7/img2Sec7Part.png">
                </div>
                <h5>in beauty salons and clinics</h5>
            </a>
            <a class="animated sec7Anim3" href="<?= JRoute::_("index.php?option=com_content&view=featured&Itemid=145") ?>">
                <div class="sec7ImgWrap">
                    <img src="images/section7/img3Sec7Part.png">
                </div>
                <h5>buyers</h5>
            </a>
            <a class="animated sec7Anim3" href="<?= JRoute::_("./") ?>">
                <div class="sec7ImgWrap">
                    <img src="images/section7/img4Sec7Part.png">
                </div>
                <h5>«НПО ЛИОМАТРИКС» credibility in the scientific community</h5>
            </a>
        </div>
    </section>
    <!------------------------------SECTION-8-------------------------------->
    <section class="section8">
        <div>
            <div class="slider-indicator">
                <h2>Our partners</h2>
                <span></span>
            </div>
            <div class="sliderSec8">
                <jdoc:include type="modules" name="partners"/>
            </div>
        </div>
    </section>
    <!--(готово)----------------------------SECTION-9-------------------------------->
    <section class="section9">
        <div class="animationH2">
            <h2>Our addresses and telephone numbers:</h2>
            <span></span>
            <span></span>
        </div>
        <div class="top-contacts-belt">
            <div>
                <i class="fa fa-map-marker"></i>
                <jdoc:include type="modules" name="bottom_adress"/>
            </div>
            <div>
                <i class="fa fa-phone"></i>
                <jdoc:include type="modules" name="bottom_phone"/>
            </div>
            <div>
                <i class="fa fa-envelope mailI"></i>
                <a href="mailto:<?= JFactory::getMailer()->From; ?>"  onclick="yaCounter35741200.reachGoal('mail_partner'); return true;"><?= JFactory::getMailer()->From; ?></a>
            </div>
        </div>
        <div id="mapWrap"></div>
    </section>
    <!------------------------------SECTION-10-------------------------------->
    <section class="section10">
        <div>
            <a href="#popUpCall" class="popUpCall animated btnStyle" data-name-form="You have decided to cooperate with us! Call us!»" onclick="yaCounter35741200.reachGoal('sotrudnic_buttonclick_partner'); return true;">You have decided to cooperate with us! Call us!</a>
            <h3>Giving beauty - a very lucrative business!</h3>
            <h3>Technologies of the future - today our!</h3>
            <?php
            $query->select('params')
                ->from('#__extensions')
                ->where('extension_id = 10066');
            $socials = $db->setQuery($query)->loadResult();
            $query->clear();
            $socials = json_decode($socials);
            ?>
            <div class="linksWrap">
                <a href="<?= $socials->fb ?>">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="<?= $socials->inst ?>">
                    <i class="fa fa-instagram"></i>
                </a>
                <a href="<?= $socials->tw ?>">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="<?= $socials->vk ?>">
                    <i class="fa fa-vk"></i>
                </a>
                <a href="<?= $socials->ok ?>">
                    <i class="fa fa-odnoklassniki"></i>
                </a>
                <a href="<?= $socials->pi ?>">
                    <i class="fa fa-pinterest-p"></i>
                </a>
                <a href="<?= $socials->in ?>">
                    <i class="fa fa-linkedin"></i>
                </a>
            </div>
        </div>
    </section>
    <!----------------------------------SECTION15-------------------------------------------->
    <footer>
        <div class="footerUp">
            <div>
                <a href="./">
                    <img src="images/logo/logoFooter.png">
                </a>
                <jdoc:include type="modules" name="footer_menu" />
            </div>
        </div>
        <div class="footerDown">
            <div>
                <div>
                    <span>©</span>
                    <p>2015 ООО «НПО Лиоматрикс»</p>
                </div>
                <div>
                    <a href="http://tutmee.ru/">
                        <img src="images/logo/tutmee.png">
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <div class="popupoverlay"></div>
    <div class="autentification-popup" id="popupreg">        <!-- если в URL есть якорь #popupreg отлкрывать форму при загрузке страницы-->   
        <ul class="autentification-tab">
            <li><span class="fa fa-pencil"></span>check in</li>
            <li class="active"><span class="fa fa-key"></span>login</li>                           
        </ul>
        <table>
            <tr>
                <td>
                    <ul class="autentification-tab-item">
                        <li>
                            <!--                        <div class="error-form-place">
                                                        Ошибка формы !!!!
                                                    </div>-->
                            <form class="ajaxStop" action="<?php echo JRoute::_('index.php?option=com_users&task=gireg.register'); ?>" method="post">
                                <!--                                    <ul class="register-type">
                                                                        <li class="active">для физических лиц <div class="hide-radio"><input type="radio"  name="rtype"/></div></li>
                                                                        <li>для юридических лиц <div class="hide-radio"><input type="radio"  name="rtype"/></li>
                                                                    </ul>-->
                                <ul class="register-item">
                                    <li class="active">
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[name]" /><label>Your Name (required)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password1]" /><label>Password (required)</label></div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[email1]" /><label>Your email address (required)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password2]" /><label>Confirm Password (required)</label></div>
                                        </fieldset>
                                        <div class="capcha-check-container">
                                            <fieldset>
                                                <div class="capcha-block">
                                                    <div class="g-recaptcha" data-sitekey="6LdqdRgTAAAAAPYF1ypIO5UDbI1SaSqlNsvrPK5p"></div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="check-container">
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"> </label></div>
                                                        <span>Receive news by email service</span>
                                                    </div>
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"></label></div>
                                                        <span>I have read and agree to the terms and conditions <a href="#">Privacy policy</a></span>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <input type="hidden" name="option" value="com_users" />
                                        <input type="hidden" name="task" value="gireg.register" />
                                        <?php echo JHtml::_('form.token'); ?>
                                        <button>sign up</button>
                                    </li>
                                    <!--                                        <li>
                                    
                                                                                <fieldset>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Название организации или ИП (обязательно)</label></div>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Email (обязательно) </label></div>
                                                                                </fieldset>
                                                                                <fieldset>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Контактное лицо</label></div>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Номер телефона (обязательно)</label></div>
                                                                                </fieldset>
                                                                                <div class="password-block">
                                                                                    <fieldset><div class="form-item" data-hint=""><input type="password" /><label>Пароль(обязательно)</label></div></fieldset>
                                                                                    <fieldset><div class="form-item" data-hint=""><input type="password" /><label>Подтверждение пароля(обязательно)</label></div></fieldset>
                                                                                </div>
                                                                                <div class="capcha-check-container">
                                                                                    <fieldset>
                                                                                        <div class="capcha-block"><img src="images/capha.png" alt="" /></div>
                                                                                    </fieldset>
                                                                                    <fieldset>
                                                                                        <div class="check-container">
                                                                                            <div class="check-block">
                                                                                                <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"> </label></div>
                                                                                                <span>Получать на email новости сервиса</span>
                                                                                            </div>
                                                                                            <div class="check-block">
                                                                                                <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"></label></div>
                                                                                                <span>Я ознакомлен и согласен с условиями <a href="#">политики конфиденциальности</a></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </fieldset>
                                                                                </div>
                                                                                <button>зарегистрироваться</button>
                                    
                                                                            </li>-->
                                </ul>
                            </form>
                        </li>
                        <li class="active">
                        <jdoc:include type="modules" name="login" />
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        <div class="close-popup"></div>               
    </div>
    <div id="popUpCall">
        <form  method="POST" data-target="cell_form_partner">
            <h2>Request a call</h2>
            <p class="forNameForm">Request a call</p>
            <div>
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Your name</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone">
                    <span></span>
                    <label>Your phone number</label>
                </div>
            </div>
            <button class="btnStyle send">Send</button>
            <div class="closeFancy">
                <span></span>
                <span></span>
            </div>
        </form>
    </div>
    <div id="productPopUp">
        <form method="POST" data-target="byin_form_partner">
            <h2>Triple Action Face Mask GI BEAUTY</h2>
            <p class="forNameForm">Triple Action Face Mask GI BEAUTY</p>
            <div class="productPopUpInput">
                <div>
                    <img src="#">
                </div>
                <ul>
                    <li><i class="fa fa-check-circle"></i><p>regeneration (restoring)</p></li>
                    <li><i class="fa fa-check-circle"></i><p>face hydration</p></li>
                    <li><i class="fa fa-check-circle"></i><p>anti-age affect</p></li>
                </ul>
            </div>
            <div>
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Enter your name</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone">
                    <span></span>
                    <label>Enter your phone</label>
                </div>
                <div class="inputStyle">
                    <input name="email" type="text" class="usermail">
                    <span></span>
                    <label>Enter your email</label>
                </div>
            </div>
            <button class="btnStyle">send</button>
            <div class="closeFancy">
                <span></span>
                <span></span>
            </div>   
        </form>
    </div>
    <div id="imagePopUp">
        <img src="">
    </div>
    <div class="commonpopup" id="thanksPopUp">
        <div class="close-popup"></div>
        <div id="alert">
            <table>
                <tr>
                    <td>
                        <h2>Thank you for your application</h2>
                        <h3>all messages will be sent to the address <a href="mailto:<?= JFactory::getMailer()->From; ?>"  onclick="yaCounter35741200.reachGoal('mail_partner'); return true;"><?= JFactory::getMailer()->From; ?></a></h3>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script src="https://www.youtube.com/iframe_api"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/script.js"></script>
</body>
</html>
