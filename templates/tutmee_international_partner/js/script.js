jQuery(function ($) {
    $.validator.addMethod(
            'regexp',
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );

    $.validator.addClassRules({
        userphone: {
            required: true,
            minlength: 15,
            regexp: '[^_]+$'
        },
        usermail: {
            email: true
        }
    });
    $(window).load(function () {
        function loadpage() {

            // pontosDeCanvas();
            setTimeout(function () {
                var windowWidth = document.documentElement.clientWidth;
                if (windowWidth >= 1560) {
                    // $('.sliderSec3').slick({
                    //     slidesToShow:4,
                    //     slidesToScroll:2,
                    //     infinite:true,
                    //     autoplay:true,
                    //     autoplaySpeed:5000
                    // });
//            $('.sliderSec2-1').slick({
//                slidesToShow:4,
//                slidesToScroll:2,
//                infinite:true,
//                arrows:true
//            })
                    $('.sliderSec9').slick({
                        slidesToShow: 4,
                        slidesToScroll: 2,
                        infinite: true,
                        arrows: true
                    })
                }
                if (windowWidth < 1560) {
                    // $('.sliderSec3').slick({
                    //     slidesToShow:3,
                    //     slidesToScroll:2,
                    //     infinite:true,
                    //     autoplay:true,
                    //     autoplaySpeed:5000
                    // });
//            $('.sliderSec2-1').slick({
//                slidesToShow:3,
//                slidesToScroll:2,
//                infinite:true,
//                arrows:true
//            })
                    $('.sliderSec9').slick({
                        slidesToShow: 3,
                        slidesToScroll: 2,
                        infinite: true,
                        arrows: true
                    })
                }
                if (windowWidth <= 1460 && windowWidth > 1360) {
                    $('.sliderSec8').slick({
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        infinite: false,
                        arrows: true,
                        swipe: false
                    });
                    $('.slider-indicator span').css('width', '25%');
                    $('.sliderSec8 .slick-prev').click(function () {
                        widthCurrent = $('.slider-indicator span')[0].style.width;
                        widthCurrent = parseInt(widthCurrent.split("%")[0])
                        if (widthCurrent - 25 > 0) {
                            $('.slider-indicator span').width(widthCurrent - 25 + '%')
                        }
                    })
                    $('.sliderSec8 .slick-next').click(function () {
                        widthCurrent = $('.slider-indicator span')[0].style.width;
                        widthCurrent = parseInt(widthCurrent.split("%")[0])
                        if (widthCurrent + 25 <= 100) {
                            $('.slider-indicator span').width(widthCurrent + 25 + "%")
                        }
                    })

                }
                if (windowWidth <= 1360) {
                    $('.sliderSec8').slick({
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: false,
                        arrows: true,
                        swipe: false
                    });
                    $('.slider-indicator span').css('width', '20%');
                    $('.sliderSec8 .slick-prev').click(function () {
                        widthCurrent = $('.slider-indicator span')[0].style.width;
                        widthCurrent = parseInt(widthCurrent.split("%")[0])
                        if (widthCurrent - 20 > 0) {
                            $('.slider-indicator span').width(widthCurrent - 20 + '%')
                        }
                    })
                    $('.sliderSec8 .slick-next').click(function () {
                        widthCurrent = $('.slider-indicator span')[0].style.width;
                        widthCurrent = parseInt(widthCurrent.split("%")[0])
                        if (widthCurrent + 20 <= 100) {
                            $('.slider-indicator span').width(widthCurrent + 20 + "%")
                        }
                    })
                }
                if (windowWidth > 1460) {
                    $('.sliderSec8').slick({
                        slidesToShow: 5,
                        slidesToScroll: 5,
                        infinite: false,
                        arrows: true,
                        swipe: false
                    })
                    $('.slider-indicator span').css('width', '33.3333%');
                    $('.sliderSec8 .slick-prev').click(function () {
                        widthCurrent = $('.slider-indicator span')[0].style.width;
                        widthCurrent = parseInt(widthCurrent.split("%")[0])
                        if (widthCurrent - 33.3333 > 0) {
                            $('.slider-indicator span').width(widthCurrent - 33.3333 + '%')
                        }
                    })
                    $('.sliderSec8 .slick-next').click(function () {
                        widthCurrent = $('.slider-indicator span')[0].style.width;
                        widthCurrent = parseInt(widthCurrent.split("%")[0])
                        if (widthCurrent + 33.3333 <= 100) {
                            $('.slider-indicator span').width(widthCurrent + 33.3333 + "%")
                        }
                    })
                }
                $('#preloader').addClass("animated fadeOut")
                setTimeout(function () {
                    $('#preloader').css("display", "none");
                    $('.girlAnimation').css('opacity', '1')
                    $('#fixedBtn').addClass('bounceInRight').css('opacity', '1');
                    $('.headerFormWrap span').eq(0).css('width', '11px');
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(1).css('height', '100%');
                    }, 100)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(2).css('width', '100%');
                    }, 400)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(3).css('height', '100%');
                    }, 1300)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(4).css('width', '277px');
                    }, 1600)
                    setTimeout(function () {
                        $('#fixedBtn').removeClass('bounceInRight')
                    }, 900)
                }, 500)
            }, 300)
        }
        loadpage();
        $(document).on('click', '.byin', function () {
            yaCounter35741200.reachGoal('byin_buttonclick_partner');
            return true;
        });
    });
    function footerToBottom() {
        console.log('asdasdsd')
        var browserHeight = $(window).height(),
                footerOuterHeight = $('footerWrapperAll').outerHeight(true),
                mainHeightMarginPaddingBorder = $('.mainWrapper').outerHeight(true) - $('.mainWrapper').height();
        $('.mainWrapper').css({
            'min-height': browserHeight - footerOuterHeight - mainHeightMarginPaddingBorder,
        });
    }
    ;
    footerToBottom();
    $(window).resize(function () {
        footerToBottom();
    });
    /*--------------------------btn-animation------------------------------*/
    $('.menuTable').mouseover(function () {
        $('.fa-bars').addClass('jello');
        setTimeout(function () {
            $('.fa-bars').removeClass('jello');
        }, 700)
    })
    $('.basket').mouseover(function () {
        $('.fa-shopping-bag').addClass('jello');
        setTimeout(function () {
            $('.fa-shopping-bag').removeClass('jello');
        }, 700)
    })
    $('#fixedBtn').click(function () {
        $('#fixedBtn').addClass('rubberBand');
        setTimeout(function () {
            $('#fixedBtn').removeClass('rubberBand');
        }, 700)
    })
    /*--------------------------INPUT ANIMATION-----------------------------*/
    $('.headerFormWrap input').hover(function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    }, function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    })
    $('.slider-range').slider({
        range: "min",
        min: 18,
        max: 80,
        value: 35,
        slide: function (event, ui) {
            $("#ageInput").val(ui.value + " года");
            $(".ui-slider-range").attr('data-attribute', ui.value);
            $(this).closest('.checkBtnWrap>div').addClass('fixUiSlider');
        }
    })
    $("#ageInput").val($("#slider-range").slider("value") + ' года');
    $(".ui-slider-range").attr('data-attribute', '35');


    $('input[name="gender"]').click(function () {
        $('.sec11WrapBottom').addClass('bounceIn').css({'display': 'block', 'opacity': '1'})
    });
    $('.productPopUp').fancybox({
        'showCloseButton': false,
        'closeBtn': false,
        beforeShow: function () {
            var id = this.element;
            id = $(id).closest('.byin').attr('data-prod-id');
            for (var i = 0; i < prodArray.length; i++) {
                if (prodArray[i].virtuemart_product_id == id) {
                    $('#productPopUp').find('h2,.forNameForm').text(prodArray[i].product_name);
                    $('#productPopUp').find('.productPopUpInput').find('img').attr('src', prodArray[i].images[0].file_url);
                    $('#productPopUp').find('.productPopUpInput').find('ul').remove();
                    $('#productPopUp').find('.productPopUpInput').append(prodArray[i].descr);
                    break;
                }
            }
            $('.productPopUpInput ul li').each(function () {
                $(this).prepend('<i class="fa fa-check-circle-o"></i>')
            })
        },
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    var afterChange;
    $('.slider1Sec10,.slider2Sec10').slick({
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 3,
        dots: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            }
        ]
    });
    (function () {
        var API = google.maps;
        var $container = $('#mapWrap');
        if ($container.length == 0)
            return;
        var mapPosition = new API.LatLng(55.776597, 37.542144);
        var markerPosition = new API.LatLng(55.776597, 37.542144);
        var map = new API.Map($container[0], {
            center: mapPosition,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            panControl: false,
            zoom: 14
        });
        var marker = new API.Marker({
            map: map, position: markerPosition, icon: 'images/marker.png'
        });
        var iw = new google.maps.InfoWindow({
            content: '<div style="text-align: left; height: 45px; font-size: 18px; position:relative; z-index:1;">г.Реутов<br>ул.Безымянная дом 0</div>'
        });
    })();
    var flag4 = true;
    $(window).scroll(function () {
        var heightWindow = document.documentElement.clientHeight / 2.5;
        if ($(this).scrollTop() >= $('.section1').offset().top - heightWindow) {
            $('.section1>div:nth-of-type(2) span').addClass('animatedSpan');
            $('.sec1Anim1').addClass('flipInX').css('opacity', '1')
            setTimeout(function () {
                $('.sec1Anim2').addClass('flipInX').css('opacity', '1')
            }, 300)
            setTimeout(function () {
                $('.sec1Anim3').addClass('flipInX').css('opacity', '1')
            }, 600)
        }
        if ($(this).scrollTop() >= $('.section2').offset().top - heightWindow) {
            $('.section2>div:nth-of-type(1) span').addClass('animatedSpan');
            $('.sec2Anim1').addClass('fadeInLeft').css('opacity', '1')
            $('.sec2Anim2').addClass('fadeInRight').css('opacity', '1')
        }
        if ($(this).scrollTop() >= $('.section3').offset().top - heightWindow) {
            $('.section3>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section4').offset().top - heightWindow) {
            $('.sec4Anim1-1').addClass('flipInX').css('opacity', '1')
            setTimeout(function () {
                $('.sec4Anim1-2').addClass('animatedFinal');
            }, 100)
            setTimeout(function () {
                $('.sec4Anim1-3').addClass('animatedFinal');
                $('.sec4Anim2-1').addClass('flipInX').css('opacity', '1')
            }, 200)
            setTimeout(function () {
                $('.sec4Anim2-2').addClass('animatedFinal');
            }, 300)
            setTimeout(function () {
                $('.sec4Anim2-3').addClass('animatedFinal');
                $('.sec4Anim3-1').addClass('flipInX').css('opacity', '1')
            }, 400)
            setTimeout(function () {
                $('.sec4Anim3-2').addClass('animatedFinal');
            }, 500)
            setTimeout(function () {
                $('.sec4Anim3-3').addClass('animatedFinal');
                $('.sec4Anim4-1').addClass('flipInX').css('opacity', '1')
            }, 600)
            setTimeout(function () {
                $('.sec4Anim4-2').addClass('animatedFinal');
            }, 700)
            setTimeout(function () {
                $('.sec4Anim4-3').addClass('animatedFinal');
                $('.sec4Anim5-1').addClass('flipInX').css('opacity', '1')
            }, 800)
            setTimeout(function () {
                $('.sec4Anim5-2').addClass('animatedFinal');
            }, 900)
            setTimeout(function () {
                $('.sec4Anim5-3').addClass('animatedFinal');
                $('.sec4Anim6-1').addClass('flipInX').css('opacity', '1')
            }, 1000)
            setTimeout(function () {
                $('.sec4Anim6-2').addClass('animatedFinal');
            }, 900)
            setTimeout(function () {
                $('.sec4Anim6-3').addClass('animatedFinal');
            }, 1000)
        }
        if ($(this).scrollTop() >= $('.section6').offset().top - heightWindow) {
            $('.section6>div:nth-of-type(1) span').addClass('animatedSpan');
            $('.sec6Anim1-1').addClass('fadeInLeft').css('opacity', '1');
            $('.sec6Anim1-2').addClass('fadeInRight').css('opacity', '1');
            setTimeout(function () {
                $('.sec6Anim2-1').addClass('fadeInLeft').css('opacity', '1');
                $('.sec6Anim2-2').addClass('fadeInRight').css('opacity', '1');
            }, 400)
            setTimeout(function () {
                $('.sec6Anim3-1').addClass('fadeInLeft').css('opacity', '1');
                $('.sec6Anim3-2').addClass('fadeInRight').css('opacity', '1');
            }, 800)

        }
        if ($(this).scrollTop() >= $('.section7').offset().top - heightWindow) {
            $('.sec7Anim2').addClass('fadeInLeft').css('opacity', '1');
            $('.sec7Anim3').addClass('fadeInRight').css('opacity', '1');
            setTimeout(function () {
                $('.sec7Anim1').addClass('fadeInLeft').css('opacity', '1')
                $('.sec7Anim4').addClass('fadeInRight').css('opacity', '1')
            }, 200)
        }
        if ($(this).scrollTop() >= $('.section5').offset().top - heightWindow) {
            $('.section4>div:nth-of-type(2) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section6').offset().top - heightWindow) {
            $('.section6>div:nth-of-type(2) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section9').offset().top - heightWindow) {
            $('.section9>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() > $('.section1').offset().top - 100) {
            if (flag4 == true) {
                $('.headerDown').css('margin-top', '153px');
                $('.headerUp').addClass('headerUpFixed fadeInDown');
                $('.headerMiddle').addClass('headerMiddleFixed fadeInDown');
                flag4 = false;
            }
        }
        if ($(this).scrollTop() < $('.section1').offset().top - 100) {
            if (flag4 == false) {
                $('.headerMiddle').removeClass('fadeInDown').addClass('fadeOutUp');
                $('.headerUp').removeClass('fadeInDown').addClass('fadeOutUp');
                setTimeout(function () {
                    $('.headerDown').css('margin-top', '0')
                    $('.headerUp').removeClass('headerUpFixed fadeOutUp').css('opacity', '1');
                    $('.headerMiddle').removeClass('headerMiddleFixed fadeOutUp').css('opacity', '1');
                }, 600)
                flag4 = true;
            }
        }
    })
    /*-------------------------canvas animation---------------------------*/



    function canvasDots1(canvas, ctx) {
        var mousePosition = {
            x: 30 * canvas.width / 100,
            y: 30 * canvas.height / 100
        };

        var dots = {
            nb: 350,
            distance: 60,
            d_radius: 200,
            array: []
        };

        function Dot() {
            this.x = Math.random() * canvas.width;
            this.y = Math.random() * canvas.height;

            this.vx = -.5 + Math.random();
            this.vy = -.5 + Math.random();

            this.radius = Math.random() * 3;
        }

        Dot.prototype = {
            create: function () {
                ctx.beginPath();
                ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false);
                ctx.fill();
            },
            animate: function () {
                for (i = 0; i < dots.nb; i++) {

                    var dot = dots.array[i];

                    if (dot.y < 0 || dot.y > canvas.height) {
                        dot.vx = dot.vx;
                        dot.vy = -dot.vy;
                    }
                    else if (dot.x < 0 || dot.x > canvas.width) {
                        dot.vx = -dot.vx;
                        dot.vy = dot.vy;
                    }
                    dot.x += dot.vx;
                    dot.y += dot.vy;
                }
            },
            line: function () {
                for (i = 0; i < dots.nb; i++) {
                    for (j = 0; j < dots.nb; j++) {
                        i_dot = dots.array[i];
                        j_dot = dots.array[j];

                        if ((i_dot.x - j_dot.x) < dots.distance && (i_dot.y - j_dot.y) < dots.distance && (i_dot.x - j_dot.x) > -dots.distance && (i_dot.y - j_dot.y) > -dots.distance) {
                            if ((i_dot.x - mousePosition.x) < dots.d_radius && (i_dot.y - mousePosition.y) < dots.d_radius && (i_dot.x - mousePosition.x) > -dots.d_radius && (i_dot.y - mousePosition.y) > -dots.d_radius) {
                                ctx.beginPath();
                                ctx.moveTo(i_dot.x, i_dot.y);
                                ctx.lineTo(j_dot.x, j_dot.y);
                                ctx.stroke();
                                ctx.closePath();
                            }
                        }
                    }
                }
            }
        };

        function createDots() {
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            for (i = 0; i < dots.nb; i++) {
                dots.array.push(new Dot());
                dot = dots.array[i];

                dot.create();
            }

            dot.line();
            dot.animate();
        }

        canvas.onmousemove = function (parameter) {
            mousePosition.x = parameter.offsetX;
            mousePosition.y = parameter.offsetY;
        }
        mousePosition.x = window.innerWidth / 2;
        mousePosition.y = window.innerHeight / 2;

        setInterval(createDots, 1000 / 30);



    }

    var count = 0;
    var canvasArr = document.getElementsByClassName('canvasAnimation');
    for (var l = 0; l < canvasArr.length; l++) {
        var ctx = canvasArr[count].getContext('2d'),
                colorDot = '#AAAAAF',
                color = '#AAAAAF';
        canvasArr[count].width = canvasArr[count].offsetWidth;
        canvasArr[count].height = canvasArr[count].offsetHeight;
        canvasArr[count].style.display = 'block';
        ctx.fillStyle = colorDot;
        ctx.lineWidth = .4;
        ctx.strokeStyle = color;
        canvasDots1(canvasArr[count], ctx);
        count++;
    }

    $('.popUpCall').fancybox({
        'showCloseButton': false,
        'closeBtn': false,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('.menuTable').click(function (e) {
        e.preventDefault();
        $('.headerMiddleinner').slideToggle(600);
    });
    $('section').click(function () {
        $('.headerMiddleinner').slideUp(600);
    })
    $('.closeFancy').click(function () {
        $.fancybox.close();
    })
    jQuery(function ($) {
        $('input[name="phone"]').mask("+7(999)999-99-99", {placeholder: '_'});
    });
    $(document).ready(function () {
        var formSend;
        formSend = function (formObject, namePopUp, productName, target) {
            var fieldForm, sendAjaxAndResetForm, fieldArr, newFieldArr = [], c, nameObg = {}, taskMail = 'sendMail';
            fieldArr = formObject.find(':input,textarea,select').serializeArray();
            for (c = 0; c < fieldArr.length; c++) {
                if (fieldArr[c].value.length > 0) {
                    newFieldArr.push(fieldArr[c])
                }
            }
            if (productName) {
                nameObg.name = "product_name";
                nameObg.value = productName;
                newFieldArr.push(nameObg);
            }
            if ($(formObject).hasClass('subscribeForm')) {
                taskMail = 'sendMailShares';
            }
            $.ajax({
                type: "POST",
                url: 'index.php?option=com_ajax&plugin=ajax&format=json',
                dataType: 'json',
                data: {
                    subject: namePopUp,
                    formData: newFieldArr,
                    task: taskMail
                }
            }).done(function () {
                $.fancybox.close();
                $('#thanksPopUp').css('display', 'block')
                setTimeout(function () {
                    $('#thanksPopUp').css('opacity', '1')
                    $('form').trigger('reset');
                }, 300);
                setTimeout(function () {
                    $('#thanksPopUp').css('opacity', '0')
                }, 1800)
                setTimeout(function () {
                    $('#thanksPopUp').css('display', 'none')
                }, 2100);
                yaCounter35741200.reachGoal(target);
                return true;
            })
        }
        $('form:not(.ajaxStop)').each(function () {
            $(this).find('.inputStyle').find('div').remove();
            $(this).validate({
                errorPlacement: function (error, element) {
                    $('.error').next().addClass('errorVal');
                    $('.error').closest('.headerFormWrap').find('.formSpan').addClass('errorValHeader')
                    error.remove();
                },
                success: function () {
                    $('.valid').next().removeClass('errorVal');
                    $('.valid').closest('.headerFormWrap').find('.formSpan').removeClass('errorValHeader')
                },
                submitHandler: function (form) {

                    var object = $(form);
                    var namePopUp = $(object).find('.forNameForm').text();
                    var productName = $(object).find('.nameProductLink').text();
                    var target = object.attr('data-target');
                    formSend(object, namePopUp, productName, target);
                }

            });
        });
        $('.autentification-tab>li').click(function () {
            var index = $(this).index();
            $('.autentification-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.autentification-tab-item>li').removeClass('active').eq(index).addClass('active');
        })
        $('.register-type>li').click(function () {
            var index = $(this).index();
            $('.register-type>li').removeClass('active');
            $(this).addClass('active');
            $('.register-item>li').removeClass('active').eq(index).addClass('active');
        })

        $('.comment-button').click(function () {
            $(this).closest('.vk-comment').find('.comment-body').slideToggle(600);
        });
        $('.order-item .item-heading').click(function () {
            $('.order-item').find('.item-body').not($(this).closest('.order-item').find('.item-body')).slideUp(600);
            $(this).closest('.order-item').find('.item-body').slideToggle(600);
            $('.openorder').not($(this).find('.openorder')).addClass('fa-chevron-down').removeClass('fa-chevron-up')
            $(this).find('.openorder').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down')
        });

        $('.write-button').click(function () {
            $(this).closest('.form-item').find('input').removeAttr('readonly');
        });
        $('.lk-tab li').click(function () {
            var index = $(this).index();
            $('.lk-tab li').removeClass('active');
            $(this).addClass('active');
            $('.lk-tab-body li').removeClass('active').eq(index).addClass('active');
        });
        $('.prod-main-tab>li').click(function () {
            var index = $(this).index();
            $('.prod-main-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.prod-main-tab-body>li').removeClass('active').eq(index).addClass('active');
        });
        $('.prod-inner-tab>li').click(function () {
            var index = $(this).index();
            $('.prod-inner-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.prod-inner-tab-body>li').removeClass('active').eq(index).addClass('active');
        });


        function openCommonPopup(id) {
            $('.popupoverlay').fadeIn(400);
            $('#' + id).closest('.commonpopup').fadeIn(400);
        }

        $('.openpopupreg').click(function () {
            $('.popupoverlay,.autentification-popup').fadeIn(400);
        });

        $('.opencommonpopup').click(function () {
            var id = $(this).attr('data-id');
            openCommonPopup(id);
        });

        $('.close-popup,.popupoverlay').click(function () {
            $('.popupoverlay,.commonpopup,.autentification-popup').fadeOut(400);
        });
        $('.zoomSertificate').click(function () {
            var srcPath = ($(this).attr('data-path'))
            $('#imagePopUp').find('img').attr('src', srcPath);
        });
    });
})









