<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
if (count($items)):
    foreach ($items[0]->attachments as $key => $item):
        ?>
        <div class="slidesSec8">
            <div class="logoName">
                <p><?= $item->title ?></p>
            </div>
            <div class="logoWrapSec8">
                <div>
                    <a href="<?= $item->titleAttribute ?>" target="_blank"><img src="<?= $item->link ?>"></a>
                </div>
            </div>
        </div>
        <?php
    endforeach;
endif;
?>
