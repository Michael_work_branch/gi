<?php
defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;
$user = JFactory::getUser();
$db = JFactory::getDbo();
$query = $db->getQuery(true);
//$config=JFactory::getConfig()->get('mailfrom');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=1200">
    <jdoc:include type="head" />
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35741200 = new Ya.Metrika({id:35741200, webvisor:true, clickmap:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/35741200" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick-theme.css"/> 
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/jquery-ui.theme.min.css">
    <!--<script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery-2.1.4.min.js"></script>--> 
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.maskedinput.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/media/js/productplugin.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.validate.min.js"></script> 
</head>
<body>
    <div id="preloader">
        <div id="global">
            <div id="top" class="mask">
                <div class="plane"></div>
            </div>
            <div id="middle" class="mask">
                <div class="plane"></div>
            </div>
            <div id="bottom" class="mask">
                <div class="plane"></div>
            </div>
            <p><i>LOADING...</i></p>
        </div>
    </div>
    <a href="#popUpCall" id="fixedBtn" class="popUpCall animated" data-name-form="Request a call" onclick="yaCounter35741200.reachGoal('cell_buttonclick_selebrity'); return true;">
        <div class="fixedBtnWrap animated"></div>
        <div>
            <i class="fa fa-phone"></i>
        </div>
    </a>
    <!-------------------------------------------HEADER---------------------------------------------->
    <!------------------------------------------PARALLAX---------------------------------------->
    <header>
        <span class="parallaxFixed parFixed1"></span>
        <span class="parallaxFixed parFixed2"></span>
        <span class="parallaxFixed parFixed3"></span>
        <span class="parallaxFixed parFixed4"></span>
        <span class="parallaxFixed parFixed5"></span>
        <span class="parallaxFixed parFixed6"></span>
        <span class="parallaxFixed parFixed7"></span>
        <span class="parallaxFixed parFixed8"></span>
        <span class="parallaxFixed parFixed9"></span>
        <span class="parallaxFixed parFixed10"></span>
        <span class="parallaxFixed parFixed11"></span>
        <span class="parallaxFixed parFixed12"></span>
        <span class="parallaxFixed parFixed13"></span>
        <span class="parallaxFixed parFixed14"></span>
        <span class="parallaxFixed parFixed15"></span>
        <span class="parallaxFixed parFixed16"></span>
        <span class="parallaxFixed parFixed17"></span>
        <span class="parallaxFixed parFixed18"></span>
        <span class="parallaxFixed parFixed19"></span>
        <span class="parallaxFixed parFixed20"></span>
        <span class="parallaxFixed parFixed21"></span>
        <span class="parallaxFixed parFixed22"></span>
        <span class="parallaxFixed parFixed23"></span>
        <span class="parallaxFixed parFixed24"></span>
        <span class="parallaxFixed parFixed24"></span>
        <span class="parallaxFixed parFixed26"></span>
        <span class="parallaxFixed parFixed27"></span>
        <span class="parallaxFixed parFixed28"></span>
        <span class="parallaxFixed parFixed29"></span>
        <span class="parallaxFixed parFixed30"></span>
        <span class="parallaxFixed parFixed31"></span>
        <span class="parallaxFixed parFixed32"></span>

        <div class="headerUp animated">
            <div>
                <div class="headerUpFirst">
                    <jdoc:include type="modules" name="top_phone" />
                </div>
                <div class="headerUpSecond">
                    <?php if ($user->guest): ?>
                        <div class="btnHeader">
                            <button class="openpopupreg"><i class="fa fa-pencil"></i> Sign In</button>
                        </div>
                    <?php else: ?>
                        <div class="btnHeader">
                            <a href="<?= JRoute::_('index.php?option=com_users&view=profile', false) ?>"><i class="fa fa-key"></i> Personal Area</a>
                        </div>
                        <div class="btnHeader">
                            <form class="ajaxStop" action="<?= JRoute::_(htmlspecialchars(JUri::getInstance()->toString())) ?>" method="post">
                                <input type="hidden" name="option" value="com_users" />
                                <input type="hidden" name="task" value="user.logout" />
                                <input type="hidden" name="return" value="./" />
                                <?php echo JHtml::_('form.token'); ?>
                                <button class="exitProfile">
                                    <i class="fa fa-sign-out"></i> Go out
                                </button>
                            </form>
                        </div>
                    <?php endif; ?>
                    <a class="active" href="#">Ru</a>
                    <!-- <a href="#">En</a> -->
                </div>
            </div>
        </div>
        <div class="headerMiddle animated">
            <div>
                <div class="headerMiddleFirst">
                    <div class="headerMiddleImgWrap">
                        <a href="./">
                            <img src="images/logo/logoLeft.png">
                            <img src="images/logo/logoRight.png">
                        </a>                            
                    </div>
                    <a href="#" class="menuTable">
                        <i class="fa fa-bars animated"></i>
                        <p>Menu</p>
                    </a>
                </div>
                <div class="headerMiddleSecond">
                    <div class="headerFormWrap">
                        <span class="firstBorder formSpan"></span>
                        <span class="secondBorder formSpan"></span>
                        <span class="thirdBorder formSpan"></span>
                        <span class="fourthBorder formSpan"></span>
                        <span class="fifthBorder formSpan"></span>
                        <form class="subscribeForm" data-target="shares_form_selebrity">
                            <p class="forNameForm">Subscribe to shares</p>
                            <div>
                                <i class="fa fa-percent"></i>
                                <p>Subscribe to shares</p>
                            </div>
                            <div class="inFormInput">
                                <input type="text" name="email" placeholder="Your email" class="inputAnim usermail" onclick="yaCounter35741200.reachGoal('input_shares_selebrity'); return true;">
                                <button class="send headerSend">
                                    <i class="fa fa-envelope"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <a href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&lang=ru', false); ?>" class="basket">
                        <jdoc:include type="modules" name="my_cart" />
                    </a>
                </div>
            </div>
            <div class="headerMiddleinner">
                <div class="content-container">
                    <?php if ($this->countModules('mainmenu')) : ?>
                        <jdoc:include type="modules" name="mainmenu" />
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="headerDown">
            <div>
                <img src="images/header/headerImageFiz.png" class="girlAnimation">
                <div class="hWrap">
                    <h1>Professional</h1>
                    <h1>cosmetical tools</h1>
                    <h1>skin GI BEAUTY</h1>
                </div>
                <h3>for at home treatments!</h3>
                <h4>You can save</h4>
                <p>appeal • youth • money</p>
                <h4>saving to visit beauty salons and clinics!</h4>
            </div>
        </div>
    </header>
    <!--(готово)----------------------------SECTION-1-------------------------------->
    <section class="section1">
        <div class="animationH2">
            <h2>Line GI BEAUTY - for you!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <div class="sec1WrapDiv">
                <div class="animated sec1Anim1">
                    <div>
                        <img src="images/section1/img1Sec1.png"> 
                    </div>
                    <p>Highest efficiency</p> 
                </div>
                <div class="animated sec1Anim2">
                    <div>
                        <img src="images/section1/img2Sec1.png">
                    </div>
                    <p>The quality is not inferior to branded counterparts</p>
                </div>
                <div class="animated sec1Anim3">
                    <div>
                        <img src="images/section1/img3Sec1.png"> 
                    </div>
                    <p>Acceptable<br>price</p>
                </div>
            </div>
            <a class="btnStyle popUpCall" href="#popUpCall" data-name-form="Take a step to the beauty!" onclick="yaCounter35741200.reachGoal('krasot_buttonclick_selebrity'); return true;">Take a step to the beauty!</a>
        </div>
    </section>
    <!--(готово)----------------------------SECTION-2-------------------------------->
    <section class="section2">
        <div class="animationH2">
            <h2>Line GI BEAUTY - unique!</h2>
            <span></span>
            <span></span>
        </div>
        <div class="sec2WrapDiv">
            <div class="animated sec2Anim1">
                <div>
                    <img src="images/section2/img1Sec2Fiz.png">
                </div>
                <div>
                    <p>Only, which not only meets, but is produced in accordance with the recognized international standards European GMP</p>
                    <p>• in a specialized laboratory<br>• under careful quality control and production parameters</p>
                </div> 
            </div>
            <div class="animated sec2Anim2">
                <div>
                    <img src="images/section2/img2Sec2Fiz.png">
                </div>
                <div>
                    <p>The only one that contains not only the macro and trace elements, vitamins and amino acids, but also innovative components 9</p>
                </div>
            </div>
        </div>
        <div class="animationH2">
            <h2>The choice is wide:</h2>
            <span></span>
            <span></span>
        </div>
    </section>
    <!------------------------------SECTION-3-------------------------------->
    <section class="section3">
        <div class="allproduct">
            <a href="<?= JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=1') ?>">All goods</a>
        </div>
        <div class="sliderSec2-1 sliderSec2First selebrity">
            <a class="slidesSec2-1" href="#">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section3/img1Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    <div class="sec2Bottom">
                        <h5><span>9 999 999</span><i class="fa fa-rub"></i></h5>
                        <div class="buyInOneClick">
                            <i class="fa fa-hand-o-up"></i>
                            <h6>buy<br> in one click</h6>
                        </div>
                        <div class="basketCatalog">
                            <i class="fa fa-shopping-bag"></i>
                        </div>
                    </div>
                </div>
            </a>
            <a class="slidesSec2-1" href="#">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section3/img2Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    <div class="sec2Bottom">
                        <h5><span>9 999 999</span><i class="fa fa-rub"></i></h5>
                        <div class="buyInOneClick">
                            <i class="fa fa-hand-o-up"></i>
                            <h6>buy<br> in one click</h6>
                        </div>
                        <div class="basketCatalog">
                            <i class="fa fa-shopping-bag"></i>
                        </div>
                    </div>
                </div>
            </a>
            <a class="slidesSec2-1" href="#">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section3/img3Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    <div class="sec2Bottom">
                        <h5><span>9 999 999</span><i class="fa fa-rub"></i></h5>
                        <div class="buyInOneClick">
                            <i class="fa fa-hand-o-up"></i>
                            <h6>buy<br> in one click</h6>
                        </div>
                        <div class="basketCatalog">
                            <i class="fa fa-shopping-bag"></i>
                        </div>
                    </div>
                </div>
            </a>
            <a class="slidesSec2-1" href="#">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section3/img4Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    <div class="sec2Bottom">
                        <h5><span>9 999 999</span><i class="fa fa-rub"></i></h5>
                        <div class="buyInOneClick">
                            <i class="fa fa-hand-o-up"></i>
                            <h6>buy<br> in one click</h6>
                        </div>
                        <div class="basketCatalog">
                            <i class="fa fa-shopping-bag"></i>
                        </div>
                    </div>
                </div>
            </a>
            <a class="slidesSec2-1" href="#">
                <div class="slidesSec2ImgWrap">
                    <img src="images/section3/img4Slider1.png">
                </div>
                <div class="nameSec2Wrap">
                    <div>
                        <p>Triple Action Face Mask GI BEAUTY</p>
                    </div>
                </div>
                <div class="btnStyle">
                    <div class="sec2Bottom">
                        <h5><span>9 999 999</span><i class="fa fa-rub"></i></h5>
                        <div class="buyInOneClick">
                            <i class="fa fa-hand-o-up"></i>
                            <h6>buy<br> in one click</h6>
                        </div>
                        <div class="basketCatalog">
                            <i class="fa fa-shopping-bag"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </section>
    <!--------------------------------------------SECTION-4-------------------------------->
    <section class="section4 calc" >
        <form>
            <div>
                <h3>calculator</h3>
                <div class="sec11WrapTop">
                    <div class="checkBtnWrap" >
                        <div class="radioWrap" id="sex">
                            <div class="radioStyle">
                                <input type="radio" name="gender" value="ledy">
                                <div>
                                    <img src="images/section3/lips.png">
                                </div>
                            </div>
                            <div class="radioStyle">
                                <input type="radio" name="gender" value="man">
                                <div>
                                    <img src="images/section3/mustacheFiz.png">
                                </div>
                            </div>
                        </div>
                        <label>What's your gender</label>
                    </div>
                    <div class="borderForm"></div>
                    <div class="checkBtnWrap" id="age">
                        <div class="slider-range"></div>
                        <input type="hidden" name="age" id="ageInner">
                        <label>Your age</label>
                    </div>
                    <div class="borderForm"></div>
                    <div class="checkBtnWrap" id="typemerch">
                        <select>
                            <option>Cream</option>
                            <option>mask</option>
                            <option>means for washing</option>
                            <option>mesopreparations</option>
                        </select>
                        <label>Choose vehicle type</label>
                    </div>
                </div>
                <div class="sec11WrapBottom animated">
                    <div class="borderFormGor"></div>
                    <div class="radioWrapBot" id="typeskin">
                        <div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>Dry</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>Normal</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>Oily</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="condition">
                                <div>
                                    <p>combined</p>
                                </div>
                            </div>

                        </div>
                        <label>Skin condition</label>
                    </div>
                    <div class="borderForm"></div>
                    <div class="radioWrapBot" id="typeaction">
                        <div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>Humidification</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>Purification</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>Skin nutrition</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>comprehensive care</p>
                                </div>
                            </div>
                            <div class="radioStyleBot">
                                <input type="radio" name="kind">
                                <div>
                                    <p>anti-age</p>
                                </div>
                            </div>
                        </div>
                        <label>What would you like to receive?</label>
                    </div>
                    <button class="btnStyle">pick up</button>
                </div>
                <div class="absoluteBottom">
                    <h3>the effect is guaranteed!</h3>
                    <img src="images/section3/arrow.png">
                </div>
            </div>

        </form>
    </section>
    <!--------------------------------------------SECTION-5-------------------------------->
    <section class="section5">
        <div>
            <div class="sliderSec10">
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img1Sec10.png">
                    </div>
                    <p>Regenerating Serum for women 50 ml GI BEAUTY</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img2Sec10.png">
                    </div>
                    <p>A set of masks for the face GI BEAUTY, 8pcs</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img1Sec10.png">
                    </div>
                    <p>Regenerating Serum for women 50 ml GI BEAUTY</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img2Sec10.png">
                    </div>
                    <p>A set of masks for the face GI BEAUTY, 8pcs</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img1Sec10.png">
                    </div>
                    <p>Regenerating Serum for women 50 ml GI BEAUTY</p>
                </div>
                <div class="slidesSec10">
                    <div>
                        <img src="images/section4/img2Sec10.png">
                    </div>
                    <p>A set of masks for the face GI BEAUTY, 8pcs</p>
                </div>
            </div>
            <div class="afterCos">
                <div>
                    <?
                    $path_male = $_SERVER['DOCUMENT_ROOT'] . '/images/faces/male/';
                    $path_female = $_SERVER['DOCUMENT_ROOT'] . '/images/faces/female/';
                    $photo_faces = array();
                    if (file_exists($path_female)) {
                        $photo_female = scandir($path_female);
                        foreach ($photo_female as $value) {
                            if ($value != '.' and $value != '..')
                                $photo_faces[] = array('data' => 'female', 'path' => 'images/faces/female/' . $value);
                        }
                    }

                    if (file_exists($path_male)) {
                        $photo_male = scandir($path_male);
                        foreach ($photo_male as $value) {
                            if ($value != '.' and $value != '..')
                                $photo_faces[] = array('data' => 'male', 'path' => 'images/faces/male/' . $value);
                        }
                    }
                    //первая картинка
                    $img = array_shift($photo_faces);
                    print '<img id="girl" data-face="' . $img['data'] . '" src="' . $img['path'] . '">';
                    unset($photo_faces[0]);
                    ?>
                    <div class="hide-foto" style="display: none" id="calcfoto">
                        <?
                        foreach ($photo_faces as $img) {
                            print '<img data-face="' . $img['data'] . '" src="' . $img['path'] . '">';
                        }
                        ?>
                    </div>
                </div>
                <div class="sec10H6Wrap">
                    <div>
                        <h6>after peeling</h6>
                    </div>
                    <div>
                        <h6>8 hours after the application of funds GI BEAUTY</h6>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--(готово)----------------------------SECTION-6-------------------------------->
    <section class="section6">
        <div class="animationH2">
            <h2>Technologies of the future - today!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <h3>Cosmetics GI BEAUTY - is</h3>
            <div class="sec6WrapDiv">
                <div class="animated sec6Anim1-1">
                    <div class="sec6ImgWrap">
                        <img src="images/section6/img1Sec6Fiz.png">
                    </div>
                    <div class="sec6TextWrap">
                        <div>
                            <div>
                                <p>The latest development of Russian scientists - company «НПО ЛИОМАТРИКС», taking into account the peculiarities of women's and men's skin</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="animated sec6Anim1-2">
                    <div class="sec6ImgWrap">
                        <img src="images/section6/img2Sec6Fiz.png">
                    </div>
                    <div class="sec6TextWrap">
                        <div>
                            <div>
                                <p>Perfectly balanced, qualitative composition, to effectively act on the skin immediately in three directions:</p>
                                <ul>
                                    <li>
                                        <i class="fa fa-plus-circle"></i>
                                        <p>Epidermis- hydration and nutrition,</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-plus-circle"></i>
                                        <p>Derma - anti-age effect (rejuvenation)</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-plus-circle"></i>
                                        <p>The immune system of the skin - increased protective properties and anti-inflammatory effect</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec6WrapDiv">
                <div class="animated sec6Anim2-1">
                    <div class="sec6ImgWrap">
                        <img src="images/section6/img3Sec6Fiz.png">
                    </div>
                    <div class="sec6TextWrap">
                        <div>
                            <div>
                                <p>Four times more rapid penetration into the skin</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="animated sec6Anim2-2">
                    <div class="sec6ImgWrap">
                        <img src="images/section6/img4Sec6Fiz.png">
                    </div>
                    <div class="sec6TextWrap">
                        <div>
                            <div>
                                <p>The absence of a greasy, free ports, allows the skin to "breathe"</p> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sec6WrapDiv">
                <div class="animated sec6Anim3-1">
                    <div class="sec6ImgWrap">
                        <img src="images/section6/img5Sec6Fiz.png">
                    </div>
                    <div class="sec6TextWrap">
                        <div>
                            <div>
                                <p>Innovative packaging that protects against bacterial contamination</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="animated sec6Anim3-2">
                    <div class="sec6TextOtherWrap">
                        <h5>Exactly<br>so</h5>
                        <h6>GI BEAUTY</h6>
                    </div>
                    <div class="sec6TextWrap">
                        <div>
                            <div>
                                <p>Unlike conventional means, does not allow a temporary and permanent results</p>
                                <ul>
                                    <li>
                                        <i class="fa fa-plus-circle"></i>
                                        <p>Eliminate the causes of skin imperfections</p>
                                    </li>
                                    <li>
                                        <i class="fa fa-plus-circle"></i>
                                        <p>Provide her right balanced care</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--(готово)----------------------------SECTION-7-------------------------------->
    <section class="section7">
        <div class="animationH2">
            <h2>Russian patent confirms the novelty and uniqueness!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <div class="sliderSec9">
                <jdoc:include type="modules" name="patents"/>
            </div>
        </div>
    </section>
    <!--(готово)----------------------------SECTION-8-------------------------------->
    <section class="section8">
        <div class="animationH2">
            <h2>Learn more about GI BEAUTY!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <div>
                <jdoc:include type="modules" name="video_1"/>
            </div>
        </div>
    </section>
    <!--(+-)----------------------------SECTION-9-------------------------------->
    <section class="section9">
        <div>
            <jdoc:include type="modules" name="secret"/>
            <form method="POST" data-target="consult_form_selebrity">
                <p class="forNameForm">Visit the video tutorial</p>
                <div class="sec9InputWrap">
                    <div class="inputStyle">
                        <input name="name" type="text">
                        <span></span>
                        <label>Notify Your name*</label>
                    </div>
                    <div class="inputStyle">
                        <input name="phone" type="text" class="userphone">
                        <span></span>
                        <label>Your phone number*</label>
                    </div> 
                </div>
                <button class="btnStyle send">Book a free consultation call and!</button>
            </form>
        </div>
    </section>
    <!------------------------------SECTION-10-------------------------------->
    <section class="section10">
        <div class="animationH2">
            <h2>GI BEAUTY appreciate all!</h2>
            <span></span>
            <span></span>
        </div>
        <div>
            <jdoc:include type="modules" name="fl_sliders"/>
        </div>
    </section>
    <!------------------------------SECTION-11-------------------------------->
    <section class="section11">
        <div>
            <h3>and, of course, we all!</h3>
            <h3>Technologies of the future - today our!</h3>
            <?php
            $query->select('params')
                    ->from('#__extensions')
                    ->where('extension_id = 10066');
            $socials = $db->setQuery($query)->loadResult();
            $query->clear();
            $socials = json_decode($socials);
            ?>
            <div class="linksWrap">
                <a href="<?= $socials->fb ?>">
                    <i class="fa fa-facebook"></i>
                </a>
                <a href="<?= $socials->inst ?>">
                    <i class="fa fa-instagram"></i>
                </a>
                <a href="<?= $socials->tw ?>">
                    <i class="fa fa-twitter"></i>
                </a>
                <a href="<?= $socials->vk ?>">
                    <i class="fa fa-vk"></i>
                </a>
                <a href="<?= $socials->ok ?>">
                    <i class="fa fa-odnoklassniki"></i>
                </a>
                <a href="<?= $socials->pi ?>">
                    <i class="fa fa-pinterest-p"></i>
                </a>
                <a href="<?= $socials->in ?>">
                    <i class="fa fa-linkedin"></i>
                </a>
            </div>
            <a class="btnStyle popUpCall" href="#popUpCall" data-name-form="Order premium funds!" onclick="yaCounter35741200.reachGoal('zakaz_buttonclick_selebrity'); return true;">Order premium funds!</a>
            <h3>Striking in its beauty!</h3>
        </div>
    </section>
    <!------------------------------SECTION-14-------------------------------->
    <section class="section14">
        <div class="animationH2">
            <h2>Contact Information:</h2>
            <span></span>
            <span></span>
        </div>
        <div class="top-contacts-belt">
            <div>
                <i class="fa fa-map-marker"></i>
                <?
                $document = & JFactory::getDocument();
                $renderer = $document->loadRenderer('modules');
                $options = array();
                $position = 'bottom_adress';
                echo $renderer->render($position, $options, null);
                ?>
            </div>
            <div>
                <i class="fa fa-phone"></i>
                <?
                $position = 'bottom_phone';
                echo $renderer->render($position, $options, null);
                ?>
            </div>
            <div>
                <i class="fa fa-envelope mailI"></i>
                <a href="mailto:<?= JFactory::getMailer()->From; ?>" onclick="yaCounter35741200.reachGoal('mail_selebrity'); return true;"><?= JFactory::getMailer()->From; ?></a>
            </div>
        </div>
        <div id="mapWrap"></div>
    </section>
    <!----------------------------------SECTION15-------------------------------------------->

    <footer>
        <div class="footerUp">
            <div>
                <a href="./">
                    <img src="images/logo/logoFooter.png">
                </a>
                <jdoc:include type="modules" name="footer_menu" />
            </div>
        </div>
        <div class="footerDown">
            <div>
                <div>
                    <span>©</span>
                    <p>2015 ООО «НПО Лиоматрикс»</p>
                </div>
                <div>
                    <a href="http://tutmee.ru/">
                        <!--<p>Создание дизайна<br>и разработка сайтов<br>LTD Tutmee.ru</p>-->
                        <img src="images/logo/tutmee.png">
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <div class="popupoverlay"></div>
    <div class="autentification-popup" id="popupreg">        <!-- если в URL есть якорь #popupreg отлкрывать форму при загрузке страницы-->   
        <ul class="autentification-tab">
            <li><span class="fa fa-pencil"></span>check in</li>
            <li class="active"><span class="fa fa-key"></span>login</li>                           
        </ul>
        <table>
            <tr>
                <td>
                    <ul class="autentification-tab-item">
                        <li>
                            <!--                        <div class="error-form-place">
                                                        Ошибка формы !!!!
                                                    </div>-->
                            <form class="ajaxStop" action="<?php echo JRoute::_('index.php?option=com_users&task=gireg.register'); ?>" method="post">
                                <!--                                    <ul class="register-type">
                                                                        <li class="active">для физических лиц <div class="hide-radio"><input type="radio"  name="rtype"/></div></li>
                                                                        <li>для юридических лиц <div class="hide-radio"><input type="radio"  name="rtype"/></li>
                                                                    </ul>-->
                                <ul class="register-item">
                                    <li class="active">
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[name]" /><label>Your Name (required)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password1]" /><label>Password (required)</label></div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[email1]" /><label>Your email address (required)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password2]" /><label>Confirm Password (required)</label></div>
                                        </fieldset>
                                        <div class="capcha-check-container">
                                            <fieldset>
                                                <div class="capcha-block">
                                                    <div class="g-recaptcha" data-sitekey="6LdqdRgTAAAAAPYF1ypIO5UDbI1SaSqlNsvrPK5p"></div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="check-container">
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"> </label></div>
                                                        <span>Receive news by email service</span>
                                                    </div>
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"></label></div>
                                                        <span>I have read and agree to the terms and conditions <a href="#">Privacy policy</a></span>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <input type="hidden" name="option" value="com_users" />
                                        <input type="hidden" name="task" value="gireg.register" />
                                        <?php echo JHtml::_('form.token'); ?>
                                        <button>sign up</button>
                                    </li>
                                    <!--                                        <li>
                                    
                                                                                <fieldset>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Название организации или ИП (обязательно)</label></div>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Email (обязательно) </label></div>
                                                                                </fieldset>
                                                                                <fieldset>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Контактное лицо</label></div>
                                                                                    <div class="form-item  " data-hint=""><input type="text" /><label>Номер телефона (обязательно)</label></div>
                                                                                </fieldset>
                                                                                <div class="password-block">
                                                                                    <fieldset><div class="form-item" data-hint=""><input type="password" /><label>Пароль(обязательно)</label></div></fieldset>
                                                                                    <fieldset><div class="form-item" data-hint=""><input type="password" /><label>Подтверждение пароля(обязательно)</label></div></fieldset>
                                                                                </div>
                                                                                <div class="capcha-check-container">
                                                                                    <fieldset>
                                                                                        <div class="capcha-block"><img src="images/capha.png" alt="" /></div>
                                                                                    </fieldset>
                                                                                    <fieldset>
                                                                                        <div class="check-container">
                                                                                            <div class="check-block">
                                                                                                <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"> </label></div>
                                                                                                <span>Получать на email новости сервиса</span>
                                                                                            </div>
                                                                                            <div class="check-block">
                                                                                                <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"></label></div>
                                                                                                <span>Я ознакомлен и согласен с условиями <a href="#">политики конфиденциальности</a></span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </fieldset>
                                                                                </div>
                                                                                <button>зарегистрироваться</button>
                                    
                                                                            </li>-->
                                </ul>
                            </form>
                        </li>
                        <li class="active">
                        <jdoc:include type="modules" name="login" />
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        <div class="close-popup"></div>               
    </div>
    <div class="commonpopup">
        <div class="close-popup" ></div>
        <div id="by_inclick">
            <form action="#" data-target="byin_form_selebrity">
                <input type="hidden" class="forNameForm">
                <input type="hidden" name="prodname" id="prodname">
                <input type="hidden" name="totalprice" id="total-pr">
                <input type="hidden" name="priceforone" id="price-forone">
                <div class="inclick-top-belt">
                    <h2 class="forNameForm">Buy 1 click</h2>
                    <div class="in-click-inner">                            
                        <table>
                            <tr>
                                <td rowspan="4"><a href="#" class="img-cont"><img src="images/merch.png" alt="" /></a></td>
                                <td>Name</td>
                                <td><a href="#" class="nameProductLink">Triple Action Face Mask GI BEAUTY</a></td>
                            </tr>
                            <tr>
                                <td>Price</td>
                                <td><i class="price-forone">100 000</i> <span class="fa fa-rub"></span></td>
                            </tr>
                            <tr>
                                <td>Qty</td>
                                <td>
                                    <div class="merchendise-count">
                                        <span class="minus inclickSpan">-</span>
                                        <input type="text" class="number" name="quantity" value="1" readonly="readonly"/>
                                        <span class="plus inclickSpan">+</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Cost of</td>
                                <td><i class="total-pr">100 000</i> <span class="fa fa-rub"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="inclick-bottom-belt">
                    <div class="inputStyle">
                        <input type="text" name="name"/>
                        <span></span>
                        <label>Your name</label>
                    </div>
                    <div class="inputStyle">
                        <input type="text" name="phone" class="userphone"/>
                        <span></span>
                        <label>Your phone number</label>
                    </div>
                    <button>send an order <span class="fa fa-arrow-right"></span></button>
                </div>
            </form>
        </div>
    </div>
    <div id="popUpCall">
        <form  method="POST" data-target="cell_form_selebrity">
            <h2 class="nameInner">Request a call</h2>
            <p class="forNameForm nameInner">Request a call</p>
            <div>
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Your name</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone">
                    <span></span>
                    <label>Your phone number</label>
                </div>
            </div>
            <button class="btnStyle send">Send</button>
            <div class="closeFancy">
                <span></span>
                <span></span>
            </div>
        </form>
    </div>
    <div id="imagePopUp">
        <img src="">
    </div>
    <div class="commonpopup" id="thanksPopUp">
        <div class="close-popup"></div>
        <div id="alert">
            <table>
                <tr>
                    <td>
                        <h2>Thank you for your application</h2>
                        <h3>all messages will be sent to the address <a href="mailto:<?= JFactory::getMailer()->From; ?>"  onclick="yaCounter35741200.reachGoal('mail_selebrity'); return true;"><?= JFactory::getMailer()->From; ?></a></h3>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="commonpopup">
        <div class="addedInBasket" id="addedInBasket">
            <div>
                <div>
                    <h2><span class="numberAddInBasket"></span> x <span class="pname"></span> added <a href="<?= JRoute::_("/index.php?option=com_virtuemart&view=cart") ?>">your shopping cart</a></h2>
                </div>            
            </div>
            <div class="addInBasketWrap">
                <button class="btnStyle addedInBasketStyle closePopUpAdded">
                    <i class="fa fa-arrow-left"></i>continue shopping
                </button>
                <a class="btnStyle addedInBasketStyle" href="<?= JRoute::_("/index.php?option=com_virtuemart&view=cart") ?>">go to shopping cart<i class="fa fa-arrow-right"></i></a>
            </div>
            <div class="close-popup"></div>
        </div>
    </div>
    <script src="https://www.youtube.com/iframe_api"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/script.js"></script> 
</body>
</html>
