jQuery(function ($) {
    $.validator.addMethod(
            'regexp',
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );

    $.validator.addClassRules({
        userphone: {
            required: true,
            minlength: 15,
            regexp: '[^_]+$'
        },
        usermail: {
            email: true
        }
    });
    $(window).load(function () {
        $('.calc form button').click(function () {
            calcMainPage.monitoring();
            return false;
        })
        function loadpage() {
            setTimeout(function () {
                var windowWidth = document.documentElement.clientWidth;
                if (windowWidth >= 1560) {
                    // $('.sliderSec3').slick({
                    //     slidesToShow:4,
                    //     slidesToScroll:2,
                    //     infinite:true,
                    //     autoplay:true,
                    //     autoplaySpeed:5000
                    // });
//                    $('.sliderSec2-1').slick({
//                        slidesToShow: 4,
//                        slidesToScroll: 2,
//                        infinite: true,
//                        arrows: true
//                    })
                    $('.sliderSec9').slick({
                        slidesToShow: 4,
                        slidesToScroll: 2,
                        infinite: true,
                        arrows: true
                    })
                }
                if (windowWidth < 1560) {
                    // $('.sliderSec3').slick({
                    //     slidesToShow:3,
                    //     slidesToScroll:2,
                    //     infinite:true,
                    //     autoplay:true,
                    //     autoplaySpeed:5000
                    // });
//                    $('.sliderSec2-1').slick({
//                        slidesToShow: 3,
//                        slidesToScroll: 2,
//                        infinite: true,
//                        arrows: true
//                    })
                    $('.sliderSec9').slick({
                        slidesToShow: 3,
                        slidesToScroll: 2,
                        infinite: true,
                        arrows: true
                    })
                }
                if (windowWidth >= 1460) {

//                    $('.sliderSec10').slick({
//                        slidesToShow: 2,
//                        slidesToScroll: 2,
//                        infinite: true,
//                        arrows: true
//                    })
                }
                if (windowWidth < 1460) {
//                    $('.sliderSec10').slick({
//                        slidesToShow: 1,
//                        slidesToScroll: 1,
//                        infinite: true,
//                        arrows: true
//                    })
                }
                $('#preloader').addClass("animated fadeOut")
                setTimeout(function () {
                    $('#preloader').css("display", "none");
                    $('.girlAnimation').css('opacity', '1')
                    $('#fixedBtn').addClass('bounceInRight').css('opacity', '1');
                    $('.headerFormWrap span').eq(0).css('width', '11px');
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(1).css('height', '100%');
                    }, 100)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(2).css('width', '100%');
                    }, 400)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(3).css('height', '100%');
                    }, 1300)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(4).css('width', '277px');
                    }, 1600)
                    setTimeout(function () {
                        $('#fixedBtn').removeClass('bounceInRight')
                    }, 900)
                }, 500)
            }, 300)
        }
        loadpage();
        $(document).on('click', '.buyInOneClick', function () {
            yaCounter35741200.reachGoal('byin_buttonclick_cosmetologi');
            return true;
        });
    })
    /*--------------------------btn-animation------------------------------*/
    $('.menuTable').mouseover(function () {
        $('.fa-bars').addClass('jello');
        setTimeout(function () {
            $('.fa-bars').removeClass('jello');
        }, 700)
    })
    $('.basket').mouseover(function () {
        $('.fa-shopping-bag').addClass('jello');
        setTimeout(function () {
            $('.fa-shopping-bag').removeClass('jello');
        }, 700)
    })
    $('#fixedBtn').click(function () {
        $('#fixedBtn').addClass('rubberBand');
        setTimeout(function () {
            $('#fixedBtn').removeClass('rubberBand');
        }, 700)
    })
    /*--------------------------INPUT ANIMATION-----------------------------*/
    $('.headerFormWrap input').hover(function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    }, function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    })
    $('.slider-range').slider({
        range: "min",
        min: 18,
        max: 80,
        value: 35,
        slide: function (event, ui) {
            $("#ageInput").val(ui.value + " года");
            $(".ui-slider-range").attr('data-attribute', ui.value);
            $(this).closest('.checkBtnWrap>div').addClass('fixUiSlider');
        }
    })
    $("#ageInput").val($("#slider-range").slider("value") + ' года');
    $(".ui-slider-range").attr('data-attribute', '35');


    $('input[name="gender"]').click(function () {
        $('.sec11WrapBottom').addClass('bounceIn').css({'display': 'block', 'opacity': '1'})
    });

    var afterChange;
    $('.slider1Sec10,.slider2Sec10').slick({
        centerMode: true,
        centerPadding: '60px',
        slidesToShow: 3,
        dots: false,
        infinite: false,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });
    $('.slick-center').addClass('slick-active').find('h4').css('opacity', '1');
    // $('.slider1Sec10,.slider2Sec10').on('beforeChange', function(event, slick, currentSlide, nextSlide){
    //     $('.slick-center').next().css('z-index','69') 
    //     $('.slick-center').prev().css('z-index','68')
    // });
    $('.slider1Sec10,.slider2Sec10').on('afterChange', function (event, slick, currentSlide, nextSlide) {
        $('.slick-center').addClass('slick-active').find('h4').css('opacity', '1');
        $('.slick-center').next().find('h4').css('opacity', '0');
        $('.slick-center').prev().find('h4').css('opacity', '0');
    });
    (function () {
        var API = google.maps;
        var $container = $('#mapWrap');
        if ($container.length == 0)
            return;
        var mapPosition = new API.LatLng(55.776597, 37.542144);
        var markerPosition = new API.LatLng(55.776597, 37.542144);
        var map = new API.Map($container[0], {
            center: mapPosition,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true,
            panControl: false,
            zoom: 14
        });
        var marker = new API.Marker({
            map: map, position: markerPosition, icon: 'images/marker.png'
        });
        var iw = new google.maps.InfoWindow({
            content: '<div style="text-align: left; height: 45px; font-size: 18px; position:relative; z-index:1;">г.Реутов<br>ул.Безымянная дом 0</div>'
        });
    })();
    var flag4 = true;
    $(window).scroll(function () {
        var heightWindow = document.documentElement.clientHeight / 2.5;
        if ($(this).scrollTop() >= $('.section1').offset().top - heightWindow) {
            $('.section1>div:nth-of-type(1) span').addClass('animatedSpan');
            $('.sec1Anim1').addClass('flipInX').css('opacity', '1')
            setTimeout(function () {
                $('.sec1Anim2').addClass('flipInX').css('opacity', '1')
            }, 300)
            setTimeout(function () {
                $('.sec1Anim3').addClass('flipInX').css('opacity', '1')
            }, 600)
        }
        if ($(this).scrollTop() >= $('.section2').offset().top - heightWindow) {
            $('.section2>div:nth-of-type(1) span').addClass('animatedSpan');
            $('.sec2Anim1').addClass('fadeInLeft').css('opacity', '1')
            $('.sec2Anim2').addClass('fadeInRight').css('opacity', '1')
        }
        if ($(this).scrollTop() >= $('.section3').offset().top - heightWindow) {
            $('.section2>div:nth-of-type(3) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section6').offset().top - heightWindow) {
            $('.section6>div:nth-of-type(1) span').addClass('animatedSpan');
            $('.sec6Anim1-1').addClass('fadeInLeft').css('opacity', '1');
            $('.sec6Anim1-2').addClass('fadeInRight').css('opacity', '1');
            setTimeout(function () {
                $('.sec6Anim2-1').addClass('fadeInLeft').css('opacity', '1');
                $('.sec6Anim2-2').addClass('fadeInRight').css('opacity', '1');
            }, 400)
            setTimeout(function () {
                $('.sec6Anim3-1').addClass('fadeInLeft').css('opacity', '1');
                $('.sec6Anim3-2').addClass('fadeInRight').css('opacity', '1');
            }, 800)

        }
        if ($(this).scrollTop() >= $('.section7').offset().top - heightWindow) {
            $('.section7>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section8').offset().top - heightWindow) {
            $('.section8>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section10').offset().top - heightWindow) {
            $('.section10>div:nth-of-type(1) span').addClass('animatedSpan');
        }
        if ($(this).scrollTop() >= $('.section14').offset().top - heightWindow) {
            $('.section14>div:nth-of-type(1) span').addClass('animatedSpan');
        }

        if ($(this).scrollTop() > $('.section1').offset().top - 100) {
            if (flag4 == true) {
                $('.headerDown').css('padding-top', '153px');
                $('.headerUp').addClass('headerUpFixed fadeInDown');
                $('.headerMiddle').addClass('headerMiddleFixed fadeInDown');
                flag4 = false;
            }
        }
        if ($(this).scrollTop() < $('.section1').offset().top - 100) {
            if (flag4 == false) {
                $('.headerMiddle').removeClass('fadeInDown').addClass('fadeOutUp');
                $('.headerUp').removeClass('fadeInDown').addClass('fadeOutUp');
                setTimeout(function () {
                    $('.headerDown').css('padding-top', '0')
                    $('.headerUp').removeClass('headerUpFixed fadeOutUp').css('opacity', '1');
                    $('.headerMiddle').removeClass('headerMiddleFixed fadeOutUp').css('opacity', '1');
                }, 600)
                flag4 = true;
            }
        }
    });
    $('.menuTable').click(function (e) {
        e.preventDefault();
        $('.headerMiddleinner').slideToggle(600);
    });
    $('section').click(function () {
        $('.headerMiddleinner').slideUp(600);
    })
    $('.popUpCall').click(function () {
        $('#popUpCall').find('.nameInner').text($(this).attr('data-name-form'));
    })
    $('.popUpCall').fancybox({
        'showCloseButton': false,
        'closeBtn': false,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('.zoomSertificate').fancybox({
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('.closeFancy').click(function () {
        $.fancybox.close();
    })
    jQuery(function ($) {
        $('input[name="phone"]').mask("+7(999)999-99-99", {placeholder: '_'});
    });
    $(document).ready(function () {
        var formSend;
        formSend = function (formObject, namePopUp, productName, target) {
            var fieldForm, sendAjaxAndResetForm, fieldArr, newFieldArr = [], c, nameObg = {}, taskMail = 'sendMail';
            fieldArr = formObject.find(':input,textarea,select').serializeArray();
            for (c = 0; c < fieldArr.length; c++) {
                if (fieldArr[c].value.length > 0) {
                    newFieldArr.push(fieldArr[c])
                }
            }
            if (productName) {
                nameObg.name = "product_name";
                nameObg.value = productName;
                newFieldArr.push(nameObg);
            }
            if ($(formObject).hasClass('subscribeForm')) {
                taskMail = 'sendMailShares';
            }
            $.ajax({
                type: "POST",
                url: 'index.php?option=com_ajax&plugin=ajax&format=json',
                dataType: 'json',
                data: {
                    subject: namePopUp,
                    formData: newFieldArr,
                    task: taskMail
                }
            }).done(function () {
                $.fancybox.close();
                $('.close-popup,.popupoverlay').click();
                $('.merchendise-count').find('input').attr('value', '1')
                $('#thanksPopUp').css('display', 'block')
                setTimeout(function () {
                    $('#thanksPopUp').css('opacity', '1')
                    $('form').trigger('reset');
                }, 300);
                setTimeout(function () {
                    $('#thanksPopUp').css('opacity', '0')
                }, 1800)
                setTimeout(function () {
                    $('#thanksPopUp').css('display', 'none')
                }, 2100);
                yaCounter35741200.reachGoal(target);
                return true;
            })
        }
        $('form:not(.ajaxStop)').each(function () {
            $(this).validate({
                errorPlacement: function (error, element) {
                    $('.error').next().addClass('errorVal');
                    $('.error').closest('.headerFormWrap').find('.formSpan').addClass('errorValHeader')
                    error.remove();
                },
                success: function () {
                    $('.valid').next().removeClass('errorVal');
                    $('.valid').closest('.headerFormWrap').find('.formSpan').removeClass('errorValHeader')
                },
                submitHandler: function (form) {

                    var object = $(form);
                    var namePopUp = $(object).find('.forNameForm').text();
                    var productName = $(object).find('.nameProductLink').text();
                    var target = object.attr('data-target');
                    formSend(object, namePopUp, productName, target);
                }

            });
        });
        $('.autentification-tab>li').click(function () {
            var index = $(this).index();
            $('.autentification-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.autentification-tab-item>li').removeClass('active').eq(index).addClass('active');
        })
        $('.register-type>li').click(function () {
            var index = $(this).index();
            $('.register-type>li').removeClass('active');
            $(this).addClass('active');
            $('.register-item>li').removeClass('active').eq(index).addClass('active');
        })

        $('.comment-button').click(function () {
            $(this).closest('.vk-comment').find('.comment-body').slideToggle(600);
        });
        $('.order-item .item-heading').click(function () {
            $('.order-item').find('.item-body').not($(this).closest('.order-item').find('.item-body')).slideUp(600);
            $(this).closest('.order-item').find('.item-body').slideToggle(600);
            $('.openorder').not($(this).find('.openorder')).addClass('fa-chevron-down').removeClass('fa-chevron-up')
            $(this).find('.openorder').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down')
        });

        $('.write-button').click(function () {
            $(this).closest('.form-item').find('input').removeAttr('readonly');
        });
        $('.lk-tab li').click(function () {
            var index = $(this).index();
            $('.lk-tab li').removeClass('active');
            $(this).addClass('active');
            $('.lk-tab-body li').removeClass('active').eq(index).addClass('active');
        });
        $('.prod-main-tab>li').click(function () {
            var index = $(this).index();
            $('.prod-main-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.prod-main-tab-body>li').removeClass('active').eq(index).addClass('active');
        });
        $('.prod-inner-tab>li').click(function () {
            var index = $(this).index();
            $('.prod-inner-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.prod-inner-tab-body>li').removeClass('active').eq(index).addClass('active');
        });


        function openCommonPopup(id) {
            $('.popupoverlay').fadeIn(400);
            $('#' + id).closest('.commonpopup').fadeIn(400);
        }
        ;

        $('.openpopupreg').click(function () {
            $('.popupoverlay,.autentification-popup').fadeIn(400);
        });

        $('.opencommonpopup').click(function () {
            var id = $(this).attr('data-id');
            openCommonPopup(id);
        });

        function openCommonPopup(id, element) {
            $('.popupoverlay').fadeIn(400);
            if (id == 'by_inclick') {
                var proid = $(element).closest('.byin').attr('data-prod-id');
                for (var i = 0; i < prodArray.length; i++) {
                    if (prodArray[i].virtuemart_product_id == proid) {
                        $('#by_inclick table tr:nth-child(1) td:nth-child(3),forNameForm').text(prodArray[i].product_name);
                        $('#prodname').val(prodArray[i].product_name);
                        $('#by_inclick table td .img-cont').find('img').attr('src', prodArray[i].images[0].file_url);
                        $('#by_inclick table tr:nth-child(2) td:nth-child(2) .price-forone,#by_inclick table tr:nth-child(4) td:nth-child(2) .total-pr').text(prodArray[i].product_price);
                        $('#by_inclick #price-forone,#by_inclick #total-pr').val(prodArray[i].product_price);
                        break;
                    }
                }
                $('#' + id).closest('.commonpopup').fadeIn(400);
            } else {
                $('#' + id).closest('.commonpopup').fadeIn(400);
            }

        }

        $('.openpopupreg').click(function () {
            $('.popupoverlay,.autentification-popup').fadeIn(400);
        });

        $(document).on('click', '.opencommonpopup', function () {
            var id = $(this).attr('data-id');
            var element = $(this);
            openCommonPopup(id, element);
        });

        $('.close-popup,.popupoverlay,.closePopUpAdded').click(function () {
            $('.popupoverlay,.commonpopup,.autentification-popup').fadeOut(400);
            $('.merchendise-count').find('input').attr('value', '1')
        })
        $('.zoomSertificate').click(function () {
            var srcPath = ($(this).attr('data-path'))
            $('#imagePopUp').find('img').attr('src', srcPath);
        });
        $('.merchendise-count span').click(function () {
            var number = $(this).parent('div').find('input').attr('value');
            if ($(this).hasClass('plus')) {
                number++;
                $(this).parent('div').find('input').attr('value', number).text(number)
                if (number >= 999) {
                    number = 999;
                    $(this).parent('div').find('input').attr('value', number).text(number)
                }
                ;
            }
            if ($(this).hasClass('minus')) {
                number--;
                $(this).parent('div').find('input').attr('value', number).text(number)
                if (number <= 1) {
                    number = 1;
                    $(this).parent('div').find('input').attr('value', number).text(number)
                }
            }
        });
        var inClickVal
        $('.inclickSpan').click(function () {
            inClickVal = Number($(this).closest('.merchendise-count').find('input').val());
            $('.total-pr').text(inClickVal * Number($('.price-forone').text()))
        })
    });

})



