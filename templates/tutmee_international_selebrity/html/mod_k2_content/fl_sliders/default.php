<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
if (count($items)):
    foreach ($items as $key => $item):
        ?>
        <h3><?= $item->introtext ?></h3>
        <div class="slider1Sec10">
            <?
            foreach ($item->attachments as $attachments):
                ?>
                <div class="slides1Sec10">
                    <div>
                        <img src="<?= $attachments->link ?>">
                    </div>
                    <h4><?= $attachments->title?></h4>
                </div>
                <?
            endforeach;
            ?></div><?
    endforeach;
endif;
?>