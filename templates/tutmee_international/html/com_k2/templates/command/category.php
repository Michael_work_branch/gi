<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
$i = 0;
?>
<div class="team-box">
    <?php
    if (isset($this->leading) && count($this->leading)):
        foreach ($this->leading as $key => $item):
            $i++;
            if ($i == 1):
                ?><div class="team-belt"><div class="inlineWrapper"><?
            endif;
            ?>
            
                <div class = "team-item">
                    <div class = "item-content">
                        <div class = "fio">
                            <? $fio = explode(' ', $item->title) ?>
                            <p><?= $fio[0] ?></p>
                            <p><?= $fio[1] ?></p>
                            <p><?= $fio[2] ?></p>
                        </div>
                        <div class = "position">
                            <p><?= $item->extraFields->Dolzhnosty->value ?></p>
                        </div>
                        <div class="contactdata">
                            <p class="fa fa-mobile-phone"><?= $item->extraFields->Telefon->value ?></p>
                            <a  class="fa fa-envelope-o" href="#"><?= $item->extraFields->Pochtovyyyashtik->value ?></a>
                        </div>
                    </div>

                    <div class="img-cont"><img src="media/k2/items/src/<?= md5('Image'.$item->id) ?>.jpg" alt="" /></div>
                    <div class="clear"></div> 
                </div>

                <?
                if ($i == 2 or !isset($this->leading[$key+1])):
                    $i = 0;
                    ?>
                    </div></div><?
            endif;

        endforeach;
    endif;
    ?>
</div>