<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
?>
<div class="matherial-box">                
    <div class="content-container">
        <img src="media/k2/items/src/<?= md5('Image'.$this->item->id) ?>.jpg" alt="" />
        <h2><?php echo $this->item->title; ?></h2>
        <?php echo $this->item->introtext; ?>
    </div>
</div>