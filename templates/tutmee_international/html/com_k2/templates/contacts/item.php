<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;

?>

<div class="contacts-box">
    <div class="section14">                    
        <div class="top-contacts-belt">
            <div>
                <i class="fa fa-map-marker"></i>
                <?
                $document = & JFactory::getDocument();
                $renderer = $document->loadRenderer('modules');
                $options = array();
                $position = 'bottom_adress';
                echo $renderer->render($position, $options, null);
                ?>
            </div>
            <div>
                <i class="fa fa-phone"></i>
                <?
                $position = 'bottom_phone';
                echo $renderer->render($position, $options, null);
                ?>
            </div>
            <div>
                <i class="fa fa-envelope mailI"></i>
                <a href="mailto:<?= JFactory::getMailer()->From; ?>"><?= JFactory::getMailer()->From; ?></a>
            </div>
        </div>
        <div id="mapWrap"></div>
    </div>
    <div class="feadback-form-container">
        <h2>Feedback</h2>
        <div class="content-container">
            <form action="" data-target="feedback">
                <fieldset>
                    <div class="form-item"><input type="text" name="name"/><label>Your name</label></div>
                </fieldset>
                <fieldset>
                    <div class="form-item"><input type="text" class="userphone" name="phone"/><label>Your phone number</label></div>
                </fieldset>
                <div class="form-item qestion"><input type="text" name="question"/><label>Ask a question*</label></div>
                <button>Send</button>
            </form>
        </div>
    </div>
</div>