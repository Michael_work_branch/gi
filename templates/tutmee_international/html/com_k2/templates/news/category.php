<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
$db = JFactory::getDbo();
$query = $db->getQuery(true);
/*?>
<!--
<?php if ((isset($this->leading) || isset($this->primary) || isset($this->secondary) || isset($this->links)) && (count($this->leading) || count($this->primary) || count($this->secondary) || count($this->links))): ?>
    <!-- Item list -->
    <div class="itemList">
        <?php if (isset($this->primary) && count($this->primary)): ?>
            <!-- Primary items -->
            <div id="itemListPrimary">
                <?php foreach ($this->primary as $key => $item): ?>

                    <?php
                    // Define a CSS class for the last container on each row
                    if ((($key + 1) % ($this->params->get('num_primary_columns')) == 0) || count($this->primary) < $this->params->get('num_primary_columns'))
                        $lastContainer = ' itemContainerLast';
                    else
                        $lastContainer = '';
                    ?>

                    <div class="itemContainer<?php echo $lastContainer; ?>"<?php echo (count($this->primary) == 1) ? '' : ' style="width:' . number_format(100 / $this->params->get('num_primary_columns'), 1) . '%;"'; ?>>
                        <?php
                        // Load category_item.php by default
                        $this->item = $item;
                        echo $this->loadTemplate('item');
                        ?>
                    </div>
                    <?php if (($key + 1) % ($this->params->get('num_primary_columns')) == 0): ?>
                        <div class="clr"></div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="clr"></div>
            </div>
        <?php endif; ?>
        <?php if (isset($this->secondary) && count($this->secondary)): ?>
            <div id="itemListSecondary">
                <?php foreach ($this->secondary as $key => $item): ?>

                    <?php
                    // Define a CSS class for the last container on each row
                    if ((($key + 1) % ($this->params->get('num_secondary_columns')) == 0) || count($this->secondary) < $this->params->get('num_secondary_columns'))
                        $lastContainer = ' itemContainerLast';
                    else
                        $lastContainer = '';
                    ?>

                    <div class="    itemContainer<?php echo $lastContainer; ?>"<?php echo (count($this->secondary) == 1) ? '' : ' style="width:' . number_format(100 / $this->params->get('num_secondary_columns'), 1) . '%;"'; ?>>
                        <?php
                        // Load category_item.php by default
                        $this->item = $item;
                        echo $this->loadTemplate('item');
                        ?>
                    </div>
                    <?php if (($key + 1) % ($this->params->get('num_secondary_columns')) == 0): ?>
                        <div class="clr"></div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="clr"></div>
            </div>
        <?php endif; ?>
        <?php if (isset($this->links) && count($this->links)): ?>
            <!-- Link items -->
            <div id="itemListLinks">
                <h4><?php echo JText::_('K2_MORE'); ?></h4>
                <?php foreach ($this->links as $key => $item): ?>

                    <?php
                    // Define a CSS class for the last container on each row
                    if ((($key + 1) % ($this->params->get('num_links_columns')) == 0) || count($this->links) < $this->params->get('num_links_columns'))
                        $lastContainer = ' itemContainerLast';
                    else
                        $lastContainer = '';
                    ?>

                    <div class="itemContainer<?php echo $lastContainer; ?>"<?php echo (count($this->links) == 1) ? '' : ' style="width:' . number_format(100 / $this->params->get('num_links_columns'), 1) . '%;"'; ?>>
                        <?php
                        // Load category_item_links.php by default
                        $this->item = $item;
                        echo $this->loadTemplate('item_links');
                        ?>
                    </div>
                    <?php if (($key + 1) % ($this->params->get('num_links_columns')) == 0): ?>
                        <div class="clr"></div>
                    <?php endif; ?>
                <?php endforeach; ?>
                <div class="clr"></div>
            </div>
        <?php endif; ?>

    </div>


<?php endif; */?>
<!-- End K2 Category Layout -->
<div class="news-list-box">
    <div class="news-filter">
        <span>Рубрики:</span>
        <?
        $query->select('id, name')
                ->from('#__k2_categories')
                ->where('parent = 3');
        $subCats = $db->setQuery($query)->loadObjectList();
        $query->clear();
        ?>
        <?php
        foreach ($subCats as $key => $subCategory):
            $request = JFactory::getApplication()->input->request;
            ?>
            <a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&layout=category&task=category&itemId=3&id=' . $subCategory->id); ?>"<?
            if ($request->get('id', '', 'int') == $subCategory->id):print ' class="active"';
            endif;
            ?>>
                   <?php echo $subCategory->name; ?>
            </a>
        <?php endforeach; ?>
    </div>
    <?php if (isset($this->leading) && count($this->leading)): ?>
        <div class="inlineWrapper">
        <?php
        foreach ($this->leading as $key => $item):
            $date = explode(' ', $item->publish_up);
            $date = explode('-', $date[0]);
            $year = $date[0];
            $day_monf = $date[2] . '/' . $date[1];
            ?>
            <div class="slidesSec13">
                <a href="<?=JRoute::_($item->link)?>">
                    <div>
                        <h2><?= $day_monf ?></h2>
                        <h6><?= $year ?></h6>
                        <p><?= $item->title ?></p>
                    </div>
                    <div>
                        <img src="<?= $item->imageGeneric ?>">
                    </div>
                </a>
            </div>
        <? endforeach; ?>
        </div>
    <? endif; ?>
    <? if ($this->pagination->getPagesLinks()): ?>
        <div class="pagination-container">
            <?php if ($this->params->get('catPagination')) echo $this->pagination->getPagesLinks(); ?>
        </div>
    <?php endif; ?>
</div> 
