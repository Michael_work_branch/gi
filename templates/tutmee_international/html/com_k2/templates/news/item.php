<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
?>
<div class="news-box">
    <div class="news-head">
        <div class="content-container">
            <div class="img-cont"><img src="media/k2/items/src/<?= md5('Image' . $this->item->id) ?>.jpg" alt="" /></div>
            <table>
                <tr>
                    <td>
                        <?
                        $date = explode(' ', $this->item->created);
                        $date = explode('-', $date[0]);
                        $year = $date[0];
                        $day_monf = $date[2] . '/' . $date[1];
                        ?>
                        <p class="news-date"><?= $day_monf ?><span><?= $year ?></span></p>
                        <h2><?= $this->item->title ?></h2>
                    </td>
                </tr>
            </table>
            <div class="clear"></div>
        </div>
    </div>
    <div class="news-body">
        <div class="content-container">
            <?= $this->item->introtext ?>
        </div>
    </div>
    <div class="vk-comment">
        <div class="content-container">
            <button class="comment-button">Comment the news</button>
        </div>
        <div class="comment-body">
            <script type="text/javascript">
                VK.init({apiId: 5317632, onlyWidgets: true});
            </script>
            <div class="content-container" id="vk-comment"></div>
            <script type="text/javascript">
                VK.Widgets.Comments("vk-comment", {limit: 10, width: "1118", attach: "*"}, "news_<?=$this->item->id?>");
            </script>
        </div>
    </div>
    <div class="news-control">
        <div class="content-container">
            <!--<a href="#"><span class="fa fa-arrow-left"></span>предыдущая новость</a>-->
            <a href="<?= $this->item->category->link ?>">back to list</a>
            <!--<a href="#">следующая новость<span class="fa fa-arrow-right"></span></a>-->
        </div>
    </div>
    <div class="news-subscribe">
        <div class="content-container">
            <form action="#">
                <p class="forNameForm">Subscribe to news</p>
                <span>Subscribe to news</span>
                <div class="form-item"><input type="text" name="email" class="usermail"/><label>Your email</label></div>
                <button class="fa fa-envelope-o"></button>
            </form>
        </div>
    </div>
</div>