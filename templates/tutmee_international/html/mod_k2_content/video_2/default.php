<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
if (count($items)):
    ?>
    <div>
        <iframe width="560" height="315" src="<?= $items[0]->video ?>" frameborder="0" allowfullscreen></iframe>
    </div>
<? endif; ?>