<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
if (count($items)):
    foreach ($items as $key => $item):
        $date = explode(' ', $item->publish_up);
        $date = explode('-', $date[0]);
        $year = $date[0];
        $day_monf = $date[2] . '/' . $date[1];
        ?>
        <div class="slidesSec13">
            <a href="<?= JRoute::_($item->link) ?>">
                <div>
                    <h2><?= $day_monf ?></h2>
                    <h6><?= $year ?></h6>
                    <p><?= $item->title ?></p>
                </div>
                <div>
                    <img src="<?= $item->imageGeneric ?>">
                </div>
            </a>
        </div>
    <? endforeach; ?>
<? endif; ?>