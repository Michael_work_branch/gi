<?php
/**
 * @version		2.6.x
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2014 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */
// no direct access
defined('_JEXEC') or die;
if (count($items)):
    ?>
    <div><?= $items[0]->introtext ?></div>
    <div class="sliderWrap"><?
        foreach ($items[0]->attachments as $key => $item):
            ?>
            <div>
                <a href="<?= $item->titleAttribute ?>"><img src="<?= $item->link ?>"></a>
            </div>
        <?php endforeach; ?>
    </div>
<? endif; ?>