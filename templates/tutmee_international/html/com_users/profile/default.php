<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

//Из соображений обратной совместимости, а точнее невозможности переопределения рендеринга вида
//пишем все обработчики, по старинке, в этом файле

$jinput = JFactory::getApplication()->input;
$db = JFactory::getDbo();
$query = $db->getQuery(true);

//Пишем обработчик
if ($jinput->post->get('save', NULL, 'int')) {
    JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
    
    function saveUserData ($db, $query, $userdata, $id) {
        $query->update('#__users')
            ->set('email = ' . $db->quote($userdata['email']) . ''
                . ', username = ' . $db->quote($userdata['email'])
                . ', name = ' . $db->quote($userdata['name']))
            ->where('id = ' . (int)$id);
        $db->setQuery($query)->execute();
        $query->clear();
        
        $user = JFactory::getUser();
        $user->set('email', $userdata['email']);
        $user->set('name', $userdata['name']);
        
        return $user;
    }
    
    //Меняем пароли
    $pwd1 = $jinput->post->get('password1', NULL, 'string');
    $pwd2 = $jinput->post->get('password2', NULL, 'string');
    if (!empty($pwd1) && ($pwd1 == $pwd2)) {
        $pwd1 = md5($pwd1);
        $query->update('#__users')
            ->set('password = ' . $db->quote($pwd1))
            ->where('id = ' . (int)$this->data->id);
        $db->setQuery($query)->execute();
        $query->clear();
    }
    
    //Меняем регистрационные данные
    $userdata = $jinput->post->get('userdata', NULL, 'array');
    if (!empty($userdata)) {
        //Для начала проверим email, чтобы он не был занят другим пользователем
        $query->select('*')
            ->from('#__users')
            ->where('email = ' . $db->quote($userdata['email']), 'OR')
            ->where('username = ' . $db->quote($userdata['email']));
        $res = $db->setQuery($query)->loadObjectList();
        $query->clear();
        
        switch (count($res)) {
            case 0:
                $this->data = saveUserData($db, $query, $userdata, $this->data->id);
                break;
            case 1:
                //Если это email не нашего пользователя, то ничего не делаем, иначе:
                if ($this->data->id == $res[0]->id) {
                    $this->data = saveUserData($db, $query, $userdata, $this->data->id);
                }
                break;
            default:
                //Иначе произошла какая-то хрень и мы на всякий пожарный деактивируем пользователя
                $query->update('#__users')
                    ->set('block = 1')
                    ->where('id = ' . (int)$this->data->id);
                $db->setQuery($query)->execute();
                $query->clear();
                break;
        }
    }
    
    //Меняем прочие пользовательские данные
    $userinfos = $jinput->post->get('userinfos', NULL, 'array');
    if (count($userinfos)) {
        foreach ($userinfos as $k => $v) {
            $fields[] = $db->quoteName($k) . ' = ' . $db->quote($v);
        }
//        $conditions = ['virtuemart_user_id = ' . (int)$this->data->id];
        $conditions = 'virtuemart_user_id = ' . (int)$this->data->id;

        $query->update($db->quoteName('#__virtuemart_userinfos'))
                ->set(implode(',', $fields))
                ->where($conditions);
        $db->setQuery($query)->execute();
        $query->clear();
    }
}

//Выбираем пользовательские данные из таблицы VM
$query->select('*')
    ->from('#__virtuemart_userinfos')
    ->where('virtuemart_user_id = ' . (int) $this->data->id);
$userinfos = $db->setQuery($query)->loadObject();
$query->clear();

//Если по каким-то причинам записи с пользовательскими данными нет, то создаем ее
if (!$userinfos) {
    $query->insert('#__virtuemart_userinfos')
        ->columns('virtuemart_user_id')
        ->values((int)$this->data->id);
    $db->setQuery($query)->execute();
    $query->clear();
}
?>
<div class="lk-box">
    <ul class="lk-tab">
        <li class="active">user data</li>
        <li>history of orders</li>
<!--        <li>Накопительная программа</li>
        <li class="active">Спецпредложения</li>-->
    </ul>
    <ul class="lk-tab-body">
        <li class="active">
            <form action="<?= JRoute::_(htmlspecialchars(JUri::getInstance()->toString())) ?>" method="post" class="data-company">
                <div class="about-company">
                    <div class="content-container">
                        <fieldset>
<!--                            <div class="form-item" data-hint=""><input type="text" /><label>Компания</label> <span class="write-button fa fa-pencil "></span></div>-->
                            <div class="form-item" data-hint="">
                                <input type="text" name="userinfos[phone_1]" value="<?= $userinfos->phone_1 ?>" class="userphone"/>
                                <label>Phone</label> 
                                <span class="write-button fa fa-pencil "></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-item" data-hint="">
                                <input type="text" name="userdata[name]" value="<?= $this->data->name ?>"/>
                                <label>The contact person</label> 
                                <span class="write-button fa fa-pencil "></span>
                            </div>
                        </fieldset>
                        <fieldset>
                            <div class="form-item" data-hint="">
                                <input type="text" name="userdata[email]" value="<?= $this->data->email ?>" />
                                <label>Email</label> 
                                <span class="write-button fa fa-pencil "></span>
                            </div>
                        </fieldset>
                    </div>
                </div>
                <!--                            <div class="company-atributes">
                                                <div class="content-container">
                                                    <h2>Реквизиты компании</h2>
                                                    <fieldset>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label >Юридический адрес (обязательно)</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label >Банк (обязательно)</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                    </fieldset>
                                                    <div class="clear"></div>
                                                    <fieldset>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label >Расчетный счет (обязательно) </label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>КПП (обязательно)</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>ОКДП</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>Кор. счет (обязательно)</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>БИК банка(обязательно)</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>ОКПО</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                    </fieldset>
                                                    <fieldset>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>ИНН (обязательно)</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>ОКОНХ/ОКВЭД</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                        <div class="form-item" data-hint="">
                                                            <input type="text" />
                                                            <label>Сайт компании</label>
                                                            <span class="write-button fa fa-pencil "></span>
                                                        </div>
                                                    </fieldset>                                  
                                                </div>
                                            </div>-->
                <div class="company-adress">
                    <h2>delivery address</h2>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input type="text" name="userinfos[zip]" value="<?= $userinfos->zip ?>" />
                            <label>Index</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                        <div class="form-item" data-hint="">
                            <input type="text" name="userinfos[street]" value="<?= $userinfos->street ?>" />
                            <label>Street</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input type="text" name="userinfos[Region]" value="<?= $userinfos->Region ?>" />
                            <label>Region</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                        <div class="form-item" data-hint="">
                            <input type="text" name="userinfos[House]" value="<?= $userinfos->House ?>" />
                            <label>House number</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input type="text" name="userinfos[city]" value="<?= $userinfos->city ?>" />
                            <label>City</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                        <div class="form-item" data-hint="">
                            <input type="text" name="userinfos[kv]" value="<?= $userinfos->kv ?>" />
                            <label>Number of apartments / offices</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                </div>
                <div class="repassword">
                    <h2>Change password</h2>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input type="password" name="password1" />
                            <label>New password</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input type="password" name="password2" />
                            <label>Confirm password</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                </div>
                <?= JHtml::_('form.token'); ?>
                <input type="hidden" name="save" value="1" />
                <button class="btnStyle">save</button>
            </form>
        </li>
        <li>
            <!--
            <div class="order-history">
                <div class="content-container">
                    <div class="order-sort">
                        <span>Показать заказы:</span>
                        <a href="#" class="active">ВСЕ</a>
                        <a href="#">АКТИВНЫЕ</a>
                        <a href="#">ВЫПОЛНЕННЫЕ</a>
                    </div>
                </div>
                <div class="order-list">
                    <div class="order-item">
                        <div class="item-heading">
                            <div class="content-container">
                                <p class="heading-item"><span>Заказ номер:</span><span>#24452445</span></p>
                                <p class="heading-item"><span>Статус:</span><span>В обработке</span></p>
                                <p class="heading-item"><span>Дата заказа:</span><span>11.11.2015</span></p>
                                <p class="heading-item"><span>Сумма:</span><span>9 999 999 руб.</span></p>
                                <span class="fa fa-chevron-down openorder" data-alt-class="fa-chevron-up"></span>
                            </div>
                        </div>
                        <div class="item-body">
                            <div class="content-container">
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="order-item">
                        <div class="item-heading">
                            <div class="content-container">
                                <p class="heading-item"><span>Заказ номер:</span><span>#24452445</span></p>
                                <p class="heading-item"><span>Статус:</span><span>В обработке</span></p>
                                <p class="heading-item"><span>Дата заказа:</span><span>11.11.2015</span></p>
                                <p class="heading-item"><span>Сумма:</span><span>9 999 999 руб.</span></p>
                                <span class="fa fa-chevron-down openorder" data-alt-class="fa-chevron-up"></span>
                            </div>
                        </div>
                        <div class="item-body">
                            <div class="content-container">
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="order-item">
                        <div class="item-heading">
                            <div class="content-container">
                                <p class="heading-item"><span>Заказ номер:</span><span>#24452445</span></p>
                                <p class="heading-item"><span>Статус:</span><span>В обработке</span></p>
                                <p class="heading-item"><span>Дата заказа:</span><span>11.11.2015</span></p>
                                <p class="heading-item"><span>Сумма:</span><span>9 999 999 руб.</span></p>
                                <span class="fa fa-chevron-down openorder" data-alt-class="fa-chevron-up"></span>
                            </div>
                        </div>
                        <div class="item-body">
                            <div class="content-container">
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="order-item">
                        <div class="item-heading">
                            <div class="content-container">
                                <p class="heading-item"><span>Заказ номер:</span><span>#24452445</span></p>
                                <p class="heading-item"><span>Статус:</span><span>В обработке</span></p>
                                <p class="heading-item"><span>Дата заказа:</span><span>11.11.2015</span></p>
                                <p class="heading-item"><span>Сумма:</span><span>9 999 999 руб.</span></p>
                                <span class="fa fa-chevron-down openorder" data-alt-class="fa-chevron-up"></span>
                            </div>
                        </div>
                        <div class="item-body">
                            <div class="content-container">
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="merchendise">
                                    <table>
                                        <tr>
                                            <td rowspan="4" class="img"><img src="images/merch.png" alt="" /></td>
                                            <td class="fname">Наименование</td>
                                            <td class="fvalue"><a href="#">Маска тройного действия для лица GI BEAUTY</a></td>
                                        </tr>
                                        <tr>                                                       
                                            <td class="fname">Цена</td>
                                            <td class="fvalue">100 000 <i class="fa fa-rub"></i></td>
                                        </tr>
                                        <tr>                                                        
                                            <td class="fname">Кол-во</td>
                                            <td class="fvalue">1</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <form action="#">
                                                    <button class="btnStyle"><i class="fa  fa-shopping-bag"></i>заказать еще раз </button>
                                                </form>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            -->
        </li>
<!--        <li>
            <div class="loyalty-program">
                <div class="loyalty-program-top-belt">
                    <div class="content-container">
                        <p>Ваша накопительная скидка:</p>
                        <div class="discount-count">
                            <span>10%</span>
                        </div>
                        <p>Вы приобрели<span>100 товаров</span>на сумму<span>9 999 999 руб</span></p>
                    </div>
                </div>
                <div class="loyalty-program-body">
                    <div class="content-container">
                        <a href="#" class="loyalty-program-item" data-content="использовать скидку">
                            <span class="cut fa fa-scissors"></span>
                            <p>Скидка</p>
                            <p>5<span>%</span></p>
                            <p>если общая сумма покупок превышает</p>
                            <p>100 000 <i class="fa fa-rub"></i></p>
                        </a>
                        <a href="#" class="loyalty-program-item active" data-content="использовать скидку">
                            <span class="cut fa fa-scissors"></span>
                            <p>Ваша скидка</p>
                            <p>10<span>%</span></p>
                            <p>общая сумма покупок превышает</p>
                            <p>200 000 <i class="fa fa-rub"></i></p>
                        </a>
                        <a href="#" class="loyalty-program-item" data-content="использовать скидку">
                            <span class="cut fa fa-scissors"></span>
                            <p>Скидка</p>
                            <p>15<span>%</span></p>
                            <p>если общая сумма покупок превышает</p>
                            <p>300 000<i class="fa fa-rub"></i></p>
                        </a>
                        <div class="loyalty-text">
                            <p>Программа лояльности доступна только при условии: Верьте аль не верьте, а жил на белом свете Федот-стрелец, удалой молодец. Был Федот ни красавец, ни урод, ни румян, ни бледен, ни богат, ни беден, ни в парше, ни в парче, а так, вообче. Служба у Федота — рыбалка да охота. Царю — дичь да рыба, Федоту — спасибо. Гостей во дворце — как семян в огурце. Один из Швеции, другой из Греции, третий с Гавай — и всем жрать подавай! Одному — омаров, другому — кальмаров, третьему — сардин, а добытчик один! Как-то раз дают ему приказ: чуть свет поутру явиться ко двору. Царь на вид сморчок, башка с кулачок, а злобности в ем — агромадный объем. Смотрит на Федьку, как язвенник на редьку. На Федьке от страха намокла рубаха, в висках застучало, в пузе заурчало, тут, как говорится, и сказке начало...
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li class="active">
            <div class="spec-box">
                <div class="content-container">
                    <a href="#" class="spec-item"><img src="images/banner1.jpg" alt="" /></a>
                    <a href="#" class="spec-item"><img src="images/banner2.jpg" alt="" /></a>
                    <a href="#" class="spec-item"><img src="images/banner3.jpg" alt="" /></a>
                </div>
            </div>
        </li>-->
    </ul>
</div>