<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_breadcrumbs
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');

if (count($list)) {
    ?>

    <div class="breadcrmbs">
        <div class="content-container">

            <a href="./"><i class="fa fa-home"></i></a>
            <span class="fa  fa-long-arrow-right"></span>

            <?php
// Find last and penultimate items in breadcrumbs list
            end($list);
            $last_item_key = key($list);
            prev($list);
            $penult_item_key = key($list);

// Make a link if not the last item in the breadcrumbs
            $show_last = $params->get('showLast', 1);

// Generate the trail
            foreach ($list as $key => $item) :
                if ($key != $last_item_key) :
                    if (!empty($item->link)) :
                        ?>
                        <a href="<?php echo $item->link; ?>">
                        <? endif; ?>
                        <span>
                            <?php echo $item->name; ?>
                        </span>
                        <? if (!empty($item->link)) : ?>
                        </a>
                    <?php endif; ?>

                    <?php if (($key != $penult_item_key) || $show_last) : ?>
                        <span class="divider">
                            <?php echo $separator; ?>
                        </span>
                    <?php endif; ?>
                    <span class="fa  fa-long-arrow-right"></span>
                    <?php
                elseif ($show_last) :
                    // Render last item if reqd. 
                    ?>
                    <span itemprop="name">
                        <?php echo $item->name; ?>
                    </span>
                    <?php
                endif;
            endforeach;
            ?>
        </div>
    </div>
<?php } ?>