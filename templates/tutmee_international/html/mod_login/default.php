<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_SITE . '/components/com_users/helpers/route.php';
?>

<form class="ajaxStop" action="<?php echo JRoute::_(htmlspecialchars(JUri::getInstance()->toString()), true, $params->get('usesecure')); ?>" method="post">
    <div class="form-item " data-hint="Тест тест тест">
        <input type="text" name="username" />
        <label>Email</label>
    </div>
    <div class="form-item " data-hint="">
        <input type="password" name="password" />
        <label>Password</label>
    </div>
    <div class="check-block">
        <div class="check-wrapp">
            <input type="checkbox" name="remember" value="yes" />
            <label class="fa fa-check"></label>
        </div>
        <span>Remember me</span>
    </div>
    <a href="<?php echo JRoute::_('index.php?option=com_users&view=reset&Itemid=' . UsersHelperRoute::getResetRoute()); ?>" class="remem">Forgot your password?</a>
    <input type="hidden" name="option" value="com_users" />
    <input type="hidden" name="task" value="user.login" />
    <input type="hidden" name="return" value="<?php echo $return; ?>" />
    <?php echo JHtml::_('form.token'); ?>
    <button>login</button>
</form>