<?php
/**
 *
 * Show the products in a category
 *
 * @package    VirtueMart
 * @subpackage
 * @author RolandD
 * @author Max Milbers
 * @todo add pagination
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 9017 2015-10-14 10:44:34Z Milbo $
 */
defined('_JEXEC') or die('Restricted access');
?>
<div class="catalog-box">
    <?
//    $document = & JFactory::getDocument();
//    $renderer = $document->loadRenderer('modules');
//    $options = array();
//    $position = 'search_kat';
//    echo $renderer->render($position, $options, null);
    ?>
    <? /* --------------------------------------- */ ?>

    <? /* --------------------------------------- */ ?>
    <div class="catalog-body">
        <div class="content-container">
            <?php
            /** Вывод  * */
            if ($this->showproducts) {
                if (!empty($this->products)) {
                    $products = array();
                    $products[0] = $this->products;
                    echo shopFunctionsF::renderVmSubLayout($this->productsLayout, array('products' => $products, 'currency' => $this->currency, 'products_per_row' => $this->perRow));
                } elseif (!empty($this->keyword)) {
                    echo vmText::_('COM_VIRTUEMART_NO_RESULT') . ($this->keyword ? ' : (' . $this->keyword . ')' : '');
                }
                ?>
            <? } ?>
        </div>
    </div>