<?php
/**
 * sublayout products
 *
 * @package	VirtueMart
 * @author Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
 * @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
 */
defined('_JEXEC') or die('Restricted access');

$product = $viewData['product'];
$position = $viewData['position'];
$customTitle = isset($viewData['customTitle']) ? $viewData['customTitle'] : false;

if ($position === 'description') {
    ?>
    <div class="content-container">
        <ul class="prod-main-tab">
            <li class="active">описание</li>
            <li>отзывы</li>
        </ul>
    </div>
    <div class="bord"></div>
    <div class="content-container">
        <ul class="prod-main-tab-body">
            <li class="active">
                <ul class="prod-inner-tab">
                    <?
                    $i = true;
                    foreach ($product->customfieldsSorted[$position] as $field) {
                        if ($field->is_hidden || empty($field->display))
                            continue;
                        elseif ($field->custom_parent_id == 3 /* or $field->virtuemart_custom_id == 5 or $field->virtuemart_custom_id == 6 */) {
                            ?>
                            <li<? ($i) ? print' class="active"' and $i = false : false ?>><?= $field->custom_title ?></li>
                            <?
                        }
                    }
                    ?>
                </ul>
                <ul class="prod-inner-tab-body">
                    <?
                    $i = true;
                    foreach ($product->customfieldsSorted[$position] as $field) {
                        if ($field->is_hidden || empty($field->display))
                            continue;
                        elseif ($field->custom_parent_id == 3) {
                            ?>
                            <li<? ($i) ? print' class="active"' and $i = false : false ?>><?= $field->customfield_value ?></li>
                            <?
                        }
                    }
                    ?>                        

                </ul>
            </li>
            <li>
                <script type="text/javascript">
                    VK.init({apiId: 5317632, onlyWidgets: true});
                </script>
                <div id="vk-comment" class="vk-comment"></div>
                <script type="text/javascript">
                    VK.Widgets.Comments("vk-comment", {limit: 10, width: "1118", attach: "*"}, "product_<? $product->virtuemart_product_id ?>");
                </script>
            </li>
        </ul>
    </div>
    <?
} elseif ($position === 'short_description') {
    echo array_pop($product->customfieldsSorted[$position])->customfield_value;
}?>