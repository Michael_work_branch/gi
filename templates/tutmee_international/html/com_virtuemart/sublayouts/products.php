<?php
/**
 * sublayout products
 *
 * @package	VirtueMart
 * @author Max Milbers
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL2, see LICENSE.php
 * @version $Id: cart.php 7682 2014-02-26 17:07:20Z Milbo $
 */
defined('_JEXEC') or die('Restricted access');
$products_per_row = $viewData['products_per_row'];
$currency = $viewData['currency'];
$showRating = $viewData['showRating'];
$verticalseparator = " vertical-separator";
echo shopFunctionsF::renderVmSubLayout('askrecomjs');

$ItemidStr = '';
$Itemid = shopFunctionsF::getLastVisitedItemId();
if (!empty($Itemid)) {
    $ItemidStr = '&Itemid=' . $Itemid;
}

foreach ($viewData['products'] as $type => $products) {

    foreach ($products as $product) {
        // this is an indicator wether a row needs to be opened or not
        // Show the vertical seperator
        if ($nb == $products_per_row or $nb % $products_per_row == 0) {
            $show_vertical_separator = ' ';
        } else {
            $show_vertical_separator = $verticalseparator;
        }

        // Show Products 
        ?>
        <div class="merchendise-item byin" data-prod-id="<?=$product->virtuemart_product_id?>">
            <a class="img-cont" href="<?php echo $product->link . $ItemidStr; ?>">
                <?php
//                echo $product->images[0]->displayMediaThumb('class="browseProductImage"', false);
                ?>
                <img src="./<?= $product->images[0]->file_url?>"/>
            </a>
            <div class="merch-descr"><a href="<?php echo $product->link . $ItemidStr; ?>"><?php echo JHtml::link($product->link . $ItemidStr, $product->product_name); ?></a></div>
            <div class="merch-control-belt">
                <div class="merchprice"><? echo shopFunctionsF::renderVmSubLayout('prices', array('product' => $product, 'currency' => $currency)); ?></div>
                <button class="inclick opencommonpopup" data-id="by_inclick" onclick="yaCounter35741200.reachGoal('oneclick'); return true;"><i class="fa fa-hand-o-up"></i><span>Купить в 1 клик</span></button>
                <? echo shopFunctionsF::renderVmSubLayout('addtocart', array('product' => $product,)); ?>
                <!--<a href="#" class="incart fa  fa-shopping-bag"></a>-->
            </div>
        </div>
        <?
        
    }
}
?>