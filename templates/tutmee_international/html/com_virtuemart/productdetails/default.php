<?php
/**
 *
 * Show the product details page
 *
 * @package	VirtueMart
 * @subpackage
 * @author Max Milbers, Eugen Stranz, Max Galt
 * @link http://www.virtuemart.net
 * @copyright Copyright (c) 2004 - 2014 VirtueMart Team. All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * @version $Id: default.php 9058 2015-11-10 18:30:54Z Milbo $
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

/* Let's see if we found the product */
if (empty($this->product)) {
    echo vmText::_('COM_VIRTUEMART_PRODUCT_NOT_FOUND');
    echo '<br /><br />  ' . $this->continue_link_html;
    return;
}

echo shopFunctionsF::renderVmSubLayout('askrecomjs', array('product' => $this->product));
$document = JFactory::getDocument();
$document->addScript("/templates/" . JFactory::getApplication()->getTemplate() . "/js/jquery.bxslider.js");
$document->addStyleSheet("/templates/" . JFactory::getApplication()->getTemplate() . "/js/jquery.bxslider.js");

$jinput = JFactory::getApplication()->input;
$last_products = $jinput->cookie->get('last_products', null, 'string');
if ($last_products):
    $last_products = json_decode($last_products);
endif;
?>
<div class="product-box">
    <div class="product-top-belt">
        <div class="content-container">
            <div class="left-side">
                <div class="product-slider">
                    <?php
                    foreach ($this->product->images as $image) {
                        echo "<img src='http://" . $_SERVER['HTTP_HOST'] . DIRECTORY_SEPARATOR . $image->file_url . "' alt='' />";
                    }
                    ?>
                </div>
            </div>
            <div class="right-side">
                <h2><?php echo $this->product->product_name ?></h2>
                <div class='product-properties'>
                    <? echo shopFunctionsF::renderVmSubLayout('customfields', array('product' => $this->product, 'position' => 'short_description')); ?>
                </div>

                <form action="<?php echo JRoute::_('index.php?option=com_virtuemart', false); ?>" data-target="in_cart_form" method="post"  class="product product-controll-block byin" data-prod-id="<?=$this->product->virtuemart_product_id?>">
                    <div class="price-count">
                        <div class="prodprice"><? echo shopFunctionsF::renderVmSubLayout('prices', array('product' => $this->product)); ?></div>
                        <div class="merchendise-count">
                            <span class="minus">-</span>
                            <input type="text" class="number" value="1" name="quantity" readonly="readonly"/>
                            <span class="plus">+</span>
                        </div>
                    </div>
                    <div class="in-click opencommonpopup" data-id="by_inclick" onclick="yaCounter35741200.reachGoal('oneclick'); return true;"><i class="fa fa-hand-o-up"></i><span >Купить в 1 клик</span></div>
                    <button class="vmAddCartSlider incart fa fa-shopping-bag"></button>

                    <input type="hidden" name="virtuemart_product_id" value="<?php echo $this->product->virtuemart_product_id ?>"/>
                    <input type="hidden" name="pname" value="<?php echo $this->product->product_name ?>"/>
                    <input type="hidden" name="pid" value="<?php echo $this->product->virtuemart_product_id ?>"/>
                </form>

            </div>
            <div class="clear"></div>
        </div>
    </div>
    <!--Готово-->
    <div class="product-bottom-belt">
        <? echo shopFunctionsF::renderVmSubLayout('customfields', array('product' => $this->product, 'position' => 'description')); ?>
    </div>
    <!--/Готово-->
    <? if ($last_products):
        ?>
        <div class="recent">
            <div class="section8">
                <div class="animationH2">
                    <h2>Вы недавно просматривали</h2>
                    <span ></span>
                    <span ></span>
                </div>
            </div>
            <div class="sliderSec8">
                <?
                foreach ($last_products as $product):
                    ?>
                    <a href="<?= $product->link ?>"><div class="img-cont">
                            <img src="<?= $product->image ?>" alt="" />
                        </div>
                        <p><?= $product->title ?></p>
                    </a>

                    <?
                endforeach;
                ?>
            </div>
        </div>
    <? endif; ?>
</div>

<?
$new = array(
    'virtuemart_product_id' => $this->product->virtuemart_product_id,
    'image' => "http://" . $_SERVER['HTTP_HOST'] . DIRECTORY_SEPARATOR . $image->file_url,
    'link' => JRoute::_($this->product->link),
    'title' => $this->product->product_name
);
$isset = false;
if ($last_products) {
    foreach ($last_products as &$product) {
        if ($new['virtuemart_product_id'] == $product->virtuemart_product_id)
            $isset = true;
    }
    if (!$isset)
    {
        $last_products[] = $new; 
    }
} else{
    $last_products = array($new);
}

$jinput->cookie->set('last_products', json_encode($last_products), time() + (3600 * 24), '/');
?>
