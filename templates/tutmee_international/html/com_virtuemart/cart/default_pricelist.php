<div class="merch-scroll-block">
    <table>
        <?php
        foreach ($this->cart->products as $pkey => $prow) {
            $prow->prices = array_merge($prow->prices, $this->cart->cartPrices[$pkey]);
            ?>
            <tr>
                <td><!--Картинка-->
                    <input type="hidden" name="cartpos[]" value="<?php echo $pkey ?>">
                    <!--Картинка-->
                    <?php if ($prow->virtuemart_media_id) { ?>
                        <?php
                        if (!empty($prow->images[0])) {
                            echo $prow->images[0]->displayMediaThumb('', FALSE);
                        }
                    }
                    echo JHtml::link($prow->url, $prow->product_name);
//                echo $this->customfieldsModel->CustomsFieldCartDisplay($prow);
                    ?>
                </td>
                <td class="priceForOne"><!--цена-->
                    <p><?php
                        echo
                        number_format(round($prow->prices['product_price']), 0, '.', ' ');
                        ?></p><span class="fa fa-rub"></span>
                </td>
                <td><!--количество-->
                    <div class="merchendise-count">
                        <span class="minus">-</span>
                        <input type="text" name="quantity[<?php echo $pkey; ?>]" class="number" value="<?php echo $prow->quantity ?>" readonly="readonly"/>
                        <span class="plus">+</span>
                    </div>
                </td>
                <td class="priceTotal"><!--Общая стоиость-->
                    <p><?php
                        echo
                        number_format(round($prow->prices['product_price'] * $prow->quantity), 0, '.', ' ');
                        ?></p><span class="fa fa-rub"></span>
                </td>
                <td>
                    <div class="delete fa fa-trash" name="delete.<?php echo $pkey ?>" title="<?php echo vmText::_('COM_VIRTUEMART_CART_DELETE') ?>" ></div>
                    <input type="hidden" name="virtuemart_product_id" value="<?= $prow->virtuemart_product_id ?>" />
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>
<table  class="cart-result">
    <tr>
        <td >
            <!--<span>Вы экономите:</span><span>9 999 999<i class="fa fa-rub"></i></span>-->
        </td>
        <td >
            <span>Сумма заказа:</span>
            <span class="oldprice"></span>
            <span class="endprice"><span><?= $this->cart->cartPrices['basePrice'] ?></span><i class="fa fa-rub"></i></span>
        </td>                                    
    </tr>
</table>
<? ?>
