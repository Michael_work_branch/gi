<?php
defined('_JEXEC') or die('');

/**
*
* Template for the shopping cart
*
* @package	VirtueMart
* @subpackage Cart
* @author Max Milbers
*
* @link http://www.virtuemart.net
* @copyright Copyright (c) 2004 - 2010 VirtueMart Team. All rights reserved.
* @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
* VirtueMart is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
*/
?>
<div class="thanksPage">
	<div>
		<h2>Спасибо за заказ!</h2>
		<p>номер вашего заказа: <span class="numberOrder">21458769</span></p>
		<p>На вашу почту <a href="#" class="feedback">info@mail.ru</a> было выслано письмо с деталями закза.</p>
		<p>Если у вас остались какие-то вопросы, мы всегда рабы на них ответить по телефону: 8 (968) 686-19-79</p>
	</div>
</div>




