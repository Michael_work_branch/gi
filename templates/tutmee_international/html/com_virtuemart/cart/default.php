<?php
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.formvalidation');

$user = JFactory::getUser();
if (!$user->guest):
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);
    $query->select('zip, city, Region, street, House, kv')
            ->from('#__virtuemart_userinfos')
            ->where('virtuemart_user_id = ' . $user->id);
    $addres = $db->setQuery($query)->loadObjectList();
    $query->clear();
endif;
?>
<div id="cart">
    <div class="content-container">
        <h2>ваши товары</h2>
        <table class="cart-header">
            <tr>
                <td>Наименование</td>
                <td>Цена</td>
                <td>Кол-во</td>
                <td>Стоимость</td>
                <td>Удалить</td>
            </tr>
        </table>
        <?
        echo $this->loadTemplate('pricelist');
        ?>
    </div>
    <form id="new_order" action="<?= JRoute::_('/index.php?option=com_awmart&view=newOrder'); ?>" method="post" class="ajaxStop">
        <div class="data-company">
            <div class="company-adress">
                <div class="content-container">
                    <div class="dellivery-label" data-text="Доставка по России - бесплатно"></div>
                    <h2>адрес доставки</h2>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input form="new_order" name="address[index]" type="text" <? isset($addres) ? print 'value="' . $addres[0]->zip . '" ' : false ?>/>
                            <label >Индекс</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                        <div class="form-item" data-hint="">
                            <input form="new_order" name="address[street]" type="text" <? isset($addres) ? print 'value="' . $addres[0]->street . '" ' : false ?>/>
                            <label >Улица</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input form="new_order" name="address[region]" type="text" <? isset($addres) ? print 'value="' . $addres[0]->Region . '" ' : false ?>/>
                            <label >Область / Край</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                        <div class="form-item" data-hint="">
                            <input form="new_order" name="address[hous]" type="text" <? isset($addres) ? print 'value="' . $addres[0]->House . '" ' : false ?>/>
                            <label >Номер дома</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                        <div class="form-item" data-hint="">
                            <input form="new_order" name="phone" required class="userphone"  type="text" <? isset($addres) ? print 'value="' . $addres[0]->House . '" ' : false ?>/>
                            <label >Номер телефона</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                    <fieldset>
                        <div class="form-item" data-hint="">
                            <input form="new_order" name="address[city]" type="text" <? isset($addres) ? print 'value="' . $addres[0]->city . '" ' : false ?>/>
                            <label >Город</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                        <div class="form-item" data-hint="">
                            <input form="new_order" name="address[apartment]" type="text" <? isset($addres) ? print 'value="' . $addres[0]->kv . '" ' : false ?>/>
                            <label >Номер квартиры / офиса</label>
                            <span class="write-button fa fa-pencil "></span>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <input type="hidden" value="<?= $this->cart->cartPrices['basePrice'] ?>" id="endprice_hidden" name="endprice" />
    </form>
    <div class="content-container">
        <div class="cart-control-belt">
            <a href="<?= JRoute::_('index.php?option=com_virtuemart&view=category&virtuemart_category_id=1') ?>" class="cart-control"><span class="fa fa-arrow-left"></span>продолжить покупки</a>
            <button form="new_order" class="cart-control">оформить заказ<span class="fa fa-arrow-right"></span></button>
            <div class="robokassa"><span>Оплата через платежную
                    систему ROBOKASSA</span><img src="images/robocassa.png" alt="" /></div>
            <div class="clear"></div>
        </div> 
    </div>               
</div>
