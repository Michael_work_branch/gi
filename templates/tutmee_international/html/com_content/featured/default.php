<?
$document = JFactory::getDocument();
$renderer = $document->loadRenderer('modules');
$options = array();
?>
<!----------------------------------SECTION-1-------------------------------------------------->
<section class="section1">
    <div>
        <h2>INNOVATIVE PRODUCTS OF THE HIGHEST QUALITY!</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <a href="<?php echo JRoute::_('images/pdf/present_osnova.pdf', false); ?>" target="_blank">
            <button class="btnStyle">Open presentation</button>
        </a>
    </div>
</section>
<!----------------------------------SECTION-2-------------------------------------------------->
<section class="section2">
    <div>
        <div class="sec2TitleWrap animated">
            <img src="images/section2/openbook.png">
            <h3>Pick the best from the catalog!</h3>
        </div>
        <div class="maleWrap">
            <div class="womenCat animated">
                <p>For</p>
                <h4>lady</h4>
                <img src="images/section2/lady.png">
                <div class="maskSec2"></div>
                <span class="sec2Anim1"></span>
                <span class="sec2Anim2"></span>
                <span class="sec2Anim3"></span>
            </div>
            <div class="manCat animated">
                <p>For</p>
                <h4>gentlemen</h4>
                <img src="images/section2/gentleman.png">
                <div class="maskSec2"></div>
                <span class="sec2Anim4"></span>
                <span class="sec2Anim5"></span>
                <span class="sec2Anim6"></span>
            </div>
        </div>
        <div class="catalogSec2">
            <div class="genderWrap">
                <button class="btnStyle" id="forLady">For lady</button>
                <button class="btnStyle" id="forGentleman">For gentlemen</button>
            </div>
            <div class="sliderSec2-1 sliderSec2First main">

            </div>
            <div class="sliderSec2-1 sliderSec2Second main">

            </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-3-------------------------------------------------->
<section class="section3">
    <div class="animationH2">
        <h2>«НПО ЛИОМАТРИКС» - essential experience, scientific knowledge</h2>
        <span></span>
        <span></span>
    </div>
    <div class="sec3Wrap">
        <div>
            <img src="images/section3/img4.png" class="sec3Anim1 animated">
            <h6>Our scientists carry out</h6>
            <p><span data-value="7">0</span> innovative scientific researches</p>
        </div>
        <div>
            <img src="images/section3/img3.png" class="sec3Anim2 animated">
            <h6>Participated in</h6>
            <p>в <span data-value="78">0</span> scientific conferences</p>
        </div>
        <div>
            <img src="images/section3/img2.png" class="sec3Anim3 animated">
            <h6>We work with</h6>
            <p><span data-value="6">0</span> leading research centres</p>
        </div>
        <div>
            <img src="images/section3/img1.png" class="sec3Anim4 animated">
            <h6>And cooperate with more than</h6>
            <p><span data-value="54">0</span> clinics around the world</p>
        </div>
    </div>
</section>
<!----------------------------------SECTION-4-------------------------------------------------->
<section class="section4">
    <div>
        <span class="sec4Anim sec4Block1">
            <span></span>
        </span>
        <span class="sec4Anim sec4Block2">
            <span></span>
        </span>
        <span class="sec4Anim sec4Block3">
            <span></span>
        </span>
        <span class="sec4Anim sec4Block4">
            <span></span>
        </span>
        <div class="sec4Wrap">
            <?
            $position = 'presedent';
            echo $renderer->render($position, $options, null);
            ?>
        </div>
    </div>
</section>
<!----------------------------------SECTION-5-------------------------------------------------->
<section class="section5">
    <div>
        <div class="slider-indicator">
            <h2>Our partners</h2>
            <span></span>
        </div>
        <div class="sliderSec5">
            <?
            $position = 'partners';
            echo $renderer->render($position, $options, null);
            ?>
        </div>
    </div>
</section>
<!----------------------------------SECTION-6-------------------------------------------------->
<section class="section6">
    <div class="animationH2">
        <h2>Credo  «НПО ЛИОМАТРИКС» - continuous improvement!</h2>
        <span></span>
        <span></span>
    </div>
    <div class="sec6AnimWrap">
        <div class="sec6Anim1-1 animated">
            <img src="images/section6/img1Sec6.png">
            <p>The company has the most advanced scientific base</p>
        </div>
        <div class="sec6Anim2-1 animated">
            <img src="images/section6/img2Sec6.png">
            <p>The team - a team of like-minded</p>
        </div>
        <div class="sec6Anim3-1 animated">
            <img src="images/section6/img3Sec6.png">
            <p>Constantly we generate innovative ideas</p>
        </div>
    </div>
    <div class="sec6Wrap animationH2">
        <span></span>
        <h3>Actively participate in a competition of innovative research projects</h3>
        <div class="sec6WrapInline">
            <img src="images/section6/logo1Sec6.png" class="sec6Anim1 animated">
            <img src="images/section6/logo2Sec6.png" class="sec6Anim2 animated">
            <img src="images/section6/logo3Sec6.png" class="sec6Anim3 animated">
            <img src="images/section6/logo4Sec6.png" class="sec6Anim4 animated">
            <img src="images/section6/logo5Sec6.png" class="sec6Anim5 animated">
        </div>
        <h3>and win!</h3>
        <span></span>
    </div>
</section>
<!----------------------------------SECTION-7-------------------------------------------------->
<section class="section7">
    <div>
        <h3>WE ENGAGED IN THE DEVELOPMENT, RESEARCH AND EVALUATION OF THE DIFFERENT BIOPLASTIC MATERIALS WITH THE USE OF CELL TECHNOLOGIES FOR THE TREATMENT OF PATIENTS</h3>
        <div class="sec7Wrap">
            <?
            $position = 'video_1';
            echo $renderer->render($position, $options, null);
            $position = 'video_2';
            echo $renderer->render($position, $options, null);
            ?>
        </div>
    </div>
</section>
<!----------------------------------SECTION-8-------------------------------------------------->
<section class="section8">
    <div class="animationH2">
        <h2>THERE IS NO BEAUTY WITHOUT SCIENCE!</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <p><span>«НПО ЛИОМАТРИКС»</span> creates a new generation of cosmeceutical products. All products are manufactured in accordance with the international standard GMP, are tested and released for sale only after being proven
         to be effective.</p>
        <div class="sliderSec8">
            <div class="slidesSec8">
                <img src="images/section8/img1Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img2Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img3Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img4Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img5Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img6Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img7Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img4Sec8.png">
            </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-9-------------------------------------------------->
<section class="section9">
    <div class="animationH2">
        <h2>WANNA FIND OUT MORE?</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <form data-target="imm_form">
            <p class="forNameForm">Want to know more?</p>
            <h3>ASK A MANAGER «НПО ЛИОМАТРИКС»</h3>
            <div class="formWrap">
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Your name</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone" onclick="yaCounter35741200.reachGoal('lmm_phone'); return true;">
                    <span></span>
                    <label>Your phone number*</label>
                </div>
            </div>
            <div class="textAreaStyle">
                <textarea name="question"></textarea>
                <span></span>
                <label>Ask a question*</label>
            </div>
            <button class="btnStyle">Send</button>
            <div class="sec9WrapText">
                <p>Company policy is absolute confidentiality in respect of current and future customers</p>
            </div>
        </form>
    </div>
</section>
<!----------------------------------SECTION-10------------------------------------------------>
<section class="section10">
    <div class="animationH2">
        <h2>WE ARE FOR THE LATITUDE RANGE!</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <div>
            <h4>we work</h4>
            <div class="sec10Wrap">
                <div class="sec10Anim1-1 animated">
                    <h3>WITH COSMETOLOGISTS</h3>
                    <img src="images/section10/bg1Sec10.png">
                    <a href="<?= JRoute::_('/index.php?option=com_content&view=featured&Itemid=143') ?>" class="maskSec2"></a>
                    <span class="sec10Anim1"></span>
                    <span class="sec10Anim2"></span>
                    <span class="sec10Anim3"></span>
                </div>
                <div class="sec10Anim2-1 animated">
                    <h3>WITH SALONS AND CLINICS</h3>
                    <img src="images/section10/bg2Sec10.png">
                    <a href="<?= JRoute::_('/index.php?option=com_content&view=featured&Itemid=144') ?>" class="maskSec2"></a>
                    <span class="sec10Anim4"></span>
                    <span class="sec10Anim5"></span>
                    <span class="sec10Anim6"></span>
                </div>
                <div class="sec10Anim3-1 animated">
                    <h3>WITH SELF-COSMETOLOGISTS</h3>
                    <img src="images/section10/bg3Sec10.png">
                    <a href="<?= JRoute::_('/index.php?option=com_content&view=featured&Itemid=145') ?>" class="maskSec2"></a>
                    <span class="sec10Anim7"></span>
                    <span class="sec10Anim8"></span>
                    <span class="sec10Anim9"></span>
                </div>
                <div class="sec10Anim4-1 animated">
                    <h3>AND SEARCHING FOR NEW PARTNERS</h3>
                    <img src="images/section10/bg4Sec10.png">
                    <a href="<?= JRoute::_('/index.php?option=com_content&view=featured&Itemid=146') ?>" class="maskSec2"></a>
                    <span class="sec10Anim10"></span>
                    <span class="sec10Anim11"></span>
                    <span class="sec10Anim12"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-11-------------------------------------------->
<section class="section11 calc">
    <div>
        <h4>calculator</h4>
        <form>
            <div class="sec11WrapTop">
                <div class="checkBtnWrap">
                    <div class="radioWrap" id="sex">
                        <div class="radioStyle">
                            <input type="radio" name="gender" value="lady">
                            <div>
                                <img src="images/section11/lips.png">
                            </div>
                        </div>
                        <div class="radioStyle">
                            <input type="radio" name="gender" value="man">
                            <div>
                                <img src="images/section11/mustache.png">
                            </div>
                        </div>
                    </div>
                    <label>What's your gender</label>
                </div>
                <div class="borderForm"></div>
                <div class="checkBtnWrap" id="age">
                    <div class="slider-range"></div>
                    <input type="hidden" name="age" id="ageInner">
                    <label>Your age</label>
                </div>
                <div class="borderForm"></div>
                <div class="checkBtnWrap"  id="typemerch">
                    <select>
                        <option>Cream</option>
                        <option>mask</option>
                        <option>means for washing</option>
                        <option>mesopreparations</option>
                    </select>
                    <label>Choose vehicle type</label>
                </div>
            </div>
            <div class="sec11WrapBottom animated">
                <div class="borderFormGor"></div>
                <div class="radioWrapBot" id="typeskin">
                    <div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Dry</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Normal</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Oily</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Combined</p>
                            </div>
                        </div>

                    </div>
                    <label>Skin condition</label>
                </div>
                <div class="borderForm"></div>
                <div class="radioWrapBot" id="typeaction">
                    <div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>Humidification</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>Purification</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>Skin nutrition</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>comprehensive care</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>anti-age</p>
                            </div>
                        </div>
                    </div>
                    <label>What would you like to receive?</label>
                </div>
                <button class="btnStyle">pick up</button>
            </div>
        </form>
        <div class="absoluteBottom">
            <h3>the effect is guaranteed!</h3>
            <img src="images/section11/arrow.png">
        </div>
    </div>
</section>
<!----------------------------------SECTION12-------------------------------------------->
<section class="section12">
    <div>
        <div class="sliderSec12">

        </div>
        <div class="afterCos">
            <div>
                <?
                $path_male = $_SERVER['DOCUMENT_ROOT'] . '/images/faces/male/';
                $path_female = $_SERVER['DOCUMENT_ROOT'] . '/images/faces/female/';
                $photo_faces = array();
                if (file_exists($path_female)) {
                    $photo_female = scandir($path_female);
                    foreach ($photo_female as $value) {
                        if ($value != '.' and $value != '..')
                            $photo_faces[] = array('data' => 'female', 'path' => 'images/faces/female/' . $value);
                    }   
                }

                if (file_exists($path_male)) {
                    $photo_male = scandir($path_male);
                    foreach ($photo_male as $value) {
                        if ($value != '.' and $value != '..')
                            $photo_faces[] = array('data' => 'male', 'path' => 'images/faces/male/' . $value);
                    }
                }
                //первая картинка
                $img = array_shift($photo_faces);
                print '<img id="girl" data-face="' . $img['data'] . '" src="' . $img['path'] . '">';
                unset($photo_faces[0]);
                ?>
                <div class="hide-foto" style="display: none" id="calcfoto">
                    <?
                    foreach ($photo_faces as $img) {
                        print '<img data-face="' . $img['data'] . '" src="' . $img['path'] . '">';
                    }
                    ?>
                </div>
            </div>
            <div class="sec12H6Wrap">
                <div>
                    <h6>after peeling</h6>
                </div>
                <div>
                    <h6>8 hours after the application of funds GI BEAUTY</h6>
                </div>
            </div>

        </div>
    </div>
</section>
<!----------------------------------SECTION13-------------------------------------------->
<section class="section13">
    <div class="animationH2">
        <h2>News feed</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <a href="<?=  JRoute::_('/index.php?option=com_k2&view=itemlist&layout=category&task=category&id=3')?>" class="allNews">All news</a>
        <div class="sliderSec13">
            <?
            $position = 'index_news';
            echo $renderer->render($position, $options, null);
            ?>
        </div>

        <!--            <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>11/01</h2>
                                <h6>2015</h6>
                                <p>5 простых утренних процедур для здоровья 5 простых утренних процедур для здоровья 5 простых утренних процедур</p>
                            </div>
                            <div>
                                <img src="images/section13/img1Sec13.png">
                            </div>
                        </a>
                    </div>
                    <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>15/01</h2>
                                <h6>2015</h6>
                                <p>Возрастные изменения кожи</p>
                            </div>
                            <div>
                                <img src="images/section13/img2Sec13.png">
                            </div>
                        </a>
                    </div>
                    <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>11/01</h2>
                                <h6>2016</h6>
                                <p>5 простых утренних процедур для здоровья</p>
                            </div>
                            <div>
                                <img src="images/section13/img1Sec13.png">
                            </div>
                        </a>
                    </div>
                    <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>11/01</h2>
                                <h6>2016</h6>
                                <p>Возрастные изменения кожи</p>
                            </div>
                            <div>
                                <img src="images/section13/img2Sec13.png">
                            </div>
                        </a>
                    </div>-->
        <form class="sec13formWrap" data-target="news_form">
            <h3>Subscribe to news</h3>
            <p class="forNameForm">Subscribe to news</p>
            <div class="inputStyle">
                <input type="text" name="email" class="usermail"  onclick="yaCounter35741200.reachGoal('news_mail'); return true;">
                <span></span>
                <label>Your email</label>
            </div>
            <button>
                <i class="fa fa-envelope-o"></i>
            </button>
        </form>
    </div>
</section>
<!----------------------------------SECTION14-------------------------------------------->
<section class="section14">
    <div class="animationH2">
        <h2>Contact Information:</h2>
        <span></span>
        <span></span>
    </div>
    <div class="top-contacts-belt">
        <div>
            <i class="fa fa-map-marker"></i>
            <?
            $position = 'bottom_adress';
            echo $renderer->render($position, $options, null);
            ?>
        </div>
        <div>
            <i class="fa fa-phone"></i>
            <?
            $position = 'bottom_phone';
            echo $renderer->render($position, $options, null);
            ?>
        </div>
        <div>
            <i class="fa fa-envelope mailI"></i>
            <a href="mailto:<?= JFactory::getMailer()->From; ?>" onclick="yaCounter35741200.reachGoal('mail_index'); return true;"><?= JFactory::getMailer()->From; ?></a>
        </div>
    </div>
    <div id="mapWrap"></div>
</section>
