



jQuery(function ($) {
    $.validator.addMethod(
            'regexp',
            function (value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );

    $.validator.addClassRules({
        userphone: {
            required: true,
            minlength: 15,
            regexp: '[^_]+$'
        },
        usermail: {
            email: true
        }
    });
    var numberSlidesToShow, numberElements, widthCurrent;
    var widthSpanTotal = $('.slider-indicator').width();

    $(window).load(function () {
        $('.calc form button').click(function () {
            calcMainPage.monitoring();
            return false;
        })

        function loadpage() {
            var windowWidth = document.documentElement.clientWidth;
            if ($('.product-slider').length != 0) {
                $('.product-slider').bxSlider({
                    mode: 'fade',
                    easing: 'ease-in-out',
                    pager: 'true',
                    tickerHover: 'true'
                });
            }
//            if (windowWidth <= 1560) {
//                $('.sliderSec2-1').slick({
//                    slidesToShow: 3,
//                    slidesToScroll: 2,
//                    infinite: true,
//                    arrows: true
//                })
//            }
            if (windowWidth <= 1460) {
                $('.sliderSec8').slick({
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 4000
                });
                $('.sliderSec13').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    arrows: true,
                    autoplaySpeed: 4000,
                    autoplay: true
                });
//                $('.sliderSec12').slick({
//                    slidesToShow: 1,
//                    slidesToScroll: 1,
//                    infinite: true,
//                    arrows: true
//                })
            }
            if (windowWidth <= 1460 && windowWidth > 1360) {
                $('.sliderSec5').slick({
                    slidesToShow: 4,
                    slidesToScroll: 4,
                    infinite: false,
                    arrows: true,
                    swipe: false
                });
                $('.slider-indicator span').css('width', '25%');
                $('.sliderSec5 .slick-prev').click(function () {
                    widthCurrent = $('.slider-indicator span')[0].style.width;
                    widthCurrent = parseInt(widthCurrent.split("%")[0])
                    if (widthCurrent - 25 > 0) {
                        $('.slider-indicator span').width(widthCurrent - 25 + '%')
                    }
                })
                $('.sliderSec5 .slick-next').click(function () {
                    widthCurrent = $('.slider-indicator span')[0].style.width;
                    widthCurrent = parseInt(widthCurrent.split("%")[0])
                    if (widthCurrent + 25 <= 100) {
                        $('.slider-indicator span').width(widthCurrent + 25 + "%")
                    }
                })

            }
            if (windowWidth <= 1360) {
                $('.sliderSec5').slick({
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: false,
                    arrows: true,
                    swipe: false
                });
                $('.slider-indicator span').css('width', '20%');
                $('.sliderSec5 .slick-prev').click(function () {
                    widthCurrent = $('.slider-indicator span')[0].style.width;
                    widthCurrent = parseInt(widthCurrent.split("%")[0])
                    if (widthCurrent - 20 > 0) {
                        $('.slider-indicator span').width(widthCurrent - 20 + '%')
                    }
                })
                $('.sliderSec5 .slick-next').click(function () {
                    widthCurrent = $('.slider-indicator span')[0].style.width;
                    widthCurrent = parseInt(widthCurrent.split("%")[0])
                    if (widthCurrent + 20 <= 100) {
                        $('.slider-indicator span').width(widthCurrent + 20 + "%")
                    }
                })
            }
            if (windowWidth > 1460) {
                $('.sliderSec5').slick({
                    slidesToShow: 5,
                    slidesToScroll: 5,
                    infinite: false,
                    arrows: true,
                    swipe: false
                })
                $('.slider-indicator span').css('width', '33.3333%');
                $('.sliderSec5 .slick-prev').click(function () {
                    widthCurrent = $('.slider-indicator span')[0].style.width;
                    widthCurrent = parseInt(widthCurrent.split("%")[0])
                    if (widthCurrent - 33.3333 > 0) {
                        $('.slider-indicator span').width(widthCurrent - 33.3333 + '%')
                    }
                })
                $('.sliderSec5 .slick-next').click(function () {
                    widthCurrent = $('.slider-indicator span')[0].style.width;
                    widthCurrent = parseInt(widthCurrent.split("%")[0])
                    if (widthCurrent + 33.3333 <= 100) {
                        console.log(widthCurrent)
                        $('.slider-indicator span').width(widthCurrent + 33.3333 + "%")
                    }
                })
                $('.sliderSec8').slick({
                    slidesToShow: 4,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 4000
                })
                $('.sliderSec13').slick({
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    infinite: true,
                    arrows: true,
                    autoplaySpeed: 4000,
                    autoplay: true
                })
//                $('.sliderSec12').slick({
//                    slidesToShow: 2,
//                    slidesToScroll: 2,
//                    infinite: true,
//                    arrows: true
//                })
            }
//            if (windowWidth > 1560) {
//                $('.sliderSec2-1').slick({
//                    slidesToShow: 4,
//                    slidesToScroll: 2,
//                    infinite: true,
//                    arrows: true
//                })
//            }
            setTimeout(function () {
                $('#preloader').addClass("animated fadeOut")
                setTimeout(function () {
                    $('#preloader').css("display", "none");
                    $('.girlAnimation').css('opacity', '1')
                    // $('.headerDown').css('background-color','#C8EDFD');
                    $('.headerAnim3').css({'transform': 'translateX(0%) rotate(13deg)', 'opacity': '1'});
                    $('.headerAnim2').css({'transform': 'translateX(0%) rotate(29deg)', 'opacity': '1'});
                    $('.headerAnim1').css({'transform': 'translateX(0%) rotate(16deg)', 'opacity': '1'});
                    $('.headerAnim4').css({'transform': 'translateX(0%) rotate(-16deg)', 'opacity': '1'});
                    $('.headerAnim5').css({'transform': 'translateX(0%) rotate(-9deg)', 'opacity': '1'});
                    $('#fixedBtn').addClass('bounceInRight').css('opacity', '1');
                    $('.headerFormWrap span').eq(0).css('width', '11px');
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(1).css('height', '100%');
                    }, 100)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(2).css('width', '100%');
                    }, 400)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(3).css('height', '100%');
                    }, 1300)
                    setTimeout(function () {
                        $('.headerFormWrap span').eq(4).css('width', '277px');
                    }, 1600)
                    setTimeout(function () {
                        $('#fixedBtn').removeClass('bounceInRight')
                    }, 900)

                }, 500)
            }, 300)

        }
        loadpage();
    });
    $('#fixedBtn').click(function () {
        $('#fixedBtn').addClass('rubberBand');
        setTimeout(function () {
            $('#fixedBtn').removeClass('rubberBand');
        }, 700)
    })

    /*--------------------------btn-animation------------------------------*/
    $('.menuTable').mouseover(function () {
        $('.fa-bars').addClass('jello');
        setTimeout(function () {
            $('.fa-bars').removeClass('jello');
        }, 700)
    })
    $('.basket').mouseover(function () {
        $('.fa-shopping-bag').addClass('jello');
        setTimeout(function () {
            $('.fa-shopping-bag').removeClass('jello');
        }, 700)
    })
    /*--------------------------INPUT ANIMATION-----------------------------*/
    $('.headerFormWrap input').hover(function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    }, function () {
        if ($(this).val().length > 0) {
            $(this).closest('.headerFormWrap').find('form').addClass('inputActive');

        }
        else if ($(this).val().length == 0) {
            $(this).closest('.headerFormWrap').find('form').removeClass('inputActive');
        }
    })
    /*-----------------------ANIMATION NUMBER--------------------------*/
    var flag = true;
    var flag2 = true;
    var flag3 = true;
    var flag4 = true;
    /*-----------------------SCROLL ANIMATION-------------------------------*/
    $(window).scroll(function () {
        var heightWindow = document.documentElement.clientHeight / 1.5;
        if ($('.section1').length != 0) {
            if ($(this).scrollTop() >= $('.section1').offset().top - heightWindow) {
                $('.section1 span').addClass('animatedSpan');
            }
        }
        if ($('.section2').length != 0) {
            if ($(this).scrollTop() >= $('.section2').offset().top - heightWindow) {
                $('.sec2TitleWrap').addClass('bounceInDown').css('opacity', '1');
                if (flag == true) {
                    setTimeout(function () {
                        $('.womenCat').addClass('rotateInDownRight').css('opacity', '1');
                        $('.manCat').addClass('rotateInDownLeft').css('opacity', '1');
                        flag = false;
                        setTimeout(function () {
                            $('.womenCat').removeClass('rotateInDownRight');
                            $('.manCat').removeClass('rotateInDownLeft');

                        }, 1000)

                    }, 200)
                }
            }
        }
        if ($('.section3').length != 0) {
            if ($(this).scrollTop() >= $('.section3').offset().top - heightWindow) {
                $('.section3>div:nth-of-type(1) span').addClass('animatedSpan');
                $('.sec3Anim1').addClass('bounceInDown').css('opacity', '1');
                setTimeout(function () {
                    $('.sec3Anim2').addClass('bounceInDown').css('opacity', '1');
                }, 200)
                setTimeout(function () {
                    $('.sec3Anim3').addClass('bounceInDown').css('opacity', '1');
                }, 400)
                setTimeout(function () {
                    $('.sec3Anim4').addClass('bounceInDown').css('opacity', '1');
                }, 600)
            }
        }
        if ($('.section4').length != 0) {
            if ($(this).scrollTop() >= $('.section4').offset().top - heightWindow) {
                $('.sec4Block1 span').addClass('activeSpan');
                setTimeout(function () {
                    $('.sec4Block2 span').addClass('activeSpan');
                }, 200)
                setTimeout(function () {
                    $('.sec4Block3 span').addClass('activeSpan');
                }, 400)
                setTimeout(function () {
                    $('.sec4Block4 span').addClass('activeSpan');
                }, 600)
            }
        }
        if ($('.section3').length != 0) {
            if ($(this).scrollTop() > $('.section3').offset().top - heightWindow && flag2 === true) {
                $('.sec3Wrap p span').each(function () {
                    var spanValue = parseInt($(this).attr('data-value'), 10);
                    var counterI = 0;
                    var animNumber = window.setInterval(function () {
                        counterI += 1;
                        $('.sec3Wrap p span[data-value="' + spanValue + '"]').text(counterI)
                        if (counterI >= spanValue) {
                            clearInterval(animNumber)
                        }
                    }, 25)

                })
                flag2 = false;

            }
        }
        if ($('.sec6WrapInline').length != 0) {
            if ($(this).scrollTop() >= $('.sec6WrapInline').offset().top - heightWindow * 1.2) {
                $('.sec6Anim1').addClass('fadeInDown').css('opacity', '1');
                setTimeout(function () {
                    $('.sec6Anim2').addClass('fadeInUp').css('opacity', '1');
                }, 200)
                setTimeout(function () {
                    $('.sec6Anim3').addClass('fadeInDown').css('opacity', '1');
                }, 400)
                setTimeout(function () {
                    $('.sec6Anim4').addClass('fadeInUp').css('opacity', '1');
                }, 600)
                setTimeout(function () {
                    $('.sec6Anim5').addClass('fadeInDown').css('opacity', '1');
                }, 800)
            }
        }
        if ($('.sec6AnimWrap').length != 0) {
            if ($(this).scrollTop() >= $('.sec6AnimWrap').offset().top - heightWindow) {
                $('.sec6Anim1-1').addClass('fadeInRight').css('opacity', '1');
                setTimeout(function () {
                    $('.sec6Anim2-1').addClass('fadeInRight').css('opacity', '1');
                }, 300)
                setTimeout(function () {
                    $('.sec6Anim3-1').addClass('fadeInRight').css('opacity', '1');
                }, 600)
            }
        }


        if ($('.section7').length != 0) {
            if ($(this).scrollTop() >= $('.section7').offset().top - heightWindow) {
                if (flag3 == true) {
                    setTimeout(function () {
                        $('.sec7Anim1').addClass('rotateInDownLeft').css('opacity', '1');
                        $('.sec7Anim2').addClass('rotateInDownRight').css('opacity', '1');
                    }, 200)
                    flag3 = false;
                }

            }
        }
        if ($('.sec10Wrap').length != 0) {
            if ($(this).scrollTop() >= $('.sec10Wrap').offset().top - heightWindow) {
                $('.sec10Anim1-1').addClass('flipInY').css('opacity', '1');
                setTimeout(function () {
                    $('.sec10Anim2-1').addClass('flipInY').css('opacity', '1');
                }, 200)
                setTimeout(function () {
                    $('.sec10Anim3-1').addClass('flipInY').css('opacity', '1');
                }, 400)
                setTimeout(function () {
                    $('.sec10Anim4-1').addClass('flipInY').css('opacity', '1');
                }, 600)
            }
        }

        if ($('.section6').length != 0) {
            if ($(this).scrollTop() >= $('.section6').offset().top - heightWindow) {
                $('.section6>div:nth-of-type(1) span').addClass('animatedSpan');
            }
        }
        if ($('.sec6Wrap').length != 0) {
            if ($(this).scrollTop() >= $('.sec6Wrap').offset().top - heightWindow) {
                $('.sec6Wrap span').addClass('animatedSpan');
            }
        }
        if ($('.section8').length != 0) {
            if ($(this).scrollTop() >= $('.section8').offset().top - heightWindow) {
                $('.section8>div:nth-of-type(1) span').addClass('animatedSpan');
            }
        }
        if ($('.section9').length != 0) {
            if ($(this).scrollTop() >= $('.section9').offset().top - heightWindow) {
                $('.section9>div:nth-of-type(1) span').addClass('animatedSpan');
            }
        }
        if ($('.section10').length != 0) {
            if ($(this).scrollTop() >= $('.section10').offset().top - heightWindow) {
                $('.section10>div:nth-of-type(1) span').addClass('animatedSpan');
            }
        }
        if ($('.section13').length != 0) {
            if ($(this).scrollTop() >= $('.section13').offset().top - heightWindow) {
                $('.section13>div:nth-of-type(1) span').addClass('animatedSpan');
            }
        }
        if ($('.section14').length != 0) {
            if ($(this).scrollTop() >= $('.section14').offset().top - heightWindow) {
                $('.section14>div:nth-of-type(1) span').addClass('animatedSpan');
            }
        }
        if ($('.section2').length != 0) {
            if ($(this).scrollTop() > $('.section1').offset().top - 100) {
                if (flag4 == true) {
                    $('.headerDown').css('margin-top', '153px');
                    $('.headerUp').addClass('headerUpFixed fadeInDown');
                    $('.headerMiddle').addClass('headerMiddleFixed fadeInDown');
                    flag4 = false;
                }
            }
        }

        if ($('.section2').length != 0) {
            if ($(this).scrollTop() < $('.section1').offset().top - 100) {
                if (flag4 == false) {
                    $('.headerMiddle').removeClass('fadeInDown').addClass('fadeOutUp');
                    $('.headerUp').removeClass('fadeInDown').addClass('fadeOutUp');
                    setTimeout(function () {
                        $('.headerDown').css('margin-top', '0')
                        $('.headerUp').removeClass('headerUpFixed fadeOutUp').css('opacity', '1');
                        $('.headerMiddle').removeClass('headerMiddleFixed fadeOutUp').css('opacity', '1');
                    }, 600)
                    flag4 = true;
                }


            }
        }
    });
    $('.sliderWrap').slick({
        infinite: true,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 3000
    })

    /*---------------------индикатор слайдера--------------------------*/
    /*-----------------------------------------------*/

    $('.slider-range').slider({
        range: "min",
        min: 18,
        max: 80,
        value: 35,
        slide: function (event, ui) {
            $("#ageInner").val(ui.value);
            $(".ui-slider-range").attr('data-attribute', ui.value);
            $(this).closest('.checkBtnWrap>div').addClass('fixUiSlider')
        }
    })
    $("#ageInner").val(35);
    $(".ui-slider-range").attr('data-attribute', '35');


    $('input[name="gender"]').click(function () {
        $('.sec11WrapBottom').addClass('bounceIn').css({'display': 'block', 'opacity': '1'})
    })
    $('.textAreaStyle textarea').blur(function () {
        if ($('.textAreaStyle textarea').val().length > 0) {
            $('.textAreaStyle textarea').addClass('textareaActive');
        }
        if ($('.textAreaStyle textarea').val().length == 0) {
            $('.textAreaStyle textarea').removeClass('textareaActive');
        }
    })
    /*-------------------------------------------------*/
    $('.womenCat').click(function () {
        $(this).css({
            "transform": "scale(2)",
            "-o-transform": "scale(2)",
            "-moz-transform": "scale(2)",
            "-ms-transform": "scale(2)",
            "-webkit-transform": "scale(2)",
            "opacity": "0"
        })
        setTimeout(function () {
            $('.maleWrap>div').css('display', 'none');
        }, 500)
        $('.manCat').css('opacity', '0');
        $('#forLady').addClass('activeGender');
        $('.catalogSec2').css({'opacity': '1', 'z-index': '50'});
        $('.sliderSec2First').css({'opacity': '1', 'z-index': '51'})
    });
    $('.manCat').click(function () {
        $(this).css({
            "transform": "scale(2)",
            "-o-transform": "scale(2)",
            "-moz-transform": "scale(2)",
            "-ms-transform": "scale(2)",
            "-webkit-transform": "scale(2)",
            "opacity": "0"
        })
        setTimeout(function () {
            $('.maleWrap>div').css('display', 'none');
        }, 500)
        $('.womenCat').css('opacity', '0');
        $('#forGentleman').addClass('activeGender');
        $('.catalogSec2').css({'opacity': '1', 'z-index': '50'});
        $('.sliderSec2Second').css({'opacity': '1', 'z-index': '51'})
    });
    $('#forLady').click(function () {
        $('#forGentleman').removeClass('activeGender');
        $('#forLady').addClass('activeGender');
        $('.sliderSec2Second').css({'opacity': '0', 'z-index': '50'})
        $('.sliderSec2First').css({'opacity': '1', 'z-index': '51'})
    })
    $('.popUpCall').click(function () {
        $('#popUpCall').find('.nameInner').text($(this).attr('data-name-form'));
    })
    $('.popUpCall').fancybox({
        'showCloseButton': false,
        'closeBtn': false,
        helpers: {
            overlay: {
                locked: false
            }
        }
    });
    $('.closeFancy').click(function () {
        $.fancybox.close();
    });
    $('#forGentleman').click(function () {
        $('#forLady').removeClass('activeGender')
        $('#forGentleman').addClass('activeGender')
        $('.sliderSec2First').css({'opacity': '0', 'z-index': '50'})
        $('.sliderSec2Second').css({'opacity': '1', 'z-index': '51'})
    });
    if ($('#mapWrap').length != 0) {
        (function () {
            var API = google.maps;
            var $container = $('#mapWrap');
            if ($container.length == 0)
                return;
            var mapPosition = new API.LatLng(55.776597, 37.542144);
            var markerPosition = new API.LatLng(55.776597, 37.542144);
            var map = new API.Map($container[0], {
                center: mapPosition,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
                panControl: false,
                zoom: 14
            });
            var marker = new API.Marker({
                map: map, position: markerPosition, icon: 'http://gi-beauty.ru/images/marker.png'
            });
            var iw = new google.maps.InfoWindow({
                content: '<div style="text-align: left; height: 45px; font-size: 18px; position:relative; z-index:1;">г.Реутов<br>ул.Безымянная дом 0</div>'
            });
        })();
    }

    $(document).ready(function () {
        var formSend;
        formSend = function (formObject, namePopUp, productName, target) {
            var fieldForm, sendAjaxAndResetForm, fieldArr, newFieldArr = [], c, nameObg = {}, taskMail = 'sendMail';
            fieldArr = formObject.find(':input,textarea,select').serializeArray();
            for (c = 0; c < fieldArr.length; c++) {
                if (fieldArr[c].value.length > 0) {
                    newFieldArr.push(fieldArr[c])
                }
            }
            if (productName) {
                nameObg.name = "product_name";
                nameObg.value = productName;
                newFieldArr.push(nameObg);
            }
            if ($(formObject).hasClass('subscribeForm')) {
                taskMail = 'sendMailShares';
            }
            $.ajax({
                type: "POST",
                url: 'index.php?option=com_ajax&plugin=ajax&format=json',
                dataType: 'json',
                data: {
                    subject: namePopUp,
                    formData: newFieldArr,
                    task: taskMail
                }
            }).done(function () {
                $.fancybox.close();
                $('.close-popup,.popupoverlay').click();
                $('.merchendise-count').find('input').attr('value', '1')
                $('#thanksPopUp').css('display', 'block')
                setTimeout(function () {
                    $('#thanksPopUp').css('opacity', '1')
                    $('form').trigger('reset');
                }, 300);
                setTimeout(function () {
                    $('#thanksPopUp').css('opacity', '0')
                }, 2500)
                setTimeout(function () {
                    $('#thanksPopUp').css('display', 'none')
                }, 2800);
                yaCounter35741200.reachGoal(target);
                return true;
            })
        }
        $('form:not(.ajaxStop)').each(function () {
            $(this).validate({
                errorPlacement: function (error, element) {
                    $('.error').next().addClass('errorVal');
                    $('.error').closest('.headerFormWrap').find('.formSpan').addClass('errorValHeader')
                    error.remove();
                },
                success: function () {
                    $('.valid').next().removeClass('errorVal');
                    $('.valid').closest('.headerFormWrap').find('.formSpan').removeClass('errorValHeader')
                },
                submitHandler: function (form) {

                    var object = $(form);
                    var namePopUp = $(object).find('.forNameForm').text();
                    var productName = $(object).find('.nameProductLink').text();
                    var target = object.attr('data-target');
                    formSend(object, namePopUp, productName, target);
                }

            });
        });
        $('.autentification-tab>li').click(function () {
            var index = $(this).index();
            $('.autentification-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.autentification-tab-item>li').removeClass('active').eq(index).addClass('active');
        })
        $('.register-type>li').click(function () {
            var index = $(this).index();
            $('.register-type>li').removeClass('active');
            $(this).addClass('active');
            $('.register-item>li').removeClass('active').eq(index).addClass('active');
        })

        $('.comment-button').click(function () {
            $(this).closest('.vk-comment').find('.comment-body').slideToggle(600);
        });
        $('.order-item .item-heading').click(function () {
            $('.order-item').find('.item-body').not($(this).closest('.order-item').find('.item-body')).slideUp(600);
            $(this).closest('.order-item').find('.item-body').slideToggle(600);
            $('.openorder').not($(this).find('.openorder')).addClass('fa-chevron-down').removeClass('fa-chevron-up')
            $(this).find('.openorder').toggleClass('fa-chevron-up').toggleClass('fa-chevron-down')
        });

        $('.write-button').click(function () {
            $(this).closest('.form-item').find('input').removeAttr('readonly');
        });
        $('.lk-tab li').click(function () {
            var index = $(this).index();
            $('.lk-tab li').removeClass('active');
            $(this).addClass('active');
            $('.lk-tab-body li').removeClass('active').eq(index).addClass('active');
        });
        $('.prod-main-tab>li').click(function () {
            var index = $(this).index();
            $('.prod-main-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.prod-main-tab-body>li').removeClass('active').eq(index).addClass('active');
        });
        $('.prod-inner-tab>li').click(function () {
            var index = $(this).index();
            $('.prod-inner-tab>li').removeClass('active');
            $(this).addClass('active');
            $('.prod-inner-tab-body>li').removeClass('active').eq(index).addClass('active');
        });

        $('.menuTable').click(function (e) {
            e.preventDefault();
            $('.headerMiddleinner').slideToggle(600);
        });
        $('.mainWrapper').click(function () {
            $('.headerMiddleinner').slideUp(600);
        })
        function openCommonPopup(id, element) {
            $('.popupoverlay').fadeIn(400);
            if (id == 'by_inclick') {
                var proid = $(element).closest('.byin').attr('data-prod-id');
                for (var i = 0; i < prodArray.length; i++) {
                    if (prodArray[i].virtuemart_product_id == proid) {
                        $('#by_inclick table tr:nth-child(1) td:nth-child(3),forNameForm').text(prodArray[i].product_name);
                        $('#prodname').val(prodArray[i].product_name);
                        $('#by_inclick table td .img-cont').find('img').attr('src', prodArray[i].images[0].file_url);
                        $('#by_inclick table tr:nth-child(2) td:nth-child(2) .price-forone,#by_inclick table tr:nth-child(4) td:nth-child(2) .total-pr').text(prodArray[i].product_price);
                        $('#by_inclick #price-forone,#by_inclick #total-pr').val(prodArray[i].product_price);
                        break;
                    }
                }
                $('#' + id).closest('.commonpopup').fadeIn(400);
            } else {
                $('#' + id).closest('.commonpopup').fadeIn(400);
            }

        }

        $('.openpopupreg').click(function () {
            $('.popupoverlay,.autentification-popup').fadeIn(400);
        });

        $(document).on('click', '.opencommonpopup', function () {
            var id = $(this).attr('data-id');
            var element = $(this);
            openCommonPopup(id, element);
        });

        $('.close-popup,.popupoverlay,.closePopUpAdded').click(function () {
            $('.popupoverlay,.commonpopup,.autentification-popup').fadeOut(400);
            $('.merchendise-count').find('input').attr('value', '1')
        })
    });
    // $('.headerSend,sec13formWrap button').click(function(e){
    //     e.preventDefault();
    // })
    /*---------------------счетчик--------------------------*/
    $('.merchendise-count span').click(function () {
        var number = $(this).parent('div').find('input').attr('value');
        if ($(this).hasClass('plus')) {
            number++;
            $(this).parent('div').find('input').attr('value', number).text(number)
            if (number >= 999) {
                number = 999;
                $(this).parent('div').find('input').attr('value', number).text(number)
            }
            ;
        }
        if ($(this).hasClass('minus')) {
            number--;
            $(this).parent('div').find('input').attr('value', number).text(number)
            if (number <= 1) {
                number = 1;
                $(this).parent('div').find('input').attr('value', number).text(number)
            }
        }
    });
    $(function () {

        $("body").css({padding: 0, margin: 0});
        var f = function () {
            $(".mainWrapper").css({position: "relative"});
            var h1 = $("body").height();
            var h2 = $(window).height();
            var d = h2 - h1;
            var h = $(".mainWrapper").height() + d;
            var ruler = $("<div>").appendTo(".mainWrapper");
            h = Math.max(ruler.position().top, h);
            ruler.remove();
            $(".mainWrapper").height(h);
        };
        setInterval(f, 1000);
        $(window).resize(f);
        f();

    });
    jQuery(function ($) {
        $('input[name="phone"],.userphone').mask("+7(999)999-99-99", {placeholder: '_'});
    });
    $('.merchendise-count span:not(.inclickSpan)').click(function () {
        var numberProduct, idProduct, totalNumber, currentNumber, numberProductTotal, clickedElement, currentPrice, totalPrice, totalAllPrice;
        clickedElement = $(this)
        numberProduct = $(this).closest('.merchendise-count').find('input').val();
        idProduct = $(this).closest('tr').find('input[name="virtuemart_product_id"]').val();
        $.ajax({
            type: "POST",
            url: 'index.php?option=com_ajax&plugin=ajax&format=json',
            dataType: 'json',
            data: {
                virtuemart_product_id: idProduct,
                quantity: numberProduct,
                task: 'cartUpdate'
            },
            success: function (data) {
                if (data.data) {
                    currentPrice = clickedElement.closest('tr').find('.priceForOne').text().replace(/\s+/g, '');
                    currentPrice = Number(currentPrice);
                    totalPrice = currentPrice * numberProduct;
                    clickedElement.closest('tr').find('.priceTotal p').text(totalPrice);
                    totalAllPrice = 0
                    $('.priceTotal').each(function () {
                        totalAllPrice = totalAllPrice + Number($(this).find('p').text().replace(/\s+/g, ''));
                    })
                    totalAllPrice = totalAllPrice.toString();
                    $('.endprice span').text(totalAllPrice);
                    $('#endprice_hidden').val(totalAllPrice);
                    totalPrice = totalPrice.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
                    clickedElement.closest('tr').find('.totalPrice').text(totalPrice)
                    numberProductTotal = -1;
                    $('.number').each(function () {
                        numberProductTotal = numberProductTotal + Number($(this).val());
                    })
                    $('.basket').find('p span').text(numberProductTotal);
                }

            }
        })
    })
    $('.delete').click(function () {
        var idProduct, currentDelete, numberProductTotal, totalPrice, totalAllPrice;
        currentDelete = $(this)
        idProduct = $(this).next().val();
        $.ajax({
            type: "POST",
            url: 'index.php?option=com_ajax&plugin=ajax&format=json',
            dataType: 'json',
            data: {
                virtuemart_product_id: idProduct,
                task: 'cartDelete'
            },
            success: function (data) {
                if (data.data) {
                    totalPrice = currentDelete.closest('tr').find('.priceTotal p').text().replace(/\s+/g, '');
                    totalPrice = Number(totalPrice);
                    totalAllPrice = $('.endprice').text().replace(/\s+/g, '');
                    totalAllPrice = Number(totalAllPrice) - totalPrice;
                    totalAllPrice = totalAllPrice.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
                    $('.endprice span').text(totalAllPrice);
                    currentDelete.closest('tr').remove();
                    numberProductTotal = -1;
                    $('.number').each(function () {
                        numberProductTotal = numberProductTotal + Number($(this).val());
                    })
                    $('.basket').find('p span').text(numberProductTotal);
                }
            }
        })
    })
    var currentNumberInBasket;
    $('.incart:not(.vmAddCartSlider)').click(function () {
        currentNumberInBasket = Number($('.basket p span').text());
        currentNumberInBasket = currentNumberInBasket + Number($(this).prev().val());
        $('.basket p span').text(currentNumberInBasket)
    });
    var inClickVal
    $('.inclickSpan').click(function () {
        inClickVal = Number($(this).closest('.merchendise-count').find('input').val());
        $('.total-pr').text(inClickVal * Number($('.price-forone').text()))
    })
    $('input:not(.userphone),textarea').keypress(function(key){
        
        if ($(this).val().length > 0) {
            $(this).val($(this).val()+String.fromCharCode(key.keyCode).toLowerCase())
        }
        else{
            $(this).val($(this).val()+String.fromCharCode(key.keyCode))
        }
        return false;
    })

});
