/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(function($) {
    $.validator.addMethod(
            'regexp',
            function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
            );

    $.validator.addClassRules({
        userphone: {
            required: true,
            minlength: 15,
            regexp: '[^_]+$'
        },
        usermail: {
            email: true
        },
        required: {
            required: true
        }
    });

    $(document).ready(function() {
        $('form').each(function() {
            $(this).validate({
                submitHandler: function(form) {

                    var object = $(form);
                    ajaxSubmit(object);
                }

            });
        });
        $(".userphone").inputmask("mask", {"mask": "8(999) 999-99-99"}); //specifying fn & options

    });
});

