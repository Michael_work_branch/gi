/* 
 GI product plugin
 */

(function($) {
    var sliderSec12, sliderSec2_1;
    function poductpluginInit() {
    	console.log('asdasdasd')
        if ($('.sliderSec12').length != 0) {
            sliderSec12 = $('.sliderSec12').productplugin(
                    {
                        'filter': [],
                        'itemTemplate': '<div class="slidesSec12"><a><div class="imgCont"></div><p></p></a></div>',
                        'productContainer': '.slidesSec12',
                        'imageContainer': '.imgCont',
                        onRender: function(object) {
                        	console.log('asdasdasd')
                            var windowWidth = $(window).width();
                            if (windowWidth <= 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
        }

        /*********/
        if ($('.sliderSec2-1').length != 0) {
            $('.sliderSec2-1.sliderSec2First').productplugin(
                    {
                        'filter': [{"id_group": "10", "value": "1"}],
                        'itemTemplate': ' <div class="slidesSec2-1"><a><div class="slidesSec2ImgWrap"></div></a><div class="nameSec2Wrap"><div><a><p class="prdName"></p></a></div></div><div class="btnStyle"><div class="sec2Bottom"><h5><span class="prdPrice">9 999 999</span><i class="fa fa-rub"></i></h5><div class="buyInOneClick"><i class="fa fa-hand-o-up"></i><h6>купить<br>в 1 клик</h6></div><div class="basketCatalog"><i class="fa fa-shopping-bag"></i></div></div></div></div>',
                        'productContainer': '.slidesSec2-1',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {
                            var windowWidth = $(window).width();
                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2First').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2First').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
            $('.sliderSec2-1.sliderSec2Second').productplugin(
                    {
                        'filter': [{"id_group": "10", "value": "2"}],
                        'productContainer': '.slidesSec2-1',
                        'itemTemplate': ' <div class="slidesSec2-1"><a><div class="slidesSec2ImgWrap"></div></a><div class="nameSec2Wrap"><div><a><p class="prdName"></p></a></div></div><div class="btnStyle"><div class="sec2Bottom"><h5><span class="prdPrice">9 999 999</span><i class="fa fa-rub"></i></h5><div class="buyInOneClick"><i class="fa fa-hand-o-up"></i><h6>купить<br>в 1 клик</h6></div><div class="basketCatalog"><i class="fa fa-shopping-bag"></i></div></div></div></div>',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {
                            var windowWidth = $(window).width();
                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2Second').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2Second').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
        }
    }
    ;
    function calcInit() {
        if ($('.section11 form').length != 0) {
            calcMainPage = $('.section11 form').calcData({
                containers: {
                    'calcid_10': '#sex',
                    'calcid_12': '#typemerch',
                    'calcid_13': '#typeskin',
                    'calcid_14': '#typeaction',
                    'calcid_15': '#age',
                }
            }, sliderSec12);
        }
    }
    var inArray = Array.prototype.indexOf ?
            function(arr, val) {
                return arr.indexOf(val) != -1
            } :
            function(arr, val) {
                var i = arr.length
                while (i--) {
                    if (a[i] === val)
                        return true
                }
                return false
            }


    var prodArray, customsArray;
    getProdArray = function() {
    	console.log('ajaxDone')
        $.ajax({
            type: "POST",
            url: 'index.php?option=com_ajax&plugin=ajax&format=json',
            dataType: 'json',
            data: {
                task: 'getProducts'
            }
        }).done(function(data) {
        	console.log('ajax')
            prodArray = data.data[0].prods;
            customsArray = data.data[0].customs;
            console.log(customsArray);
            console.log(prodArray);
            poductpluginInit();
            calcInit();
        });
    }
    getProdArray();
    $.fn.productplugin = function(options) {

        options = $.extend({
            'filter': [],
            'productContainer': '.productItem',
            'imageContainer': '.imageContainer',
            'productNameContainer': 'p',
            'itemTemplate': '<div class="productItem"><a><div class="imageContainer"></div><p></p></a><div class="price"></div><div class="price"></div></div>',
            'productPriseContainer': '.price',
            'productDescriptionContainer': '.productpluginDecription',
            'link': 'a',
            onRender: function() {
            }
        }, options);
        this.filtration = function(paramArray) {
            var tempArray = [];
            var subtempArray = [];
            if (paramArray.length != 0) {
                tempArray = prodArray;
                for (var i = 0; i < paramArray.length; i++) {
                    for (var j = 0; j < tempArray.length; j++) {
                        if (tempArray[j].customs) {
                            if (paramArray[i]["id_group"] in tempArray[j].customs) {
                                var tempID_GROUP = paramArray[i]["id_group"];
                                for (var l = 0; l < paramArray[i]["value"].length; l++) {
                                    if (inArray(tempArray[j].customs[tempID_GROUP], String(paramArray[i].value[l]))) {
                                        if (!(inArray(subtempArray, tempArray[j])))
                                        {
                                            subtempArray.push(tempArray[j]);
                                        }

                                    }

                                }
                            } else {
                                subtempArray.push(tempArray[j]);
                            }
                        } else {
                            subtempArray.push(tempArray[j]);
                        }
                    }
                    tempArray = subtempArray;
                    subtempArray = [];
                }
                console.log(tempArray);
                this.render(tempArray);
            }
            else {
                var renderArray = prodArray;
                console.log(renderArray);
                this.render(renderArray);
            }
        }
        this.render = function(renderArray) {
            this.html('');
            for (var k = 0; k < renderArray.length; k++) {
                this.append(options.itemTemplate);
                if (renderArray[k].images.length != 0) {
                    this.find(options.productContainer).eq(k).find(options.imageContainer).append('<img src="' + renderArray[k].images[0].file_url + '">');
                }
                this.find(options.productContainer).eq(k).find(options.link).attr({'href': '/index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' + renderArray[k].virtuemart_product_id + '&virtuemart_category_id=1'});
                this.find(options.productContainer).eq(k).find(options.productNameContainer).html(renderArray[k].product_name);
                this.find(options.productContainer).eq(k).find(options.productPriseContainer).html(renderArray[k].product_price);
                this.find(options.productContainer).eq(k).find(options.productDescriptionContainer).html(renderArray[k].product_price);
            }
            options.onRender(this);
        }
        this.filtration(options.filter);
        return this;
    };
    $.fn.calcData = function(options, linkObject) {
        options = $.extend({
            containers: {
                'calcid_10': '.vid10',
                'calcid_12': '.vid12',
                'calcid_13': '.vid13',
                'calcid_14': '.vid14',
                'calcid_15': '.vid15'
            }
        }, options)

        this.construct = function() {
            this.addClass('filtercalculator');
            for (var i = 0; i < customsArray.length; i++) {
                var vm_id = customsArray[i].virtuemart_custom_id;
                for (var j = 0; j < customsArray[i]['values'].length; j++) {
                    var object = options.containers['calcid_' + vm_id];
                    $(object).find('input,option').eq(j).attr('data-custom-id', customsArray[i].values[j].id);
                }
            }

        }

        this.monitoring = function() {

            var filterArray = []
            for (var key in options.containers) {
                /*********/

                if ($(options.containers[key]).find('input[type="radio"],input[type="checkbox"]').length != 0) {

                    if ($(options.containers[key]).find('input[type="radio"]:checked,input[type="checkbox"]:checked').length != 0) {
                        var array = $(options.containers[key]).find('input[type="radio"]:checked,input[type="checkbox"]:checked').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    } else {
                        var array = $(options.containers[key]).find('input[type="radio"],input[type="checkbox"]').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    }
                }
                /************/
                if ($(options.containers[key]).find('select').length != 0) {

                    if ($(options.containers[key]).find('select option:selected').length != 0) {
                        var array = $(options.containers[key]).find('select option:selected').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    } else {
                        var array = $(options.containers[key]).find('select option').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    }
                }
                if ($(options.containers[key]).find('#ageInner').length != 0) {

                    var id_group = key.split('_');
                    id_group = id_group[1];
                    var array = $(options.containers[key]).find('#ageInner').map(function() {
                        var age = $(this).val()
                        for (var k = 0; k < customsArray.length; k++) {
                            if (customsArray[k].virtuemart_custom_id == id_group) {
                                for (var l = 0; l < customsArray[k].values.length; l++) {
                                    var splitValue = customsArray[k].values[l].value.split('-');
                                    if (age > splitValue[0] && age <= splitValue[1]) {
                                        age = customsArray[k].values[l].id;
                                    }

                                }
                            }

                        }


                        return age;
                    })
                    array = array.get();
                    var id_group = key.split('_');
                    id_group = id_group[1];
                    var filter = {id_group: id_group, value: array};
                    filterArray.push(filter);
                }
                /********/
            }


            linkObject.filtration(filterArray);
        }

        this.construct();
//        this.each(function() {
//            $(window).bind('submit', this.monitoring());
//        });

        return this;
    }
})(jQuery);

