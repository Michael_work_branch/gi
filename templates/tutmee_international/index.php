<?php
defined('_JEXEC') or die;

$jinput = JFactory::getApplication()->input;
$user = JFactory::getUser();
$db = JFactory::getDbo();
$query = $db->getQuery(true);

if (($jinput->request->get('option', '', 'string') == 'com_content') && ($jinput->request->get('view', '', 'string') == 'featured'))
    $main = true;
elseif (($jinput->request->get('option', '', 'string') == 'com_k2') && ($jinput->request->get('view', '', 'string') == 'item') && ($jinput->request->get('id', '', 'int') == 3))
    $contact_page = true;
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" >
    <head>
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=1200">
        <link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
    <jdoc:include type="head" />
    <!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter35741200 = new Ya.Metrika({id:35741200, webvisor:true, clickmap:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/35741200" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?121"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick-theme.css"/> 
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/css/jquery-ui.theme.min.css">
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/media/js/productplugin.js"></script> 
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/slick-1.5.9/slick/slick.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery-ui.min.js"></script>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.maskedinput.js"></script>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/jquery.validate.min.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
    <div id="preloader">
        <div id="global">
            <div id="top" class="maskPre">
                <div class="plane"></div>
            </div>
            <div id="middle" class="maskPre">
                <div class="plane"></div>
            </div>
            <div id="bottom" class="maskPre">
                <div class="plane"></div>
            </div>
            <p><i>LOADING...</i></p>
        </div>
    </div>
    <a href="#popUpCall" id="fixedBtn" class="popUpCall animated" data-name-form="Заказать звонок" onclick="yaCounter35741200.reachGoal('call_click'); return true;">
        <div class="fixedBtnWrap animated"></div>
        <div>
            <i class="fa fa-phone"></i>
        </div>
    </a>	
    <!----------------------------------HEADER------------------------------------------------>
    <header>
        <div class="headerUp animated">
            <div>
                <div class="headerUpFirst">
                    <jdoc:include type="modules" name="top_phone" />
                </div>
                <div class="headerUpSecond">
                    <?php if ($user->guest): ?>
                        <div class="btnHeader">
                            <button class="openpopupreg"><i class="fa fa-pencil"></i> Авторизоваться на сайте</button>
                        </div>
                    <?php else: ?>
                        <div class="btnHeader">
                            <a href="<?= JRoute::_('index.php?option=com_users&view=profile', false) ?>"><i class="fa fa-key"></i> Личный кабинет</a>
                        </div>
                        <div class="btnHeader">
                            <form class="ajaxStop" action="<?= JRoute::_(htmlspecialchars(JUri::getInstance()->toString())) ?>" method="post">
                                <input type="hidden" name="option" value="com_users" />
                                <input type="hidden" name="task" value="user.logout" />
                                <input type="hidden" name="return" value="./" />
                                <?php echo JHtml::_('form.token'); ?>
                                <button class="exitProfile">
                                    <i class="fa fa-sign-out"></i> Выйти
                                </button>
                            </form>
                        </div>
                    <?php endif; ?>
                    <a class="active" href="#">Ru</a>
                    <!-- <a href="#">En</a> -->
                </div>
            </div>
        </div>
        <div class="headerMiddle animated">
            <div>
                <div class="headerMiddleFirst">
                    <div class="headerMiddleImgWrap">
                        <a href="./">
                            <img src="images/logo/logoLeft.png">
                            <img src="images/logo/logoRight.png">
                        </a>							
                    </div>
                    <a href="#" class="menuTable">
                        <i class="fa fa-bars animated"></i>
                        <p>Меню</p>
                    </a>
                </div>
                <div class="headerMiddleSecond">
                    <div class="headerFormWrap">
                        <span class="firstBorder formSpan"></span>
                        <span class="secondBorder formSpan"></span>
                        <span class="thirdBorder formSpan"></span>
                        <span class="fourthBorder formSpan"></span>
                        <span class="fifthBorder formSpan"></span>
                        <form class="subscribeForm" data-target="form_shares">
                            <p class="forNameForm">Подписаться на акции</p>
                            <div>
                                <i class="fa fa-percent"></i>
                                <p>Подписаться на акции</p>
                            </div>
                            <div class="inFormInput">
                                <input type="text" name="email" placeholder="Ваш email" class="inputAnim usermail" onclick="yaCounter35741200.reachGoal('input_shares'); return true;">
                                <button class="send headerSend">
                                    <i class="fa fa-envelope"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                    <a href="<?php echo JRoute::_('index.php?option=com_virtuemart&view=cart&lang=ru', false); ?>" class="basket">
                        <jdoc:include type="modules" name="my_cart" />
                    </a>
                </div>
            </div>
            <div class="headerMiddleinner">
                <div class="content-container">
                    <?php if ($this->countModules('mainmenu')) : ?>
                        <jdoc:include type="modules" name="mainmenu" />
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php if ($main): ?>
            <div class="headerDown">
                <span class="animHeader headerAnim1"></span>
                <span class="animHeader headerAnim2"></span>
                <span class="animHeader headerAnim3"></span>
                <span class="animHeader headerAnim4"></span>
                <span class="animHeader headerAnim5"></span>
                <div>
                    <img src="images/header/girl.png" class="girlAnimation">
                    <div class="hWrap">
                        <h1>Профессиональная</h1>
                        <h1>космецевтика</h1>
                        <h1>премиум-класса</h1>
                    </div>
                    <h3>от компании «НПО ЛИОМАТРИКС»</h3>
                    <h6>Технологии будущего применяем сегодня!</h6>
                    <p>разработка • исследование • внедрение</p>
                </div>
            </div>
        <?php endif; ?>
    </header>
    <div class="mainWrapper">
        <?php if (!$main): ?>
            <div class="content">
                <jdoc:include type="modules" name="breadcrmbs_gi" />
            <? endif; ?>
            <jdoc:include type="component" />
            <? if (!$main): ?>
            </div>
        <?php endif; ?>
    </div>
    <!----------------------------------SECTION15-------------------------------------------->
    <div class="footerWrapperAll">
        <section class="section15">
            <div>
                <h3>Технологии будущего — наше сегодня!</h3>
                <?php
                $query->select('params')
                        ->from('#__extensions')
                        ->where('extension_id = 10066');
                $socials = $db->setQuery($query)->loadResult();
                $query->clear();
                $socials = json_decode($socials);
                ?>
                <div class="linksWrap">
                    <a href="<?= $socials->fb ?>">
                        <i class="fa fa-facebook"></i>
                    </a>
                    <a href="<?= $socials->inst ?>">
                        <i class="fa fa-instagram"></i>
                    </a>
                    <a href="<?= $socials->tw ?>">
                        <i class="fa fa-twitter"></i>
                    </a>
                    <a href="<?= $socials->vk ?>">
                        <i class="fa fa-vk"></i>
                    </a>
                    <a href="<?= $socials->ok ?>">
                        <i class="fa fa-odnoklassniki"></i>
                    </a>
                    <a href="<?= $socials->pi ?>">
                        <i class="fa fa-pinterest-p"></i>
                    </a>
                    <a href="<?= $socials->in ?>">
                        <i class="fa fa-linkedin"></i>
                    </a>
                </div>
            </div>
        </section>
        <!----------------------------------FOOTER------------------------------------------------>
        <footer>
            <div class="footerUp">
                <div>
                    <a href="./">
                        <img src="images/logo/logoFooter.png">
                    </a>
                    <?php if ($this->countModules('mainmenu')) : ?>
                        <jdoc:include type="modules" name="footer_menu" />
                    <?php endif; ?>
                </div>
            </div>
            <div class="footerDown">
                <div>
                    <div>
                        <span>©</span> 
                        <p>2015 ООО «НПО Лиоматрикс»</p>
                    </div>
                    <div>
                        <a href="http://tutmee.ru/">
                            <img src="images/logo/tutmee.png">
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <div class="popupoverlay"></div>
    <div class="autentification-popup" id="popupreg">        <!-- если в URL есть якорь #popupreg отлкрывать форму при загрузке страницы-->   
        <ul class="autentification-tab">
            <li><span class="fa fa-pencil"></span>регистрация</li>
            <li class="active"><span class="fa fa-key"></span>вход</li>                           
        </ul>
        <table>
            <tr>
                <td>
                    <ul class="autentification-tab-item">
                        <li>
                            <form class="autoForm ajaxStop" action="<?php echo JRoute::_('index.php?option=com_users&task=gireg.register'); ?>" method="post">
                                <ul class="register-item">
                                    <li class="active">
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[name]" /><label>Ваше имя (обязательно)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password1]" /><label>Пароль (обязательно)</label></div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="form-item  " data-hint=""><input type="text" name="jform[email1]" /><label>Ваш электронный адрес (обязательно)</label></div>
                                            <div class="form-item  " data-hint=""><input type="password" name="jform[password2]" /><label>Подтверждение пароля(обязательно)</label></div>
                                        </fieldset>
                                        <div class="capcha-check-container">
                                            <fieldset>
                                                <div class="capcha-block">
                                                    <div class="g-recaptcha" data-sitekey="6LdqdRgTAAAAAPYF1ypIO5UDbI1SaSqlNsvrPK5p"></div>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="check-container">
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"> </label></div>
                                                        <span>Получать на email новости сервиса</span>
                                                    </div>
                                                    <div class="check-block">
                                                        <div class="check-wrapp"><input type="checkbox" /><label class="fa fa-check"></label></div>
                                                        <span>Я ознакомлен и согласен с условиями <a href="#">политики конфиденциальности</a></span>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <input type="hidden" name="option" value="com_users" />
                                        <input type="hidden" name="task" value="gireg.register" />
                                        <?php echo JHtml::_('form.token'); ?>
                                        <button>зарегистрироваться</button>
                                    </li>
                                </ul>
                            </form>
                        </li>
                        <li class="active">
                        <jdoc:include type="modules" name="login" />
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
        <div class="close-popup"></div>               
    </div>
    <div class="commonpopup">
        <div class="close-popup" ></div>
        <div id="by_inclick">
            <form action="#" data-target="oneclick_form">
                <input type="hidden" class="forNameForm">
                <input type="hidden" name="prodname" id="prodname">
                <input type="hidden" name="totalprice" id="total-pr">
                <input type="hidden" name="priceforone" id="price-forone">
                <div class="inclick-top-belt">
                    <h2 class="forNameForm">Купить в 1 клик</h2>
                    <div class="in-click-inner">                            
                        <table>
                            <tr>
                                <td rowspan="4"><a href="#" class="img-cont"><img src="images/merch.png" alt="" /></a></td>
                                <td>Наименование</td>
                                <td><a href="#" class="nameProductLink">Маска тройного действия для лица GI BEAUTY</a></td>
                            </tr>
                            <tr>
                                <td>Цена</td>
                                <td><i class="price-forone">100 000</i> <span class="fa fa-rub"></span></td>
                            </tr>
                            <tr>
                                <td>Кол-во</td>
                                <td>
                                    <div class="merchendise-count">
                                        <span class="minus inclickSpan">-</span>
                                        <input type="text" class="number" name="quantity" value="1" readonly="readonly"/>
                                        <span class="plus inclickSpan">+</span>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>Стоимость</td>
                                <td><i class="total-pr">100 000</i> <span class="fa fa-rub"></span></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="inclick-bottom-belt">
                    <div class="inputStyle">
                        <input type="text" name="name"/>
                        <span></span>
                        <label>Ваш имя</label>
                    </div>
                    <div class="inputStyle">
                        <input type="text" name="phone" class="userphone"/>
                        <span></span>
                        <label>Ваш телефон</label>
                    </div>
                    <button>отправить заказ <span class="fa fa-arrow-right"></span></button>
                </div>
            </form>
        </div>
    </div>
    <div class="commonpopup" >
        <div class="close-popup"></div>
        <div id="inclick-confirmation">
            <table>
                <tr>
                    <td>
                        <h2>ваш заказ успешно создан!</h2>
                        <h3>Наш менеджер свяжется с Вами в ближайшее время</h3>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="commonpopup" >
        <div class="close-popup"></div>
        <div id="alert">
            <table>
                <tr>
                    <td>
                        <h2>Поздравляем, Вы успешно подписались на рассылку новостей</h2>

                        <h3>все сообщения будут отправляться на адрес <a href="mailto:login@mail.ru">login@mail.ru</a></h3>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="commonpopup" id="thanksPopUp">
        <div class="close-popup"></div>
        <div id="alert">
            <table>
                <tr>
                    <td>
                        <h2>Спасибо за заявку</h2>
                        <a href="mailto:<?= JFactory::getMailer()->From; ?>" onclick="yaCounter35741200.reachGoal('mail_index'); return true;"><?= JFactory::getMailer()->From; ?></a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="commonpopup" >
        <div class="close-popup"></div>
        <div id="subscribeaction" data-text="подписка на акции">
            <table>
                <tr>
                    <td>
                        <div class="img-icon"><img src="images/icon1.png" alt="" /></div>
                        <h2>Подпишитесь и будьте в курсе!</h2>
                        <form action="#">
                            <fieldset>
                                <div class="form-item" data-hint="Тест тест тест"><input type="text" /><label>Ваше имя</label></div>
                            </fieldset>
                            <fieldset>
                                <div class="form-item" data-hint="Тест тест тест"><input type="text" /><label>Ваш электронный адрес</label></div>
                            </fieldset>
                            <button>Оформите бесплатную подписку</button>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="commonpopup" >
        <div class="close-popup"></div>
        <div id="subscribenews" data-text="подписка на новости">
            <table>
                <tr>
                    <td>
                        <div class="img-icon"><img src="images/icon2.png" alt="" /></div>
                        <h2>Подпишитесь и владейте всей полнотой информации!</h2>
                        <form action="#">
                            <fieldset>
                                <div class="form-item" data-hint="Тест тест тест"><input type="text" /><label>Ваше имя</label></div>
                            </fieldset>
                            <fieldset>
                                <div class="form-item" data-hint="Тест тест тест"><input type="text" /><label>Ваш электронный адрес</label></div>
                            </fieldset>
                            <button>Оформите бесплатную подписку</button>
                        </form>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="popupoverlay"></div>
    <div id="popUpCall">
        <form method="POST" data-target="call_form">
            <h2 class="nameInner">Заказать звонок</h2>
            <p class="forNameForm nameInner">Заказать звонок</p>
            <div>
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Ваше имя</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone">
                    <span></span>
                    <label>Ваш телефон</label>
                </div>
            </div>
            <button class="btnStyle send">Отправить</button>
            <div class="closeFancy">
                <span></span>
                <span></span>
            </div>
        </form>
    </div>
    <div class="commonpopup">
        <div class="addedInBasket" id="addedInBasket">
            <div>
                <div>
                    <h2><span class="numberAddInBasket"></span> x <span class="pname"></span> добавлен <a href="<?= JRoute::_("/index.php?option=com_virtuemart&view=cart") ?>">вашу корзину</a></h2>
                </div>            
            </div>
            <div class="addInBasketWrap">
                <button class="btnStyle addedInBasketStyle closePopUpAdded">
                    <i class="fa fa-arrow-left"></i>продолжить покупки
                </button>
                <a class="btnStyle addedInBasketStyle" href="<?= JRoute::_("/index.php?option=com_virtuemart&view=cart") ?>">перейти в корзину<i class="fa fa-arrow-right"></i></a>
            </div>
            <div class="close-popup"></div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/fancyBox/source/jquery.fancybox.js"></script>
    <? if ($main or $contact_page): ?>
        <script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <? endif; ?>
    <script src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/js/script.js"></script>
</body>
</html>
