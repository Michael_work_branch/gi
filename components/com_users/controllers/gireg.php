<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';
require_once JPATH_COMPONENT . '/controllers/registration.php';

/**
 * Registration controller class for Users.
 *
 * @since  1.6
 */
class UsersControllerGireg extends UsersControllerRegistration {
    
    public function register() {
        
        if ($curl = curl_init()) {
            $jinput = JFactory::getApplication()->input;
            curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify?secret=6LdqdRgTAAAAAPCLrudqipQL7bV_OUhuEpUMiQW-&response=' . $jinput->post->get('g-recaptcha-response', '', 'string'));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $out = curl_exec($curl);
            $out = json_decode($out);
            curl_close($curl);
        }

        if (!$out->success) {
            $this->setRedirect(JRoute::_('./', false));
			return false;
        }
        
        $jform = $this->input->post->get('jform', array(), 'array');
        $jform['email2'] = $jform['email1'];
        $jform['username'] = $jform['email1'];
        $this->input->post->set('jform', $jform);
        
        parent::register();
        
    }
    
}