<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');

class AWMartModelOrders extends JModelItem {

    public $items;

    function save($orders) {
        if (is_object($orders)and count($orders)) {
            $this->items=array();
            $db = JFactory::getDbo();
            $query=$db->getQuery(true);
            $order = new stdClass();
            $order->order = $orders->order;

            $session = JFactory::getSession();
            $cartData = json_decode($session->get('vmcart', 0, 'vm'))->cartProductsData;
            if (count($cartData)) {
                foreach ($cartData as $key => $value) {
                    $order->vmid = $value->virtuemart_product_id;
                    $order->quantity = $value->quantity;
                    $db->insertObject('#__awmart_orders_details', $order);
                    $query->clear();

                    $query->select('product_name')
                            ->from('#__virtuemart_products_ru_ru')
                            ->where('virtuemart_product_id=' . $value->virtuemart_product_id);
                    $db->setQuery($query);
                    $dt = $db->loadObject();
                    $this->items[$key]['productName'] = $dt->product_name;
                    $query->clear();

                    //получаем цену товара
                    $query->select('product_price')
                            ->from('#__virtuemart_product_prices')
                            ->where('virtuemart_product_id=' . $value->virtuemart_product_id);
                    $db->setQuery($query);
                    $dt = $db->loadObject();
                    $this->items[$key]['initialPrice'] = $dt->product_price;
                    $query->clear();

                    $this->items[$key]['quantity'] = $value->quantity;
                }
            } else {
                return FALSE;
            }
            if (!$db->insertObject('#__awmart_orders', $orders)) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public function update($update) {
        if (!$this->getDbo()->updateObject('#__awmart_orders', $update, 'order')):
            JFactory::getSession()->clear('vmcart');
            return false;
        endif;
        return true;
    }

}
