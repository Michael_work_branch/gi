<?php

defined('_JEXEC') or die;

class AWMartControllerNewOrder extends JControllerLegacy {

    public function newOrder($cachable = false, $urlparams = array()) {

        print'<meta charset="utf-8">';
        $model = $this->getModel('orders');

        $jinput = JFactory::getApplication()->input;
        $address = $jinput->request->get('address', '', 'array');
        $phone = $jinput->request->get('phone', '', 'string');
        $endprice = $jinput->request->get('endprice', '', 'string');

        $insert = array(
            "address" => json_encode($address),
            "phone" => $phone,
            "order" => time(),
            "user_id" => JFactory::getUser()->id,
            "price" => $endprice
        );
        if ($model->save((object) $insert)) {
            /* ------------------------------------------------------------------ */
            $postUrl = "https://gi-beauty.retailcrm.ru/api/v3/orders/create?apiKey=ARe3N7dhqk0wCrHYuGvO8RClwe0gSpzv";

            // дополняем массив с данными файлами
            $postData = array();

            $order = array('number' => $insert['order'],
                'externalId' => $insert['order'],
                'contragentType' => 'individual',
                'firstName' => 'unnamed',
                'phone' => $phone,
                'call' => true,
                'delivery' => array(
                    'address' => array(
                        'text' => $address['index'] . ' ' . $address['region'] . ' ' . $address['city'] . ' ' . $address['street'] . ' ' . $address['hous'] . ' ' . $address['apartment']
                    )
                )
            );

            if (count($model->items)):
                $order['items'] = $model->items;
            endif;



            $postData['order'] = json_encode($order);

//            print "<pre>";
//            print_r($postData);
//            print "</pre>";
            // создаем подключение
            $ch = curl_init($postUrl);
            // устанавлваем даные для отправки
            curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
            // флаг о том, что нужно получить результат
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // отправляем запрос
            //$response = curl_exec($ch);
            curl_exec($ch);
            // закрываем соединение
            curl_close($ch);
            /* ------------------------------------------------------------------ */
            //обработка робокассы
            // регистрационная информация (логин, пароль #1)
            $mrh_login = "liomatrixgroup";
            $mrh_pass1 = "GSF5jxfgW99LYeSn_1";

            // номер заказа
            $inv_id = $insert['order'];
//            $inv_id = 123;
            // описание заказа
            $inv_desc = "Заказ в магазине GI. Номер заказа $insert[order]";

            // сумма заказа
            $out_summ = $insert['price'];
//            $out_summ = 100;
            // предлагаемая валюта платежа
            $in_curr = "";

            // язык
            $culture = "ru";

            // кодировка
            $encoding = "utf-8";

            // формирование подписи
            $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");
            header("location: https://merchant.roboxchange.com/Index.aspx?" .
                    "MrchLogin=$mrh_login&OutSum=$out_summ&InvId=$inv_id&IncCurrLabel=$in_curr" .
                    "&Desc=$inv_desc&SignatureValue=$crc" .
                    "&Culture=$culture&Encoding=$encoding&IsTest=1");
//            print 'обьект сохранен';
        } else {
            header('location: ./');
        }
    }

    public function success() {
        JFactory::getSession()->destroy();
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $this->getView('newOrder', $viewType)->success();
        //parent::display($cachable, $urlparams);
    }

    public function fail() {
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $this->getView('newOrder', $viewType)->fail(); //установили нужный нам макет
        //parent::display($cachable, $urlparams);
    }

    public function resultUrl() {
        $post = JFactory::getApplication()->input->post;

        // регистрационная информация (пароль #2)
        $mrh_pass2 = "ixQ7CoHj2ZdM4uJQ_2";

        //установка текущего времени
        $tm = getdate(time() + 9 * 3600);
        $date = "$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";

        // чтение параметров
        $out_summ = $post->get('OutSum');
        $inv_id = $post->get('InvId');
        $crc = $post->get('SignatureValue');
        $crc = strtoupper($crc);

        $my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2"));

        if ($my_crc != $crc) {
            echo "bad sign\n";
            jexit();
        }

        $update = array(
            'order' => $inv_id,
            'date_success' => $date,
            'status' => 1
        );

        /* ------------------------------------------------------------------ */
        $postUrl = "https://gi-beauty.retailcrm.ru/api/v3/orders/" . $inv_id . "/edit?apiKey=ARe3N7dhqk0wCrHYuGvO8RClwe0gSpzv";
        // дополняем массив с данными файлами

        $order = array(
            'paymentStatus' => 'paid',
            'paymentType' => 'e-money',
//            'paymentType' => $out_summ,
        );

        $postData = array();
        $postData['order'] = json_encode($order);

//        print "<pre>";
//        print_r($out_summ);
//        print "</pre>";
        // создаем подключение
        $ch = curl_init($postUrl);
        // устанавлваем даные для отправки
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        // флаг о том, что нужно получить результат
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // отправляем запрос
        //$response = curl_exec($ch);
        curl_exec($ch);
        // закрываем соединение
        curl_close($ch);
        /* ------------------------------------------------------------------ */
        $model = $this->getModel('orders');
        $model->update((object) $update);

        // признак успешно проведенной операции
        echo "OK$inv_id\n";
        jexit();
    }

}
