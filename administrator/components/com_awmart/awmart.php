<?php

defined('_JEXEC') or die;

//JLog::addLogger(
//        array('text_file' => 'com_inphone.php'), JLog::ALL, array('com_inphone')
//);
JError::$legacy = false;

jimport('joomla.application.component.controller');

// Получаем экземпляр контроллера с префиксом Phoenix.
$controller = JControllerLegacy::getInstance('AWMart');

// Исполняем задачу task из Запроса.
$controller->execute(JFactory::getApplication()->input->get('task'));

// Перенаправляем, если перенаправление установлено в контроллере.
$controller->redirect();

/*
//так работает
$controller = JRequest::getWord('view', 'Orders');
$task = JRequest::getWord('task', 'Orders');
if ($controller == 'On') {  //по дефолту если пытаемся войти то посылаем на главную
    header('Location: ./ ');
    jexit();
}

jimport('joomla.filesystem.file');
jimport('joomla.html.parameter');

if (JFile::exists(JPATH_COMPONENT_ADMINISTRATOR    . DS . 'controllers' . DS . $controller . '.php')) {
    $classname = 'AWMartController' . $controller;
    if (!class_exists($classname))
        require_once (JPATH_COMPONENT_ADMINISTRATOR . DS . 'controllers' . DS . $controller . '.php');
    $controller = new $classname();
    $controller->execute($task);
    $controller->redirect();
}else {
    JError::raiseError(404, JText::_('Вы стучитесь в закрытую дверь. Постучите в другую, воможно там открыто.'));
}
 * */
 