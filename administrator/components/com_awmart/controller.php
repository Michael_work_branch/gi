<?php

// Запрет прямого доступа.
defined('_JEXEC') or die;

// Подключаем библиотеку контроллера Joomla.
jimport('joomla.application.component.controller');

class AWMartController extends JControllerLegacy {

    protected $default_view = 'orders';

    public function display($cachable = false, $urlparams = array()) {
        $doc = JFactory::getDocument();

        $view = $this->input->get('view', 'orders');
        $layout = $this->input->get('layout', 'default');
        $id = $this->input->getInt('id');

        parent::display($cachable);
        return $this;
    }

}
