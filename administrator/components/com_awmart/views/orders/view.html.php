<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AWMartViewOrders extends JViewLegacy {

    protected $items;
    protected $pagination;

    public function display($tpl = null) {
        if (count($errors = $this->get('Errors'))) {
            JError::raiseError(500, implode("\n", $errors));
            return false;
        }
        try {
            // Получаем данные из модели.
            $this->items = $this->get('Items');
            $this->pagination = $this->get('Pagination');

            $this->addToolbar();

            // Отображаем представление.
            parent::display($tpl);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    protected function addToolbar() {
        JToolbarHelper::title(JText::_('Заказы'));

        JToolbarHelper::deleteList('', 'Orders.delete');
        
        JHtmlSidebar::setAction('index.php?option=com_awmart&view=orders');

    }

}
