<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;

?>
<div class="adminlist">
    <div><?php echo $this->loadTemplate('head'); ?></</div>
    <div><?php echo $this->loadTemplate('body'); ?></div>
    <div><?php echo $this->loadTemplate('foot'); ?></div>
</div>