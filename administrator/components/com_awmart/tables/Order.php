<?php
defined('_JEXEC') or die;

class AWMartTableOrder extends JTable {

    /**
     * Конструктор
     *
     * Параметры объекта базы данных. Это то с чем мы будем работать.
     */
    public function __construct(&$db) {
        parent::__construct('#__awmart_orders', 'id', $db);
    }

}
