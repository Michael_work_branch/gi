<?php

defined('_JEXEC') or die;

class AWMartModelOrder extends JModelAdmin {

    /**
     * Returns возвращает ссылку на объект.
     *
     * @param	type	Тип таблицы для создания экземпляра
     * @param	string	Префикс для имени класса таблицы.
     * @param	array	Массив для модели.
     * @return	JTable	Объекты базы данных
     */
    //Возвращает ссылку на объект таблицы, при его создании.	 
    public function getTable($type = 'order', $prefix = 'AWMartTable', $config = array()) {
        return JTable::getInstance($type, $prefix, $config);
    }

    /**
     * Метод получения данных
     * 
     * @param	array	$data		Данные для формы.
     * @param	boolean	$loadData	Форма для того что бы загрузить свои данные(по умолчанию).
     * @return	mixed	Вернуть данные в случае успешного завершения.
     */
    public function getForm($data = array(), $loadData = true) {
        // Получить форму
        $form = $this->loadForm('com_awmart.order', 'order', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * Метод, чтобы получить данные, которые должны быть выведены в форме.
     *
     * @return	mixed	Данные по форме.
     */
    protected function loadFormData() {
        // Проверка сессий для ранее введёных данных формы
        $data = JFactory::getApplication()->getUserState('com_awmart.order.data', array());
        if (empty($data)) {
            $data = $this->getItem();
        }
        return $data;
    }

    public function getItems() {
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        $order = JFactory::getApplication()->input->request->get('order', 0, 'int');
        $this->indexLocate($order);
        $query->select('*')
                ->from('#__awmart_orders_details')
                ->where('`order` = ' . $order);
        $db->setQuery($query);
        if (!($result = $db->loadObjectList())) {
            $this->indexLocate();
        }
        $query->clear();
        if (count($result)):
            foreach ($result as &$val) {
                $query->select('product_name')
                        ->from('#__virtuemart_products_ru_ru')
                        ->where('virtuemart_product_id=' . $val->vmid);
                $db->setQuery($query);
                $dt = $db->loadObject();
                $val->name = $dt->product_name;
                $query->clear();
            }
        endif;
        $this->item = $result;
        print $this->getTable();
        return $result;
    }

    private function indexLocate($param = FALSE) {
        if (!$param):
            header('location: ./');
            jexit();
        endif;
    }

}
