<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;
 
// Подключаем библиотеку controlleradmin Joomla.
jimport('joomla.application.component.controlleradmin');
 
class AWMartControllerDel extends JControllerAdmin
{
    public function getModel($name = 'Del', $prefix = 'AWMartModel') {
        return parent::getModel($name, $prefix, array('ignore_request' => true));
    }
}