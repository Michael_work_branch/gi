<?php die("Access Denied"); ?>#x#a:5:{s:4:"body";s:33245:"<!----------------------------------SECTION-1-------------------------------------------------->
<section class="section1">
    <div>
        <h2>Инновационная продукция высшего качества!</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <a href="images/pdf/present_osnova.pdf" target="_blank">
            <button class="btnStyle">Откройте презентацию</button>
        </a>
    </div>
</section>
<!----------------------------------SECTION-2-------------------------------------------------->
<section class="section2">
    <div>
        <div class="sec2TitleWrap animated">
            <img src="images/section2/openbook.png">
            <h3>Выберите лучшее из каталога!</h3>
        </div>
        <div class="maleWrap">
            <div class="womenCat animated">
                <p>Для</p>
                <h4>леди</h4>
                <img src="images/section2/lady.png">
                <div class="maskSec2"></div>
                <span class="sec2Anim1"></span>
                <span class="sec2Anim2"></span>
                <span class="sec2Anim3"></span>
            </div>
            <div class="manCat animated">
                <p>Для</p>
                <h4>джентльменов</h4>
                <img src="images/section2/gentleman.png">
                <div class="maskSec2"></div>
                <span class="sec2Anim4"></span>
                <span class="sec2Anim5"></span>
                <span class="sec2Anim6"></span>
            </div>
        </div>
        <div class="catalogSec2">
            <div class="genderWrap">
                <button class="btnStyle" id="forLady">Для леди</button>
                <button class="btnStyle" id="forGentleman">Для джентльменов</button>
            </div>
            <div class="sliderSec2-1 sliderSec2First main">

            </div>
            <div class="sliderSec2-1 sliderSec2Second main">

            </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-3-------------------------------------------------->
<section class="section3">
    <div class="animationH2">
        <h2>«НПО ЛИОМАТРИКС» - ценный опыт, научные знания</h2>
        <span></span>
        <span></span>
    </div>
    <div class="sec3Wrap">
        <div>
            <img src="images/section3/img4.png" class="sec3Anim1 animated">
            <h6>Наши ученые ведут</h6>
            <p><span data-value="7">0</span> инновационных научных<br>исследований</p>
        </div>
        <div>
            <img src="images/section3/img3.png" class="sec3Anim2 animated">
            <h6>Приняли участие</h6>
            <p>в <span data-value="78">0</span> научных конференциях</p>
        </div>
        <div>
            <img src="images/section3/img2.png" class="sec3Anim3 animated">
            <h6>С нами работают</h6>
            <p><span data-value="6">0</span> ведущих научных центров</p>
        </div>
        <div>
            <img src="images/section3/img1.png" class="sec3Anim4 animated">
            <h6>Сотрудничают более</h6>
            <p><span data-value="54">0</span> клиник по всему миру</p>
        </div>
    </div>
</section>
<!----------------------------------SECTION-4-------------------------------------------------->
<section class="section4">
    <div>
        <span class="sec4Anim sec4Block1">
            <span></span>
        </span>
        <span class="sec4Anim sec4Block2">
            <span></span>
        </span>
        <span class="sec4Anim sec4Block3">
            <span></span>
        </span>
        <span class="sec4Anim sec4Block4">
            <span></span>
        </span>
        <div class="sec4Wrap">
                <div><p>Компания ООО «НПО ЛИОМАТРИКС» была создана в 2010 году Ринатом Гаптрауфовичем Гильмутдиновым - главным внештатным трансфузиологом МЗ Оренбургской области, заслуженным врачом РФ, кандидатом медицинских наук, неоднократным победителем Всесоюзного общественного форума медицинских работников за вклад в развитие донорского движения, автором мирового рекорда Гиннесса «Самая большая капля крови из людей», и Рамилем Рафаилевичем Рахматуллиным - доктором медицинских наук, лауреатом национальной премии в области инноваций имени В.Зворыкина, разработчиком гистоэквивалент-биопластического материала (биокожи)</p></div>
    <div class="sliderWrap">            <div>
                <a href=""><img src="/index.php?option=com_k2&amp;id=51_7b1d2e14c8aea2e5691024d6bcc893fd&amp;lang=ru&amp;task=download&amp;view=item"></a>
            </div>
                    <div>
                <a href="scientist.jpg"><img src="/index.php?option=com_k2&amp;id=54_01a2b0600fe3afd3ad15a44e0fe10d69&amp;lang=ru&amp;task=download&amp;view=item"></a>
            </div>
                    <div>
                <a href="bgImg2.jpg"><img src="/index.php?option=com_k2&amp;id=55_bbb5ab4d5896b7eecbffe2b24ff00cc0&amp;lang=ru&amp;task=download&amp;view=item"></a>
            </div>
            </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-5-------------------------------------------------->
<section class="section5">
    <div>
        <div class="slider-indicator">
            <h2>Наши партнеры</h2>
            <span></span>
        </div>
        <div class="sliderSec5">
                    <div class="slidesSec5">
            <div class="logoName">
                <p>Центр биомедицинских технологий ЦКБ</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://cchp.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=38_d4d116714fe0a749391d1af037542ade&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>Фонд Бортника</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://www.fasie.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=39_7c04905e56346324622b468fa96cb364&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>«Селебрити групп»</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://celebreev.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=40_c5b8350e9fb73b1769319b7ac58fd860&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>Оренбургская областная станция переливания крови</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://orenblood.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=41_75e700e9c357b6714f6756a4394129c1&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>Компания Дельрус</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://www.delrus.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=42_2956afee0a64173b5e6ebb611cf42546&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>ООО «КэмБио Лаб»</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://chembio.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=43_3437254e37585ee74b33441905d47914&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>Институт экспериментальной медицины</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://samsmu.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=44_89007ef684f88f503d450281a50f385d&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>Туристический комплекс GOLDCITY</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://goldciti.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=45_ed1d0175ea3e8b0ddf66d18e1a3288ba&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>ЗАО «НоваКом»</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://nova-com.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=46_41b13d3be14fcacda286e03adaf31b5e&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>ООО «Торговый дом Тиана»</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://teana-labs.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=47_aeb7408861303ef770cd89a6cf8590a1&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>ООО «ВИТАЦЕЛ»</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="" target="_blank"><img src="/index.php?option=com_k2&amp;id=48_5c343a165acbc13aa1055fd1289d37a8&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>Зворыкинский проект</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="" target="_blank"><img src="/index.php?option=com_k2&amp;id=49_085f62cf2d2ebac6f059c4bb1576ae45&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                <div class="slidesSec5">
            <div class="logoName">
                <p>Университет имени И.М. Сеченова</p>
            </div>
            <div class="logoWrapSec5">
                <div>
                    <a href="http://www.mma.ru/" target="_blank"><img src="/index.php?option=com_k2&amp;id=50_9c52c850dc6f59d3751bd4847d97df7d&amp;lang=ru&amp;task=download&amp;view=item"></a>
                </div>
            </div>
        </div>
                </div>
    </div>
</section>
<!----------------------------------SECTION-6-------------------------------------------------->
<section class="section6">
    <div class="animationH2">
        <h2>Кредо  «НПО ЛИОМАТРИКС» - постоянное совершенствование!</h2>
        <span></span>
        <span></span>
    </div>
    <div class="sec6AnimWrap">
        <div class="sec6Anim1-1 animated">
            <img src="images/section6/img1Sec6.png">
            <p>Компания обладает самой современной научной базой</p>
        </div>
        <div class="sec6Anim2-1 animated">
            <img src="images/section6/img2Sec6.png">
            <p>Коллектив - команда единомышленников</p>
        </div>
        <div class="sec6Anim3-1 animated">
            <img src="images/section6/img3Sec6.png">
            <p>Постоянно генерируем инновационные идеи</p>
        </div>
    </div>
    <div class="sec6Wrap animationH2">
        <span></span>
        <h3>Активно участвуем в конкурсах инновационных научных проектов</h3>
        <div class="sec6WrapInline">
            <img src="images/section6/logo1Sec6.png" class="sec6Anim1 animated">
            <img src="images/section6/logo2Sec6.png" class="sec6Anim2 animated">
            <img src="images/section6/logo3Sec6.png" class="sec6Anim3 animated">
            <img src="images/section6/logo4Sec6.png" class="sec6Anim4 animated">
            <img src="images/section6/logo5Sec6.png" class="sec6Anim5 animated">
        </div>
        <h3>и побеждаем!</h3>
        <span></span>
    </div>
</section>
<!----------------------------------SECTION-7-------------------------------------------------->
<section class="section7">
    <div>
        <h3>Занимаемся разработкой, исследованием и оценкой различных биопластических материалов  с использованием клеточных технологий  для лечения больных</h3>
        <div class="sec7Wrap">
                <div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/D0R0vNsPJCQ" frameborder="0" allowfullscreen></iframe>
        <p>Ролик на CNN "Органоспецифичесикй регенерант GI"</p>    </div>
    <div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/yoIYzO7CcH0" frameborder="0" allowfullscreen></iframe>
    </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-8-------------------------------------------------->
<section class="section8">
    <div class="animationH2">
        <h2>Без науки не быть красоте!</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <p><span>«НПО ЛИОМАТРИКС»</span> создает космецевтические препараты нового поколения .Вся продукция производится в соответствии с международным стандартом <span>GMP</span>,  проходит тестирование и поступает в продажу только после того, как доказала свою эффективность.</p>
        <div class="sliderSec8">
            <div class="slidesSec8">
                <img src="images/section8/img1Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img2Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img3Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img4Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img5Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img6Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img7Sec8.png">
            </div>
            <div class="slidesSec8">
                <img src="images/section8/img4Sec8.png">
            </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-9-------------------------------------------------->
<section class="section9">
    <div class="animationH2">
        <h2>Хотите знать больше?</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <form>
            <p class="forNameForm">Хотите знать больше?</p>
            <h3>Обратитесь к менеджеру «НПО ЛИОМАТРИКС»</h3>
            <div class="formWrap">
                <div class="inputStyle">
                    <input name="name" type="text">
                    <span></span>
                    <label>Ваше имя</label>
                </div>
                <div class="inputStyle">
                    <input name="phone" type="text" class="userphone">
                    <span></span>
                    <label>Ваш телефон*</label>
                </div>
            </div>
            <div class="textAreaStyle">
                <textarea name="question"></textarea>
                <span></span>
                <label>Задайте вопрос*</label>
            </div>
            <button class="btnStyle">Отправьте</button>
            <div class="sec9WrapText">
                <p>Политика компании — абсолютная конфиденциальность в отношении настоящих и будущих клиентов</p>
            </div>
        </form>
    </div>
</section>
<!----------------------------------SECTION-10------------------------------------------------>
<section class="section10">
    <div class="animationH2">
        <h2>Мы - за широту диапазона!</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <div>
            <h4>мы работаем</h4>
            <div class="sec10Wrap">
                <div class="sec10Anim1-1 animated">
                    <h3>с косметологами</h3>
                    <img src="images/section10/bg1Sec10.png">
                    <a href="/index.php?option=com_content&view=featured&Itemid=143" class="maskSec2"></a>
                    <span class="sec10Anim1"></span>
                    <span class="sec10Anim2"></span>
                    <span class="sec10Anim3"></span>
                </div>
                <div class="sec10Anim2-1 animated">
                    <h3>с салонами и клиниками</h3>
                    <img src="images/section10/bg2Sec10.png">
                    <a href="/index.php?option=com_content&view=featured&Itemid=144" class="maskSec2"></a>
                    <span class="sec10Anim4"></span>
                    <span class="sec10Anim5"></span>
                    <span class="sec10Anim6"></span>
                </div>
                <div class="sec10Anim3-1 animated">
                    <h3>С теми, кто «сам себе косметолог»</h3>
                    <img src="images/section10/bg3Sec10.png">
                    <a href="/index.php?option=com_content&view=featured&Itemid=145" class="maskSec2"></a>
                    <span class="sec10Anim7"></span>
                    <span class="sec10Anim8"></span>
                    <span class="sec10Anim9"></span>
                </div>
                <div class="sec10Anim4-1 animated">
                    <h3>ищем<br>новых партнеров</h3>
                    <img src="images/section10/bg4Sec10.png">
                    <a href="/index.php?option=com_content&view=featured&Itemid=146" class="maskSec2"></a>
                    <span class="sec10Anim10"></span>
                    <span class="sec10Anim11"></span>
                    <span class="sec10Anim12"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<!----------------------------------SECTION-11-------------------------------------------->
<section class="section11 calc">
    <div>
        <h4>калькулятор</h4>
        <form>
            <div class="sec11WrapTop">
                <div class="checkBtnWrap">
                    <div class="radioWrap" id="sex">
                        <div class="radioStyle">
                            <input type="radio" name="gender" value="женщина">
                            <div>
                                <img src="images/section11/lips.png">
                            </div>
                        </div>
                        <div class="radioStyle">
                            <input type="radio" name="gender" value="мужчина">
                            <div>
                                <img src="images/section11/mustache.png">
                            </div>
                        </div>
                    </div>
                    <label>Ваш пол</label>
                </div>
                <div class="borderForm"></div>
                <div class="checkBtnWrap" id="age">
                    <div class="slider-range"></div>
                    <input type="hidden" name="age" id="ageInner">
                    <label>Ваш возраст</label>
                </div>
                <div class="borderForm"></div>
                <div class="checkBtnWrap"  id="typemerch">
                    <select>
                        <option>Крем</option>
                        <option>маска</option>
                        <option>средство для умывания</option>
                        <option>мезопрепараты</option>
                    </select>
                    <label>Выберите тип средства</label>
                </div>
            </div>
            <div class="sec11WrapBottom animated">
                <div class="borderFormGor"></div>
                <div class="radioWrapBot" id="typeskin">
                    <div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Сухая</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Нормальная</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Жирная</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="condition">
                            <div>
                                <p>Комбинированная</p>
                            </div>
                        </div>

                    </div>
                    <label>Состояние кожи</label>
                </div>
                <div class="borderForm"></div>
                <div class="radioWrapBot" id="typeaction">
                    <div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>Увлажнение</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>Очищение</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>Питание</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>комплексный уход</p>
                            </div>
                        </div>
                        <div class="radioStyleBot">
                            <input type="radio" name="kind">
                            <div>
                                <p>anti-age</p>
                            </div>
                        </div>
                    </div>
                    <label>Что бы Вы хотели получить?</label>
                </div>
                <button class="btnStyle">подобрать</button>
            </div>
        </form>
        <div class="absoluteBottom">
            <h3>эффект гарантирован!</h3>
            <img src="images/section11/arrow.png">
        </div>
    </div>
</section>
<!----------------------------------SECTION12-------------------------------------------->
<section class="section12">
    <div>
        <div class="sliderSec12">

        </div>
        <div class="afterCos">
            <div>
                <img id="girl" data-face="female" src="images/faces/female/1.jpg">                <div class="hide-foto" style="display: none" id="calcfoto">
                    <img data-face="female" src="images/faces/female/2.jpg"><img data-face="female" src="images/faces/female/3.jpg"><img data-face="female" src="images/faces/female/4.jpg"><img data-face="female" src="images/faces/female/5.jpg"><img data-face="female" src="images/faces/female/6.jpg"><img data-face="female" src="images/faces/female/7.jpg"><img data-face="female" src="images/faces/female/8.jpg"><img data-face="female" src="images/faces/female/9.jpg"><img data-face="male" src="images/faces/male/11.jpg">                </div>
            </div>
            <div class="sec12H6Wrap">
                <div>
                    <h6>после пилинга</h6>
                </div>
                <div>
                    <h6>8 часов после применения средств GI BEAUTY</h6>
                </div>
            </div>

        </div>
    </div>
</section>
<!----------------------------------SECTION13-------------------------------------------->
<section class="section13">
    <div class="animationH2">
        <h2>Новостная лента</h2>
        <span></span>
        <span></span>
    </div>
    <div>
        <a href="/index.php?option=com_k2&view=itemlist&layout=category&task=category&id=3" class="allNews">Все новости</a>
        <div class="sliderSec13">
                    <div class="slidesSec13">
            <a href="/novosti/bioritmy-kozhi">
                <div>
                    <h2>01/03</h2>
                    <h6>2016</h6>
                    <p>Биоритмы кожи</p>
                </div>
                <div>
                    <img src="/media/k2/items/cache/0b1ad7a7b79268a1f4558db78e092446_Generic.jpg">
                </div>
            </a>
        </div>
            <div class="slidesSec13">
            <a href="/novosti/semya-i-zdorove/kak-izbavitsya-ot-sinyakov-pod-glazami">
                <div>
                    <h2>26/02</h2>
                    <h6>2016</h6>
                    <p>Как избавиться от синяков под глазами</p>
                </div>
                <div>
                    <img src="/media/k2/items/cache/f710044bf79a4b1f5d8b085e5e5d9711_Generic.jpg">
                </div>
            </a>
        </div>
            <div class="slidesSec13">
            <a href="/novosti/novinki-kosmetologii-i-esteticheskoj-meditsiny">
                <div>
                    <h2>25/02</h2>
                    <h6>2016</h6>
                    <p>Новинки косметологии и эстетической медицины</p>
                </div>
                <div>
                    <img src="/media/k2/items/cache/f7a0a54c92471ac4480e727e4ccf93df_Generic.jpg">
                </div>
            </a>
        </div>
            <div class="slidesSec13">
            <a href="/novosti/istoriya-kosmetiki/olay-nachal-issledovaniya-genetiki-kozhi">
                <div>
                    <h2>25/02</h2>
                    <h6>2016</h6>
                    <p>Olay начал исследования генетики кожи</p>
                </div>
                <div>
                    <img src="/media/k2/items/cache/867519228d1d5325856fc61d710ded0e_Generic.jpg">
                </div>
            </a>
        </div>
            </div>

        <!--            <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>11/01</h2>
                                <h6>2015</h6>
                                <p>5 простых утренних процедур для здоровья 5 простых утренних процедур для здоровья 5 простых утренних процедур</p>
                            </div>
                            <div>
                                <img src="images/section13/img1Sec13.png">
                            </div>
                        </a>
                    </div>
                    <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>15/01</h2>
                                <h6>2015</h6>
                                <p>Возрастные изменения кожи</p>
                            </div>
                            <div>
                                <img src="images/section13/img2Sec13.png">
                            </div>
                        </a>
                    </div>
                    <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>11/01</h2>
                                <h6>2016</h6>
                                <p>5 простых утренних процедур для здоровья</p>
                            </div>
                            <div>
                                <img src="images/section13/img1Sec13.png">
                            </div>
                        </a>
                    </div>
                    <div class="slidesSec13">
                        <a href="#">
                            <div>
                                <h2>11/01</h2>
                                <h6>2016</h6>
                                <p>Возрастные изменения кожи</p>
                            </div>
                            <div>
                                <img src="images/section13/img2Sec13.png">
                            </div>
                        </a>
                    </div>-->
        <form class="sec13formWrap">
            <h3>Подписаться на новости</h3>
            <p class="forNameForm">Подписаться на новости</p>
            <div class="inputStyle">
                <input type="text" name="email" class="usermail">
                <span></span>
                <label>Ваш email</label>
            </div>
            <button>
                <i class="fa fa-envelope-o"></i>
            </button>
        </form>
    </div>
</section>
<!----------------------------------SECTION14-------------------------------------------->
<section class="section14">
    <div class="animationH2">
        <h2>Контактная информация:</h2>
        <span></span>
        <span></span>
    </div>
    <div class="top-contacts-belt">
        <div>
            <i class="fa fa-map-marker"></i>
            <p>Россия, г. Москва Хорошевское шоссе 32-а</p>        </div>
        <div>
            <i class="fa fa-phone"></i>
            <p>8(495)150-32-18</p>
<p>8(800)777-32-18</p>        </div>
        <div>
            <i class="fa fa-envelope mailI"></i>
            <a href="mailto:info@gi-beauty.ru">info@gi-beauty.ru</a>
        </div>
    </div>
    <div id="mapWrap"></div>
</section>
";s:4:"head";a:11:{s:5:"title";s:9:"GI BEAUTY";s:11:"description";N;s:4:"link";s:0:"";s:8:"metaTags";a:2:{s:10:"http-equiv";a:1:{s:12:"content-type";s:24:"text/html; charset=utf-8";}s:8:"standard";a:2:{s:8:"keywords";N;s:6:"rights";N;}}s:5:"links";a:2:{s:10:"/feed/rss/";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:19:"application/rss+xml";s:5:"title";s:7:"RSS 2.0";}}s:11:"/feed/atom/";a:3:{s:8:"relation";s:9:"alternate";s:7:"relType";s:3:"rel";s:7:"attribs";a:2:{s:4:"type";s:20:"application/atom+xml";s:5:"title";s:8:"Atom 1.0";}}}s:11:"styleSheets";a:1:{s:29:"/components/com_k2/css/k2.css";a:3:{s:4:"mime";s:8:"text/css";s:5:"media";N;s:7:"attribs";a:0:{}}}s:5:"style";a:0:{}s:7:"scripts";a:6:{s:33:"/media/system/js/mootools-core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:24:"/media/system/js/core.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:27:"/media/jui/js/jquery.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:34:"/media/jui/js/jquery-noconflict.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:35:"/media/jui/js/jquery-migrate.min.js";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}s:49:"/components/com_k2/js/k2.js?v2.6.9&amp;sitepath=/";a:3:{s:4:"mime";s:15:"text/javascript";s:5:"defer";b:0;s:5:"async";b:0;}}s:6:"script";a:0:{}s:6:"custom";a:0:{}s:10:"scriptText";a:0:{}}s:13:"mime_encoding";s:9:"text/html";s:7:"pathway";a:0:{}s:6:"module";a:0:{}}