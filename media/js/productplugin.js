/* 
 GI product plugin
 */
var prodArray, customsArray, pageLoad = false;
(function($) {
    $(window).load(function() {
        pageLoad = true;
        $(document).on('click', '.vmAddCartSlider', function() {
            var id = $(this).closest('.byin').attr('data-prod-id');
            var pname;
            for (var i = 0; i < prodArray.length; i++) {
                if (id == prodArray[i].virtuemart_product_id) {
                    pname = prodArray[i].product_name
                }
            }

            $.ajax({
                type: "POST",
                url: 'index.php?option=com_ajax&plugin=ajax&format=json',
                dataType: 'json',
                data: {
                    task: 'addCart',
                    quantity: '1',
                    virtuemart_product_id: id,
                    pname: pname,
                    pid: id
                }
            }).done(function(data) {
                function openCommonPopup(id, element) {
                    $('.popupoverlay').fadeIn(400);
                    $('#' + id).closest('.commonpopup').fadeIn(400);
                }
                $('.basket p span').text(Number($('.basket p span').text())+1);
                $('#addedInBasket').find('.numberAddInBasket').text('1');
                $('#addedInBasket').find('.pname').text(pname);
                openCommonPopup('addedInBasket');
            });
            return  false;
        })
    });
    var fotoArray = [], fotoArrayView = [];
    function fotoInit() {
        $('#calcfoto').find('img').each(function(index) {
            //$(this).attr('data-foto-id', index);
            fotoArray.push(index);
        });
    }
    function fotoRandom() {
        // использование Math.round() даст неравномерное распределение!
        function gender() {
            if ($('#sex input[name="gender"]:checked').attr('data-custom-id') == 1) {
                return 'female';
            } else {
                if ($('#sex input[name="gender"]:checked').attr('data-custom-id') == 2) {
                    return 'male';
                } else {
                    return 'all';
                }
            }
        }
        function getRandomInt(min, max)
        {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }

        var rndIndex = getRandomInt(0, fotoArray.length - 1);
        var sex = gender();
        if (fotoArrayView.length < fotoArray.length) {
            if (!inArray(fotoArrayView, rndIndex)) {
                if (sex == $('#calcfoto').find('img').eq(rndIndex).attr('data-face') || sex == 'all') {
                    $('#girl').attr({'src': $('#calcfoto').find('img').eq(rndIndex).attr('src')});
                    fotoArrayView.push(rndIndex);
                } else {
                    fotoArrayView.push(rndIndex);
                    fotoRandom();
                }

            } else {
                fotoRandom()
            }
        } else {
            fotoArrayView = [];
            fotoRandom();
        }
        ;


    }
    var sliderSec12, sliderSec2_1, sliderSec10;
    function poductpluginInit() {

        if ($('.sliderSec12').length != 0) {
            sliderSec12 = $('.sliderSec12').productplugin(
                    {
                        'filter': [],
                        'itemTemplate': '<div class="slidesSec12"><a><div class="imgCont"></div><p></p></a></div>',
                        'productContainer': '.slidesSec12',
                        'imageContainer': '.imgCont',
                        onRender: function(object) {
                            var windowWidth = $(window).width();
                            if (windowWidth <= 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
        }

        /*********/
        if ($('.sliderSec10').length != 0) {
            sliderSec12 = $('.sliderSec10').not($('.sliderSec10.cosm')).productplugin(
                    {
                        'filter': [],
                        'itemTemplate': '<div class="slidesSec12"><a><div class="imgCont"></div><p></p></a></div>',
                        'productContainer': '.slidesSec12',
                        'imageContainer': '.imgCont',
                        onRender: function(object) {

                            var windowWidth = $(window).width();
                            if (windowWidth <= 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });

            /**/
            sliderSec12 = $('.sliderSec10.cosm').productplugin(
                    {
                        'filter': [],
                        'itemTemplate': '<div class="slidesSec10"><div class="imgCont"></div><p></p></div>',
                        'productContainer': '.slidesSec10',
                        'imageContainer': '.imgCont',
                        'link': '',
                        onRender: function(object) {

                            var windowWidth = $(window).width();
                            if (windowWidth <= 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1460) {
//                                $(object).slick('unslick');
                                $(object).slick({
                                    slidesToShow: 2,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });

        }

        if ($('.sliderSec2-1').length != 0) {
            $('.sliderSec2-1.sliderSec2First.main').productplugin(
                    {
                        'filter': [{"id_group": "10", "value": "1"}],
                        'itemTemplate': ' <div class="slidesSec2-1"><a><div class="slidesSec2ImgWrap"></div></a><div class="nameSec2Wrap"><div><a><p class="prdName"></p></a></div></div><div class="btnStyle"><div class="sec2Bottom"><h5><span class="prdPrice">9 999 999</span><i class="fa fa-rub"></i></h5><div class="buyInOneClick opencommonpopup" data-id="by_inclick"><i class="fa fa-hand-o-up"></i><h6>купить<br>в 1 клик</h6></div><form method="post" class="product" action="#"><input type="hidden" class="quantity-input js-recalculate" name="quantity[]" value="1"><button name="addtocart" class="vmAddCartSlider incart fa  fa-shopping-bag addtocart-button"></button><input class="virtuemart_product_id" type="hidden" name="virtuemart_product_id[]" ><noscript>&lt;input type="hidden" name="task" value="add"/&gt;</noscript><input type="hidden" name="option" value="com_virtuemart"><input type="hidden" name="view" value="cart"><input type="hidden" class="virtuemart_product_id" name="virtuemart_product_id[]"><input type="hidden" class="vmpname" name="pname" ><input type="hidden" class="virtuemart_product_id" name="pid" value="6"></form></div>',
                        'productContainer': '.slidesSec2-1',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {

                            var windowWidth = $(window).width();
                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2First.main').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2First.main').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
            $('.sliderSec2-1.sliderSec2Second.main').productplugin(
                    {
                        'filter': [{"id_group": "10", "value": "2"}],
                        'productContainer': '.slidesSec2-1',
                        'itemTemplate': ' <div class="slidesSec2-1"><a><div class="slidesSec2ImgWrap"></div></a><div class="nameSec2Wrap"><div><a><p class="prdName"></p></a></div></div><div class="btnStyle"><div class="sec2Bottom"><h5><span class="prdPrice">9 999 999</span><i class="fa fa-rub"></i></h5><div class="buyInOneClick opencommonpopup" data-id="by_inclick"><i class="fa fa-hand-o-up"></i><h6>купить<br>в 1 клик</h6></div><form method="post" class="product" action="#"><input type="hidden" class="quantity-input js-recalculate" name="quantity[]" value="1"><button name="addtocart" class="vmAddCartSlider incart fa  fa-shopping-bag addtocart-button"></button><input class="virtuemart_product_id" type="hidden" name="virtuemart_product_id[]" ><noscript>&lt;input type="hidden" name="task" value="add"/&gt;</noscript><input type="hidden" name="option" value="com_virtuemart"><input type="hidden" name="view" value="cart"><input type="hidden" class="virtuemart_product_id" name="virtuemart_product_id[]"><input type="hidden" class="vmpname" name="pname" ><input type="hidden" class="virtuemart_product_id" name="pid" value="6"></form></div>',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {
                            var windowWidth = $(window).width();

                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2Second.main').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2Second.main').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
            $('.sliderSec2-1.sliderSec2First.selebrity').productplugin(
                    {
                        'filter': [],
                        'productContainer': '.slidesSec2-1',
                        'itemTemplate': ' <div class="slidesSec2-1"><a><div class="slidesSec2ImgWrap"></div></a><div class="nameSec2Wrap"><div><a><p class="prdName"></p></a></div></div><div class="btnStyle"><div class="sec2Bottom"><h5><span class="prdPrice">9 999 999</span><i class="fa fa-rub"></i></h5><div class="buyInOneClick opencommonpopup" data-id="by_inclick"><i class="fa fa-hand-o-up"></i><h6>купить<br>в 1 клик</h6></div><form method="post" class="product" action="#"><input type="hidden" class="quantity-input js-recalculate" name="quantity[]" value="1"><button name="addtocart" class="vmAddCartSlider incart fa  fa-shopping-bag addtocart-button"></button><input class="virtuemart_product_id" type="hidden" name="virtuemart_product_id[]" ><noscript>&lt;input type="hidden" name="task" value="add"/&gt;</noscript><input type="hidden" name="option" value="com_virtuemart"><input type="hidden" name="view" value="cart"><input type="hidden" class="virtuemart_product_id" name="virtuemart_product_id[]"><input type="hidden" class="vmpname" name="pname" ><input type="hidden" class="virtuemart_product_id" name="pid" value="6"></form></div>',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {

                            var windowWidth = $(window).width();
                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2First.selebrity').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2First.selebrity').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
            /******/
            $('.sliderSec2-1.sliderSec2First.cosmetologi').productplugin(
                    {
                        'filter': [{"id_group": "10", "value": "1"}],
                        'itemTemplate': '<a class="slidesSec2-1 productPopUp" href="#productPopUp"><div class="slidesSec2ImgWrap"></div><div class="nameSec2Wrap"><div><p class="prdName"></p></div></div><div class="btnStyle">купить в один клик</div></a>',
                        'productContainer': '.slidesSec2-1',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {

                            var windowWidth = $(window).width();
                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2First.cosmetologi').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2First.cosmetologi').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
            $('.sliderSec2-1.sliderSec2Second.cosmetologi').productplugin(
                    {
                        'filter': [{"id_group": "10", "value": "2"}],
                        'productContainer': '.slidesSec2-1',
                        'itemTemplate': '<a class="slidesSec2-1 productPopUp" href="#productPopUp"><div class="slidesSec2ImgWrap"></div><div class="nameSec2Wrap"><div><p class="prdName"></p></div></div><div inclickbtn class="btnStyle">купить в один клик</div></a>',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {
                            var windowWidth = $(window).width();

                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2Second.cosmetologi').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2Second.cosmetologi').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
            $('.sliderSec2-1.sliderSec2First.noprice').productplugin(
                    {
                        'filter': [],
                        'itemTemplate': '<a class="slidesSec2-1 productPopUp" href="#productPopUp"><div class="slidesSec2ImgWrap"></div><div class="nameSec2Wrap"><div><p class="prdName"></p></div></div><div inclickbtn class="btnStyle">купить в один клик</div></a>',
                        'productContainer': '.slidesSec2-1',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {

                            var windowWidth = $(window).width();
                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2First.noprice').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2First.noprice').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
            /************/
            $('.sliderSec2-1.sliderSec2First.partner').productplugin(
                    {
                        'filter': [],
                        'itemTemplate': '<a class="slidesSec2-1 productPopUp" href="#productPopUp"><div class="slidesSec2ImgWrap"></div><div class="nameSec2Wrap"><div><p class="prdName"></p></div></div><div inclickbtn class="btnStyle">сделать заказ</div></a>',
                        'productContainer': '.slidesSec2-1',
                        'imageContainer': '.slidesSec2ImgWrap',
                        'productNameContainer': '.prdName',
                        'productPriseContainer': '.prdPrice',
                        'productDescriptionContainer': '.productpluginDecription',
                        'link': 'a',
                        onRender: function(object) {

                            var windowWidth = $(window).width();
                            if (windowWidth <= 1560) {
                                $('.sliderSec2-1.sliderSec2First.partner').slick({
                                    slidesToShow: 3,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                            if (windowWidth > 1560) {
                                $('.sliderSec2-1.sliderSec2First.partner').slick({
                                    slidesToShow: 4,
                                    slidesToScroll: 2,
                                    infinite: true,
                                    arrows: true
                                })
                            }
                        }
                    });
        }
    }
    ;
    function calcInit() {
        if ($('.calc form').length != 0) {
            calcMainPage = $('.calc form').calcData({
                containers: {
                    'calcid_10': '#sex',
                    'calcid_12': '#typemerch',
                    'calcid_13': '#typeskin',
                    'calcid_14': '#typeaction',
                    'calcid_15': '#age',
                }
            }, sliderSec12);
        }
    }
    var inArray = Array.prototype.indexOf ?
            function(arr, val) {
                return arr.indexOf(val) != -1
            } :
            function(arr, val) {
                var i = arr.length
                while (i--) {
                    if (a[i] === val)
                        return true
                }
                return false
            }



    getProdArray = function() {
        $.ajax({
            type: "POST",
            url: 'index.php?option=com_ajax&plugin=ajax&format=json',
            dataType: 'json',
            data: {
                task: 'getProducts'
            }
        }).done(function(data) {
            prodArray = data.data[0].prods;
            customsArray = data.data[0].customs;
            var pageInterval = setInterval(function() {
                if (pageLoad) {
                    poductpluginInit();
                    calcInit();
                    fotoInit();
                    clearInterval(pageInterval);
                }
            }, 10)


        });
    }
    getProdArray();
    $.fn.productplugin = function(options) {

        options = $.extend({
            'filter': [],
            'productContainer': '.productItem',
            'imageContainer': '.imageContainer',
            'productNameContainer': 'p',
            'itemTemplate': '<div class="productItem"><a><div class="imageContainer"></div><p></p></a><div class="price"></div><div class="price"></div></div>',
            'productPriseContainer': '.price',
            'productDescriptionContainer': '.productpluginDecription',
            'link': 'a',
            'vmpid': '.virtuemart_product_id',
            'vmpname': '.vmpname',
            onRender: function() {
            }
        }, options);
        this.filtration = function(paramArray) {
            var tempArray = [];
            var subtempArray = [];
            if (paramArray.length != 0) {
                tempArray = prodArray;
                for (var i = 0; i < paramArray.length; i++) {
                    for (var j = 0; j < tempArray.length; j++) {
                        if (tempArray[j].customs) {
                            if (paramArray[i]["id_group"] in tempArray[j].customs) {
                                var tempID_GROUP = paramArray[i]["id_group"];
                                for (var l = 0; l < paramArray[i]["value"].length; l++) {
                                    if (inArray(tempArray[j].customs[tempID_GROUP], String(paramArray[i].value[l]))) {
                                        if (!(inArray(subtempArray, tempArray[j])))
                                        {
                                            subtempArray.push(tempArray[j]);
                                        }

                                    }

                                }
                            } else {
                                subtempArray.push(tempArray[j]);
                            }
                        } else {
                            subtempArray.push(tempArray[j]);
                        }
                    }
                    tempArray = subtempArray;
                    subtempArray = [];
                }
                this.render(tempArray);
            }
            else {
                var renderArray = prodArray;
                this.render(renderArray);
            }
        }
        this.render = function(renderArray) {
            this.html('').removeClass('slick-initialized').removeClass('slick-slider');
            for (var k = 0; k < renderArray.length; k++) {
                this.append(options.itemTemplate);
                if (renderArray[k].images.length != 0) {
                    this.find(options.productContainer).eq(k).find(options.imageContainer).append('<img src="' + renderArray[k].images[0].file_url + '">');
                }
                this.find(options.productContainer).eq(k).find(options.link).attr({'href': '/index.php?option=com_virtuemart&view=productdetails&virtuemart_product_id=' + renderArray[k].virtuemart_product_id + '&virtuemart_category_id=1'});
                this.find(options.productContainer).eq(k).find(options.productNameContainer).html(renderArray[k].product_name);
                this.find(options.productContainer).eq(k).find(options.productPriseContainer).html(renderArray[k].product_price);
                this.find(options.productContainer).eq(k).find(options.productDescriptionContainer).html(renderArray[k].product_price);
                this.find(options.productContainer).eq(k).find(options.vmpid).val(renderArray[k].virtuemart_product_id);
                this.find(options.productContainer).eq(k).find(options.vmpname).val(renderArray[k].product_name);
                this.find(options.productContainer).eq(k).attr('data-prod-id', renderArray[k].virtuemart_product_id).addClass('byin');
            }
            options.onRender(this);
        }
        this.filtration(options.filter);
        return this;
    };
    $.fn.calcData = function(options, linkObject) {
        options = $.extend({
            containers: {
                'calcid_10': '.vid10',
                'calcid_12': '.vid12',
                'calcid_13': '.vid13',
                'calcid_14': '.vid14',
                'calcid_15': '.vid15'
            }
        }, options)

        this.construct = function() {
            this.addClass('filtercalculator');
            for (var i = 0; i < customsArray.length; i++) {
                var vm_id = customsArray[i].virtuemart_custom_id;
                for (var j = 0; j < customsArray[i]['values'].length; j++) {
                    var object = options.containers['calcid_' + vm_id];
                    $(object).find('input,option').eq(j).attr('data-custom-id', customsArray[i].values[j].id);
                }
            }

        }

        this.monitoring = function() {

            var filterArray = []
            for (var key in options.containers) {
                /*********/

                if ($(options.containers[key]).find('input[type="radio"],input[type="checkbox"]').length != 0) {

                    if ($(options.containers[key]).find('input[type="radio"]:checked,input[type="checkbox"]:checked').length != 0) {
                        var array = $(options.containers[key]).find('input[type="radio"]:checked,input[type="checkbox"]:checked').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    } else {
                        var array = $(options.containers[key]).find('input[type="radio"],input[type="checkbox"]').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    }
                }
                /************/
                if ($(options.containers[key]).find('select').length != 0) {

                    if ($(options.containers[key]).find('select option:selected').length != 0) {
                        var array = $(options.containers[key]).find('select option:selected').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    } else {
                        var array = $(options.containers[key]).find('select option').map(function() {
                            return $(this).attr('data-custom-id')
                        })
                        array = array.get();
                        var id_group = key.split('_');
                        id_group = id_group[1];
                        var filter = {id_group: id_group, value: array};
                        filterArray.push(filter);
                    }
                }
                if ($(options.containers[key]).find('#ageInner').length != 0) {

                    var id_group = key.split('_');
                    id_group = id_group[1];
                    var array = $(options.containers[key]).find('#ageInner').map(function() {
                        var age = $(this).val()
                        for (var k = 0; k < customsArray.length; k++) {
                            if (customsArray[k].virtuemart_custom_id == id_group) {
                                for (var l = 0; l < customsArray[k].values.length; l++) {
                                    var splitValue = customsArray[k].values[l].value.split('-');
                                    if (age > splitValue[0] && age <= splitValue[1]) {
                                        age = customsArray[k].values[l].id;
                                    }

                                }
                            }

                        }


                        return age;
                    })
                    array = array.get();
                    var id_group = key.split('_');
                    id_group = id_group[1];
                    var filter = {id_group: id_group, value: array};
                    filterArray.push(filter);
                }
                /********/
            }

            fotoRandom();
            linkObject.filtration(filterArray);
        }

        this.construct();
//        this.each(function() {
//            $(window).bind('submit', this.monitoring());
//        });

        return this;
    }
})(jQuery);

