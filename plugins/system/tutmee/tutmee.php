<?php

defined('_JEXEC') or die;

class plgSystemTutmee extends JPlugin {

    function onAfterInitialise() {

        $session = JFactory::getSession();
        $app = JFactory::getApplication();

        if ($app->isAdmin()) {
            if (!strpos($app->input->server->get('REQUEST_URI', NULL, 'string'), 'tutmeeCorp')) {
                if ($session->get('adminKey') != 'tutmeeCorp') {
                    header('Location:http://' . $app->input->server->get('SERVER_NAME', NULL, 'string'));
                    jexit();
                }
            } else { 
                $session->set('adminKey', 'tutmeeCorp');
            }
        }
    }

}
