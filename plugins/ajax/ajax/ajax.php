<?php

defined('_JEXEC') or die;

jimport('joomla.plugin.plugin');

class plgAjaxAjax extends JPlugin {

    function onAjaxAjax() {

        $jinput = JFactory::getApplication()->input;

        if ($jinput->get('task')) {
            $task = $jinput->get('task', '', 'string');
            $data = $jinput->get('formData', '', 'array');
            $session = JFactory::getSession();
            $mailfrom = JFactory::getConfig()->get('mailfrom');
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            switch ($task) {

                case 'cartDelete':
                    $cartData = json_decode($session->get('vmcart', 0, 'vm'));
                    foreach ($cartData->cartProductsData as $key => &$cart)
                        if ($cart->virtuemart_product_id == $jinput->get('virtuemart_product_id'))
                            array_splice($cartData->cartProductsData, $key, 1);
                    $session->set('vmcart', json_encode($cartData), 'vm');
                    return $cartData;
//                    return true;
                    break;

                case 'cartUpdate':
                    $cartData = json_decode($session->get('vmcart', 0, 'vm'));
                    foreach ($cartData->cartProductsData as &$cart)
                        if ($cart->virtuemart_product_id == $jinput->get('virtuemart_product_id'))
                            $cart->quantity = $jinput->get('quantity');
                    $session->set('vmcart', json_encode($cartData), 'vm');
                    return true;
                    break;

                case 'addCart':
                    $cartData = json_decode($session->get('vmcart', 0, 'vm'));
                    $add = FALSE;
                    if (!$cartData) {
                        $cartData=array();
                        $cartData['cartProductsData'] = array();
                    }
                    foreach ($cartData->cartProductsData as &$cart)
                        if ($cart->virtuemart_product_id == $jinput->post->get('virtuemart_product_id', '', 'init')) {
                            $cart->quantity +=$jinput->post->get('quantity', '', 'init');
                            $add = TRUE;
                        }
                    if (!$add)
                        $cartData->cartProductsData[] = array(
                            'virtuemart_product_id' => $jinput->post->get('virtuemart_product_id', '', 'init'),
                            'quantity' => $jinput->post->get('quantity', '', 'init')
                        );
                    $session->set('vmcart', json_encode($cartData), 'vm');
                    return $cartData;
                    break;

                case "getProducts" :

                    //Общий запрос по данным товара
                    $query->select('prod.virtuemart_product_id, prodru.product_name, ROUND(price.product_price) AS product_price, cust.customfield_value AS descr')
                            ->from('#__virtuemart_products AS prod')
                            ->leftJoin('#__virtuemart_products_ru_ru AS prodru ON prodru.virtuemart_product_id = prod.virtuemart_product_id')
                            ->leftJoin('#__virtuemart_product_prices AS price ON price.virtuemart_product_id = prod.virtuemart_product_id')
                            ->leftJoin('#__virtuemart_product_customfields AS cust ON cust.virtuemart_product_id = prod.virtuemart_product_id')
                            ->where('prod.published = 1')
                            ->where('cust.virtuemart_custom_id = 8');
                    $prods = $db->setQuery($query)->loadObjectList();
                    $query->clear();

                    //Делаем отдельную выборку с целью сформировать доступные поля для калькулятора
                    $query->select('virtuemart_custom_id, custom_title')
                            ->from('#__virtuemart_customs')
                            ->where('custom_parent_id = 9');
                    $customs = $db->setQuery($query)->loadObjectList();
                    $query->clear();
                    if (!empty($customs)) {
                        foreach ($customs as &$custom) {
                            $query->select('id, value')
                                    ->from('#__virtuemart_product_custom_plg_param_values')
                                    ->where('virtuemart_custom_id = ' . (int) $custom->virtuemart_custom_id);
                            $custom->values = $db->setQuery($query)->loadObjectList();
                            $query->clear();
                        }
                    }

                    foreach ($prods as &$prod) {
                        //Добавляем изображения
                        $query->select('media.file_url')
                                ->from('#__virtuemart_medias AS media')
                                ->leftJoin('#__virtuemart_product_medias AS prodmed ON media.virtuemart_media_id = prodmed.virtuemart_media_id')
                                ->where('prodmed.virtuemart_product_id = ' . $prod->virtuemart_product_id);
                        $prod->images = $db->setQuery($query)->loadObjectList();
                        $query->clear();
                        $prod->images[0]->file_url = 'http://'.$_SERVER['HTTP_HOST'].DIRECTORY_SEPARATOR.$prod->images[0]->file_url;
                        
//                        var_dump($prod->images);
                        //Добавляем настраиваемые поля
                        $query->select('virtuemart_custom_id, val')
                                ->from('#__virtuemart_product_custom_plg_param_ref')
                                ->where('virtuemart_product_id = ' . (int) $prod->virtuemart_product_id)
                                ->order('virtuemart_custom_id');
                        $prodCusts = $db->setQuery($query)->loadObjectList();
                        $query->clear();

                        if (!empty($prodCusts))
                            foreach ($prodCusts as $prodCust)
                                $prod->customs[$prodCust->virtuemart_custom_id][] = $prodCust->val;
                    }

                    $return['prods'] = $prods;
                    $return['customs'] = $customs;

                    return $return;

                    break;

                case 'sendMail':
                    $data = $jinput->post->get('formData', '', 'array');
                    $subject = $jinput->post->get('subject', '', 'string');
                    $return = $this->sendEmail($subject, $data);

                    return $return;

                    break;

                case "sendMailShares" :
                    $data = $jinput->post->get('formData', '', 'array');
                    $subject = $jinput->post->get('subject', '', 'string');
                    $return = $this->sendEmail($subject, $data);

                    $mail = JFactory::getMailer();
                    $mail->isHtml(true);
                    $mail->CharSet = 'utf-8';
//                    return $data[0][];
                    $mailfrom = array_shift($data);
                    $mailfrom = $mailfrom['value'];
                    $mail->AddAddress($mailfrom);
                    
                    $mailfrom = JFactory::getConfig()->get('mailfrom');
                    $mail->AddAddress($mailfrom);
                    $mail->AddAddress('i-wolhw@ya.ru');

                    $mail->Subject = "Вы подписались на акции на сайте " . $_SERVER['SERVER_NAME'];

                    $msg = '<table>';
                    $msg .= "<tr><td>Вы подписались на акции на сайте " . $_SERVER['SERVER_NAME'] . "</td></tr>";
                    $msg .= '</table>';

                    $mail->Body = $msg;
                    if (!$mail->Send())
                        return false;
                    else
                        return true;
                    break;
            }
        }
        return false;
    }

    private function sendEmail($subject, $data) {
        $mailfrom = JFactory::getMailer()->From;

        $mail = JFactory::getMailer();
        $mail->isHtml(true);
        $mail->CharSet = 'utf-8';

        if (count($data)) {
            $msg = '<table>';
            foreach ($data as $key => $val) {
                if (empty($val['value']))
                    continue;
                $msg .= "<tr><td>{$this->translateKeyName($val['name'])}: </td><td>{$val['value']}</td></tr>";
            }
            $msg .= '</table>';
        }

        $mail->AddAddress($mailfrom);
        $mail->AddAddress('i-wolhw@ya.ru');
        $mail->AddAddress('akopov.mika@yandex.ru');
        $mail->Subject = "$subject с сайта " . $_SERVER['SERVER_NAME'];
        $mail->Body = $msg;
        if (!$mail->Send())
            return false;
    }

    private function translateKeyName($key) {
        $converter = array(
            'name' => 'Имя',
            'phone' => 'Телефон',
            'email' => 'Email',
            'company' => 'Компания',
            'product_name' => 'Товар',
            'quantity' => 'Количество'
        );
        return strtr($key, $converter);
    }

}
