<?php
defined('_JEXEC') or die;
?>
<table class="table table-striped table-bordered" width="60%">
    <thead>
        <tr class="active">
            <th>Товар</th>
            <th>Количество</th>
        </tr>
    </thead>
    <tbody>
        <? if (count($this->items)): ?>
            <? foreach ($this->items as $item) { ?>
                <tr>
                    <td><?= $item->name ?></td>
                    <td class="center"><?= $item->quantity ?></td>
                </tr>
            <? } ?>
        <? endif; ?>
    </tbody>
</table>