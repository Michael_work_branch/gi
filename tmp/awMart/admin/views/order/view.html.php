<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class AWMartViewOrder extends JViewLegacy {

    protected $items;

    public function display($tpl = null) {
        try {
            // Получаем данные из модели.
            $this->items = $this->get('Items');

            $this->addToolbar();

            // Отображаем представление.
            parent::display($tpl);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    protected function addToolbar() {
        JToolbarHelper::title(JText::_('Заказ '.JFactory::getApplication()->input->request->get('order', 0, 'int')));

        JToolbarHelper::back('Назад','index.php?option=com_awmart');
    }

}
