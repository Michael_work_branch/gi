<?php
defined('_JEXEC') or die;
?>
<table class="table table-striped table-bordered">
    <thead>
        <tr class="active">
            <th><input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" /></th>
            <th>Номер заказа</th>
            <th>Цена</th>
            <th>Статус</th>
            <th>Дата заказа</th>
            <th>Дата оплаты</th>
            <!--<th>Пользователь</th>-->
            <th>Адрес</th>
            <th>Телефон</th>
            <th>ID</th>
        </tr>
    </thead>
    <tbody>
        <? if (count($this->items)): ?>
            <? foreach ($this->items as $key=>$item) { ?>
                <tr>
                    <td>
                        <?= JHtml::_('grid.id', $key, $item->id); ?>
                    </td>
                    <td class="center"><a href="<?= JRoute::_('index.php?option=com_awmart&view=Order&order=' . $item->order) ?>"><?= $item->order ?></a></td>
                    <td class="center"><?= $item->price ?></td>
                    <td class="center<? ($item->status) ? print ' success' : print ' danger' ?>">
                        <?
                        if ($item->status) {
                            print 'Оплачено';
                        } else {
                            print 'Не оплачено';
                        }
                        ?>
                    </td>
                    <td class="center"><?
                        $date = explode(' ', $item->date);
                        print "$date[1] ";
                        $date = explode('-', $date[0]);
                        print "$date[2]-$date[1]-$date[0]";
                        ?></td>
                    <td class="center"><?
                        $date = explode(' ', $item->date_success);
                        print "$date[1] ";
                        $date = explode('-', $date[0]);
                        print "$date[2]-$date[1]-$date[0]";
                        ?></td>
                    <!--<td><?= $item->user_id ?></td>-->
                    <td>
                        <?
                        $address = json_decode($item->address);
                        if ($address->index) {
                            $r.= $address->index . ',';
                        }
                        if ($address->region) {
                            $r.= $address->region . ',';
                        }
                        if ($address->city) {
                            $r.= $address->city . ',';
                        }
                        if ($address->street) {
                            $r.= $address->street . ',';
                        }
                        if ($address->hous) {
                            $r.= $address->hous . ',';
                        }
                        if ($address->apartment) {
                            $r.= $address->apartment . ',';
                        }
                        print substr($r, 0, -1)
                        ?>
                    </td>
                    <td class="center"><?= $item->phone ?></td>
                    <td class="center"><?= $item->id ?></td>
                </tr>
            <? } ?>
        <? endif; ?>
    </tbody>
</table>
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<?php echo JHtml::_('form.token'); ?>

<?php echo $this->pagination->getListFooter(); ?>
<!--<pre>
    <? print_r($this->items) ?>
</pre>-->