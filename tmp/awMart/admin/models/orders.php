<?php

// Запрет прямого доступа.
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class AWMartModelOrders extends JModelList {

    protected function getListQuery() {
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('*')
                ->from('#__awmart_orders')
                ->order('`id` desc');

        return $query;
    }

}
