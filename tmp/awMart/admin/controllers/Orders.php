<?php

defined('_JEXEC') or die;

class AWMartControllerOrders extends JControllerAdmin {

    public function Orders() {
        
        $document = JFactory::getDocument();
        $viewType = $document->getType();
        $this->getView('Orders', $viewType)->success();
        //parent::display($cachable, $urlparams);
    }

}