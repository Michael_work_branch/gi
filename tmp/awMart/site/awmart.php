<?php

defined('_JEXEC') or die;

//JLog::addLogger(
//        array('text_file' => 'com_inphone.php'), JLog::ALL, array('com_inphone')
//);
JError::$legacy = false;

$controller = JRequest::getWord('view', 'On');
$task = JRequest::getWord('task', 'newOrder');
if ($controller == 'On') {  //по дефолту если пытаемся войти то посылаем на главную
    header('Location: ./ ');
    jexit();
}

jimport('joomla.filesystem.file');
jimport('joomla.html.parameter');

if (JFile::exists(JPATH_COMPONENT . DS . 'controllers' . DS . $controller . '.php')) {
    $classname = 'AWMartController' . $controller;
    if (!class_exists($classname))
        require_once (JPATH_COMPONENT . DS . 'controllers' . DS . $controller . '.php');
    $controller = new $classname();
    $controller->execute($task); 
   $controller->redirect();
}else {
    JError::raiseError(404, JText::_('Вы стучитесь в закрытую дверь. Постучите в другую, воможно там открыто.'));
}