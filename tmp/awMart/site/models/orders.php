<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');

class AWMartModelOrders extends JModelItem {

    function save($orders) {
        if (is_object($orders)and count($orders)) {
            $db = JFactory::getDbo();
            $order = new stdClass();
            $order->order = $orders->order;

            $session = JFactory::getSession();
            $cartData = json_decode($session->get('vmcart', 0, 'vm'))->cartProductsData;
            if (count($cartData)) {
                foreach ($cartData as $value) {
                    $order->vmid = $value->virtuemart_product_id;
                    $order->quantity = $value->quantity;
                    $db->insertObject('#__awmart_orders_details', $order);
                }
            } else {
                return FALSE;
            }
            if (!$db->insertObject('#__awmart_orders', $orders)) {
                return false;
            }
            return true;
        } else {
            return false;
        }
    }

    public function update($update) {
        if (!$this->getDbo()->updateObject('#__awmart_orders', $update, 'order')):
            JFactory::getSession()->clear('vmcart');
            return false;
        endif;
        return true;
    }

}
